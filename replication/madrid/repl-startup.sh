#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cp /YDBGUI/certs/ydbgui.pem /repl/backups/madrid.ydbgui.pem

$ydb_dist/mupip set -replication=on -region "*"
sleep 1

date=`date +%Y%m%d:%H:%M:%S`
$ydb_dist/mupip replicate -source -start -passive -instsecondary=dummy -log=/tmp/dummy"$date".log
sleep 1
echo '*********************'
echo 'Source checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -source -checkhealth

$ydb_dist/mupip replicate -receive -start -listenport=3005 -log=/tmp/madrid"$date".log
sleep 1
echo '*********************'
echo 'Receive checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -receive -checkhealth

echo '*********************'
echo 'Dummy log'
echo '*********************'
tail -30 /tmp/dummy"$date".log

echo '*********************'
echo 'Madrid log'
echo '*********************'
tail -30 /tmp/madrid"$date".log
