#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cp /YDBGUI/certs/ydbgui.pem /repl/backups/melbourne.ydbgui.pem

$ydb_dist/mupip set -replication=on -region "*"
sleep 1

date=`date +%Y%m%d:%H:%M:%S`
mupip replicate -source -start -instsecondary=paris -secondary=paris:3001 -log=/tmp/paris"$date".log
mupip replicate -source -start -instsecondary=santiago -secondary=santiago:3002 -log=/tmp/santiago"$date".log
sleep 1

echo '*********************'
echo 'Source checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -source -checkhealth

echo '*********************'
echo 'Paris log'
echo '*********************'
tail -30 /tmp/paris"$date".log

echo '*********************'
echo 'Amsterdam log'
echo '*********************'
tail -30 /tmp/amsterdam"$date".log

$ydb_dist/mupip backup -replinst=/repl/backups/melbourne.repl
