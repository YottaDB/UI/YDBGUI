<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Toolbar and status bar

---

# **The toolbar**

From the toolbar you can execute all the commands needed to create, run and mange your statistical report.

<img src="md/stats/img/toolbar.png" width="800">

<div class="entry-spacer"></div>

#### <img src="md/stats/img/start-btn.png" width="32"> Start

The `Start button` begins the data collection.

When the data collection is started, it gets colored with a steady red color.

When paused, it will flash with a red color.

> It can also resume a paused session.

> If one or more sources have the processes set to PIDs and the selected processes don't exist anymore, the following dialog will appear, allowing you to re-select the PIDs from the current system processes.
> <img src="md/stats/img/pids-dialog.png" width="300">

<div class="entry-spacer"></div>

#### <img src="md/stats/img/pause-btn.png" width="32"> Pause

The `Pause button` pauses a running session, indicated by a flashing red Start button.

By clicking it again, it will resume the session.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/stop-btn.png" width="32"> Stop

The `Stop button` terminates the data acquisition.

> By restarting a new session, all previously collected data will be lost.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/sources-btn.png" width="32"> Sources

Opens the <a href="#" onclick="app.link('stats/sources')">Sources dialog</a>.

In the Sources dialog you can configure

- the samples you want to collect
- the target regions and processes
- the sample rate.

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/definition-btn.png" width="32"> Report definition

Opens the <a href="#" onclick="app.link('stats/spark-chart/index')">Report definitions dialog</a>.

In the definition dialog you can create a report based on the enabled Sources.

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/theme-btn.png" width="32"> Theme

Switches between the Light and Dark theme.

<img src="md/stats/img/theme-light-screen.png" width="300"> <img src="md/stats/img/theme-dark-screen.png" width="300">

Both themes can be fully configured in the Definition dialog Views properties

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/new-btn.png" width="32"> New report

Resets the report to a blank one.

If an unsaved report is present, it will prompt you to save it first.

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/load-btn.png" width="32"> Load report

Display the <a href="#" onclick="app.link('stats/load')">Load report dialog</a>, where you can

- load a report
- import a report
- delete a report

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/save-btn.png" width="32"> Save report

Display the <a href="#" onclick="app.link('stats/save')">Save report dialog</a>, where you can save and export a report.

This functionality is available only when the data collection is stopped.

<div class="entry-spacer"></div>

---

# **The status bar**

In the status bar you can see the status of your data collection.

<img src="md/stats/img/statusbar.png" width="800">

<div class="entry-spacer"></div>

#### <img src="md/stats/img/led.png" width="32"> Activity LED

The `activity LED` will flash **green** when samples are received.

If an error occur in the Web Socket server or the M server, it will flash **red**.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/status.png" width="150"> Status

The `Status` field indicates the current status of the data collection.

It may have the following values:

- Stopped
- Running
- Paused

<div class="entry-spacer"></div>

#### <img src="md/stats/img/sample-rate.png" width="150"> Sample Rate

The `Sample Rate` field indicates the sample rate of the currently active sources.

- If no sources are present or are disabled it will indicate: N/A
- If one source is present and enabled, it will indicate its Sample Rate in seconds
- If more than one source is present and enabled, it will return all the different Sample Rates, dash separated

<div class="entry-spacer"></div>

#### <img src="md/stats/img/timestamp.png" width="150"> Timestamp

The `Timestamp` field indicates the timestamp of the currently fetched sample.

<div class="entry-spacer"></div>

#### <img src="md/stats/img/number-of-samples.png" width="150"> \# of Samples

The `# of samples` field indicates how many samples got collected.

<div class="entry-spacer"></div>
