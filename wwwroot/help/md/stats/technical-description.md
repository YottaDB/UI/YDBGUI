<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Technical description

---

In order to bring statistical data to the browser, we use Web Sockets.

When you open the Statistics tab, we instantiate the Web Socket server, which in turn runs the M Statistics Server and establishes an IPC channel between the two. Once they are all up and running, we connect the Web Socket.

<img src="md/stats/img/Drawing1.svg" width="900" style="border: 0!important;">

#### The server

The Web Socket server is a node.js application and performs the following tasks:

- Instantiate the M Statistics Server (which is an M application)
- Create an IPC channel between the M Statistics Server and the Web Socket Server, used to execute M code at specific intervals and retrieve the result as JSON
- Expose a set of custom commands to the Web Socket clients, used to schedule tasks, run, pause and stop the data acquisition
- Route the result (coming from the M Statistics Server) to the Web Socket client, for further processing

During the Statistics session, the M Statistics server gets instantiated only once and then remains active in memory, awaiting commands through the IPC to acquire samples, meaning that we do NOT shell out each time we acquire a sample.

#### The client

The client is a listener on the Web socket that processes the messages responses. This is used to both:

- Process the acknowledged messages received from the messages sent TO the server (like Start, Stop, Pause, etc.)
- Receive statistics data at the specified time intervals in the background

Each time we receive a statistics sample, we compute the delta, min, max, and avg and we append it to an array, along with the timestamp included in the message.

To refresh the graphs, we simply issue a refresh, as the data arrays are passed by reference.

To refresh the tables, we process an array of update operations, populated by the rendering routine that generates the
report. Each entry in the array has:

- a target screen element id
- the data source (sample and sample type)

Additional operation (like coloring in base of value and top process highlight) get performed while updating.
