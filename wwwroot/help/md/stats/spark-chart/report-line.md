<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / Table dialog

---

<img src="md/stats/spark-chart/img/report-line-dialog.png" width="600">

The Table displays the samples configured in the Mappings dialog in horizontal or vertical pattern. It can additionally
auto-color the background of specific fields when the value reached a pre-defined range or the process that has the
higher value.

---

#### The header

<img src="md/stats/spark-chart/img/report-line-header.png" width="600">

The header displays an overview of the source. The Region, Processes and Samples are listed.

---

#### The orientation section

<img src="md/stats/spark-chart/img/report-line-orientation.png" width="600">

In the orientation section you can choose how the data is displayed.

- Horizontal will display the SAMPLES as columns and the SAMPLE TYPES (delta, average, etc.) as rows
  <img src="md/stats/spark-chart/img/report-line-preview-h.png" width="400">

- Vertical will display the SAMPLES TYPES (delta, average, etc.) as columns and the SAMPLES as rows
  <img src="md/stats/spark-chart/img/report-line-preview-v.png" width="400">

#### The preview section

<img src="md/stats/spark-chart/img/report-line-preview-h.png" width="600">

In the preview you can see the way the Table will look like when the data collection is running.

---

#### Data highlights

<img src="md/stats/spark-chart/img/report-line-preview-highlight-btn.png" width="100">

The data highlights mode is displayed on each cell, along with button to access the <a href="#" onclick="app.link('stats/spark-chart/highlights')">Highlights dialog</a>.

It supports the following modes:

- `Normal`: the background color is the one defined in the settings
- `Range`: the background color changes according to the cell value
- `Max`: the background color change on the process with the highest value

---

#### Change the Settings

By default, all the settings inherit from the `Defaults settings` dialog, available through the pull-down menu: `Database administration` `Preferences`:

<img src="md/stats/spark-chart/img/report-line-default-preferences.png" width="400">

It is possible to override / change all these value by changing the `Settings` through the <img src="md/stats/spark-chart/img/report-line-settings-btn.png" width="100"> button:

<img src="md/stats/spark-chart/img/report-line-settings-dialog.png" width="500">

---

#### Additional settings options

Additional options are available for the Settings through the menu:
<img src="md/stats/spark-chart/img/report-line-settings-menu.png" width="300">

##### Reset settings:

The `Reset settings` option will allow you to reset all the settings to the original settings, inherited from the `Defaults settings`.

##### Save settings as default

The `Save settings as default` option will overwrite the `Default settings`. All the newly created Table will use these
settings as default.

