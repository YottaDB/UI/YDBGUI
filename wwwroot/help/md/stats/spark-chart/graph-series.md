<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / <a href="#" onclick="app.link('stats/spark-chart/graph')">Graph properties dialog</a> / Graph series dialog

---

<img src="md/stats/spark-chart/img/graph-series-dialog.png" width="600">

In the Graph series properties dialog you can change the appearance of the individual series. This include line settings (dotted, weight, color, etc.) and y-axis.

---

On the left pane you can change the parameters:
<img src="md/stats/spark-chart/img/graph-series-left-pane.png" width="200">

while on the right side you gete a real-time preview of your changes:
<img src="md/stats/spark-chart/img/graph-series-right-pane.png" width="300">

---

#### Line settings

With the <img src="md/stats/spark-chart/img/graph-series-line-settings-btn.png" width="200"> button you have full control of the line appearance.

---

#### Y-axis settings

With the <img src="md/stats/spark-chart/img/graph-series-y-axis-btn.png" width="200"> button you have full control of the y-axis properties.
