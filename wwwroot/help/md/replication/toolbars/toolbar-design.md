<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

# <a href="#" onclick="app.link('replication/toolbars/toolbars')">Toolbars</a>

## Design toolbar

---

The Design toolbar is used to modify the appearance of the interactive chart.

You can change the way links tie the instances together, as well as auto-arrange or fit-to-content the entire Topology.

> Additional interaction can be obtained by rolling the mouse wheel up or down to zoom in or out, and by "grabbing" the paper to move the layout around.

The following actions can be taken:

- Switch to the Run toolbar
- Undo
- Redo
- Zoom in
- Zoom out
- Auto-arrange
- Fit to content
- Router change
- Connector change

---

## <img src="md/replication/toolbars/img/design-switch.png" width="50"> Switch

This icon switches to the Run toolbar.

---

## <img src="md/replication/toolbars/img/design-undo.png" width="50"> Undo

This icon un-does the last action in the history.

Each time it gets clicked, it goes back in the history; until it reaches the end, then it gets disabled.

---

## <img src="md/replication/toolbars/img/design-redo.png" width="50"> Redo

This icon re-does the last action that got un-done in the history.

Each time it gets clicked, it goes forward in the history; until it reaches the beginning, then it gets disabled.

---

## <img src="md/replication/toolbars/img/design-zoom-out.png" width="50"> Zoom out

This icon zooms the entire chart out.

> The same action can be done by rolling the mouse wheel down.


---

## <img src="md/replication/toolbars/img/design-zoom-in.png" width="50"> Zoom in

This icon zooms the entire chart in.

> The same action can be obtained by rolling the mouse wheel up.

---

## <img src="md/replication/toolbars/img/design-auto-arrange.png" width="200"> Auto arrange

This icon allows you to automatically arrange the entire chart using the following directions:

- Left to Right
- Top to Bottom

In order to fine-tune your layout, you can additionally control the:

- Rank separation: the distance between ranks in the hierarchy
- Node separation: the distance between nodes at the same level in the hierarchy

<img src="md/replication/toolbars/img/design-auto-arrange-dialog.png" width="400">

Additionally, you can re-force the currently selected direction by clicking on the left side of the combobox:

<img src="md/replication/toolbars/img/design-auto-arrange-left.png" width="200">


---

## <img src="md/replication/toolbars/img/design-fit-to-content.png" width="50"> Fit to content

This icon will automatically zoom the drawing paper to fit your layout into the current screen.

> This action will NOT change your layout


---

## <img src="md/replication/toolbars/img/design-router.png" width="200"> Router

This icon gives you control on how the path of the link is drawn between the instances.

You have four options:

- Normal
- Manhattan
- Metro
- Orthogonal

> As the layout is strictly related to the topology, you will need to experiment which value the best suite for your configuration.

Examples of the different routers:

#### Normal

<img src="md/replication/toolbars/img/design-router-normal.png" width="450">

#### Manhattan

<img src="md/replication/toolbars/img/design-router-manhattan.png" width="450">

#### Metro

<img src="md/replication/toolbars/img/design-router-metro.png" width="450">

#### Orthogonal

<img src="md/replication/toolbars/img/design-router-orthogonal.png" width="450">

> To avoid backlog overlapping, you can drag the backlog along the path:
> <img src="md/replication/toolbars/img/design-router-backlog-1.png" width="250">
> <img src="md/replication/toolbars/img/design-router-backlog-2.png" width="250">


---

## <img src="md/replication/toolbars/img/design-connector.png" width="200"> Connector

This icon gives you control on how the path of the link is drawn between the bezels of the path.

You have these options:

- Straight
- Curve
- Smooth

> As the layout is strictly related to the topology, you will need to experiment which value the best suite for your configuration.

Examples of the different connectors:

#### Straight

<img src="md/replication/toolbars/img/design-connector-straight.png" width="450">

#### Curve

<img src="md/replication/toolbars/img/design-connector-curve.png" width="450">

#### Smooth

<img src="md/replication/toolbars/img/design-connector-smooth.png" width="450">
