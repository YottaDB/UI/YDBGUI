<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## Modifying, saving and recalling the Topology interactive chart

---

When you first open the Topology tab, the software crawls through the instances and creates a topology. It then uses the default settings to draw it as a chart. The default settings are not used if you have previously saved a modified layout with the option `Autoload` set. In this case, once the topology is fetched, it will apply the saved layout and use it to draw the chart.

There are several ways to modify the topology:

- Using the mouse to move the instances around
- Using the mouse to alter the links (you can also create and remove bezels) and the backlog counters
- Using the <a href="#" onclick="app.link('replication/toolbars/toolbar-design')">Design toolbar</a> to auto-arrange, fit to content or change the links

Additionally, you can save and load the layouts, using the tools on the <a href="#" onclick="app.link('replication/toolbars/toolbar-run')">Run toolbar</a>.

## Moving the instances around

You can move the instances around by simply dragging and dropping them. You can place them anywhere you want...

If you get lost or things gets messy, you can always click on the `Fit to content` icon to bring everything in view.

---

## Altering the links

Links can be individually changed to match your requirements.

Each time you click on a link and drag it to move it, you are creating a `bezel`.

By hovering over a link, you can see the created bezels:

<img src="md/replication/img/edit-bezels.png" width="300">

By hovering over it, the cursor will change, indicating you can move it around again and correct your curve.

You can have as many bezels as you want.

If you want to remove all the bezels in a link, simply pop up the Context Menu by hovering over it and clicking the right mouse button:

<img src="md/replication/img/edit-bezels-context-menu.png" width="300">

---

## Adjusting the position of the backlog counters

When adjusting the layout, it is possible that the backlog counters will overlap each other or the instances, making them unreadable.

To adjust them, simply hover with the mouse over them and then drag them (you can drag them ONLY along the path of the link).

<img src="md/replication/img/edit-backlog-1.png" width="300"> <img src="md/replication/img/edit-backlog-2.png" width="220">

---

## Auto-arranging the layout

You can ask the system to auto-arrange the items for you.

The auto-arrange can be done in two different directions:

- Left to right
- Top to bottom

To change the direction, use the options button in the popup that appears when clicking the icon:

<img src="md/replication/img/edit-arrange-direction.png" width="350">

and it will automatically arrange the entire layout using the chosen method (and ensuring it will fit on the screen):

#### Left to Right

<img src="md/replication/img/edit-arrange-lr.png" width="350">

#### Top to Bottom

<img src="md/replication/img/edit-arrange-tb.png" width="350">

The additional parameters:

- Rank separation
- Node separation

allow you to fine tune the arrange operation to match your needs.

#### Rank separation

The Rank separation is the distance between the groups:

<img src="md/replication/img/edit-rank-1.png" width="350"> <img src="md/replication/img/edit-rank-2.png" width="350">

#### Node separation

The Node separation is the distance between the instances (following the chosen direction):

<img src="md/replication/img/edit-node-1.png" width="350"> <img src="md/replication/img/edit-node-2.png" width="350">



---

## Saving your layout

You can save your layout to be recalled at a later stage using the <a href="#" onclick="app.link('replication/save-dialog')">Save dialog</a> dialog.


---

## Loading your layout

You can load a previously saved layout using the <a href="#" onclick="app.link('replication/load-dialog')">Load dialog</a> dialog.

---
