<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/dynamic/index')">Dynamic mode: monitoring the Replication instances</a>

## Log files dialog

---

<img src="md/replication/dynamic/dialogs/img/log-files-dialog.png" width="700">

The Log Files dialog displays the latest version of the Log files.

They are grouped by Log file type, on the left hand, and by clicking it you can display the chosen log file content.

<img src="md/replication/dynamic/dialogs/img/log-files-dialog.png" width="400">

---

### Refreshing the view

In order to get the latest version of the file, you can click on the refresh button.

<img src="md/replication/dynamic/dialogs/img/log-files-refresh.png" width="400">


---

### Changing the size of the returned file

You can change the size of the tail of the returned file.

<img src="md/replication/dynamic/dialogs/img/log-files-tail.png" width="400">

Any number (positive or negative) will be used as tail.
By entering 0 it will return the entire file.

