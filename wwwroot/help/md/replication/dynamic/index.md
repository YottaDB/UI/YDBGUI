<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## Dynamic mode: monitoring the Replication instances

---

In order to start monitoring Replication you must turn the refresh on (or set it by default on, so that it will automatically start to monitor when you open the Topology tab).

When Replication monitoring is turned on, the system will query all the instances included in the Topology at the refresh frequency, querying for health status and backlog.

It will then update the interactive chart and display the status.

---

### Understand what you are seeing...

In order to correctly interpret the real-time status of your Replication, you need to undestand how it gets visualized:

- The Instances
  - The replication status
  - The machine health
- The backlog indicators

#### <a href="#" onclick="app.link('replication/dynamic/description')">Click here to get a complete description of Instances, Links and their health status</a>

---

### Real-time updates

When refresh is enabled, the software will crawl through all the instances and collect health and backlog information and update the interactive chart accordingly.

<img src="md/replication/dynamic/img/dynamic-running.png" width="800">

As you can see, the backlog counters show the entries left to be spooled or flushed into the database.

This is a typical situation when all replication traffic is idle:

<img src="md/replication/dynamic/img/dynamic-idle.png" width="600">

### Displaying log files and instance files

By right-clicking on the instances, you will pop up the context menu:

<img src="md/replication/dynamic/img/context-menu.png" width="300">

You can perform the following action for that particular instance:

- Open the GUI in another tab
- <a href="#" onclick="app.link('replication/dynamic/dialogs/log-files-dialog')">Display the log files</a>
- <a href="#" onclick="app.link('replication/dynamic/dialogs/instance-file-dialog')">Display the Instance file</a>
