/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.logs = {
    init: () => {
        $('#btnReplLogFilesRefresh').on('click', () => app.ui.replication.logs.refreshClicked())
        $('#btnReplLogsHelp').on('click', () => app.ui.replication.logs.help())

        // execute this on close to clean up the DOM from huge log files
        $('#modalReplLogFiles').on('hidden.bs.modal', () => app.ui.replication.logs.cleanUp())

        app.ui.setupDialogForTest('modalReplLogFiles')
    },

    show: async function (data, instanceName) {
        const listReplLogFilesEntries = $('#listReplLogFilesEntries')
        const listReplLogFilesPanes = $('#listReplLogFilesPanes')
        let entriesList = ''
        let entriesPanes = ''

        this.data = data
        this.instanceName = instanceName

        // hide the popup menu
        $('#divContextMenuShapes').css('display', 'none')

        // clear existing lists
        listReplLogFilesEntries.empty()
        listReplLogFilesPanes.empty()

        // populate the list and the panes
        if (data.receiver !== null) {
            entriesList += '<a class="list-group-item list-group-item-action " id="" data-toggle="list" href="#logReplLogFilesReceiver" role="tab">Receiver</a>'
            entriesPanes += '<div class="tab-pane fade show" id="logReplLogFilesReceiver" role="tabpanel">'

            entriesPanes += '<div class="repl-filename-log">' + data.receiver.file + '</div>'
            entriesPanes += '<div style="height: 370px; border: 1px solid var(--ydb-purple); overflow-y: auto;">'
            entriesPanes += '<table class="table table-hover table-bordered table-sm repl-table-log" id="tblReplLogsReceiver">'
            entriesPanes += '<thead><tr class="repl-instance-table" style="font-size: 14px; font-weight: 800; background: var(--ydb-lightgray);">'
            entriesPanes += '<th scope="col" style="width: 130px;">Timestamp</th>'
            entriesPanes += '<th scope="col">Text</th>'
            entriesPanes += '</tr></thead>'
            entriesPanes += '<tbody></tbody></table>'
            entriesPanes += '</div>'

            entriesPanes += '</div>'
        }

        if (data.updater !== null) {
            entriesList += '<a class="list-group-item list-group-item-action " id="" data-toggle="list" href="#logReplLogFilesUpdater" role="tab">Updater</a>'
            entriesPanes += '<div class="tab-pane fade show" id="logReplLogFilesUpdater" role="tabpanel">'

            entriesPanes += '<div class="repl-filename-log">' + data.updater.file + '</div>'
            entriesPanes += '<div style="height: 370px; border: 1px solid var(--ydb-purple); overflow-y: auto;">'
            entriesPanes += '<table class="table table-hover table-bordered table-sm repl-table-log" id="tblReplLogsUpdater">'
            entriesPanes += '<thead><tr class="repl-instance-table" style="font-size: 14px; font-weight: 800; background: var(--ydb-lightgray);">'
            entriesPanes += '<th scope="col" style="width: 130px;">Timestamp</th>'
            entriesPanes += '<th scope="col">Text</th>'
            entriesPanes += '</tr></thead>'
            entriesPanes += '<tbody></tbody></table>'
            entriesPanes += '</div>'

            entriesPanes += '</div>'
        }

        listReplLogFilesPanes.append(entriesPanes)

        data.sources.forEach(source => {
            entriesList += '<a class="list-group-item list-group-item-action " id="" data-toggle="list" href="#logReplLogFilesSource-' + source.source + '" role="tab">' + source.source + '</a>'

            entriesPanes += '<div class="tab-pane fade show" id="logReplLogFilesSource-' + source.source + '" role="tabpanel">'

            entriesPanes += '<div class="repl-filename-log">' + source.file + '</div>'
            entriesPanes += '<div style="height: 370px; border: 1px solid var(--ydb-purple); overflow-y: auto;">'
            entriesPanes += '<table class="table table-hover table-bordered table-sm repl-table-log" id="tblReplLogsSource-' + source.source + '">'
            entriesPanes += '<thead><tr class="repl-instance-table" style="font-size: 14px; font-weight: 800; background: var(--ydb-lightgray);">'
            entriesPanes += '<th scope="col" style="width: 130px;">Timestamp</th>'
            entriesPanes += '<th scope="col">Text</th>'
            entriesPanes += '</tr></thead>'
            entriesPanes += '<tbody></tbody></table>'
            entriesPanes += '</div>'

            entriesPanes += '</div>'

            listReplLogFilesPanes.append(entriesPanes)
        })

        listReplLogFilesEntries.append(entriesList)

        // set the tail size
        $('#inpReplLogsTail').val('-50')

        // populate the tables with log file content
        await this.populateTables(data, instanceName)

        // select the first item
        $('#listReplLogFilesEntries :first-child')
            .tab('show')

        $('#lblReplLogsTitle').text(instanceName)

        // show the dialog
        $('#modalReplLogFiles')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    refreshClicked: async function () {
        // populate the tables with log file content
        await this.populateTables(this.data, this.instanceName)
    },

    populateTables: async function (data, instanceNameLog) {
        app.ui.wait.show('Fetching data...')
        const crawlAndExtract = function (node) {
            for (let instanceName in node) {
                if (node[instanceName].done) {
                    delete node[instanceName].done
                }
                if (instanceName === instanceNameLog) {
                    logFiles = node[instanceName].logFiles
                    fullDone = true
                }

                if (fullDone === true) return

                node[instanceName].done = true

                if (node[instanceName].ERROR === undefined) {
                    if (node[instanceName].children) {
                        for (let child in node[instanceName].children) {
                            crawlAndExtract(node[instanceName].children)
                        }
                    }
                }
            }
        }

        let fileList = []
        let logFiles = {}
        let fullDone = false

        if (data.receiver !== null) {
            fileList.push(data.receiver.file)
        }

        if (data.updater !== null) {
            fileList.push(data.updater.file)
        }

        data.sources.forEach(source => {
            fileList.push(source.file)
        })

        app.userSettings.defaults.replication.discoveryService.responseType = 'L:' + instanceNameLog
        app.userSettings.defaults.replication.discoveryService.logFiles = {
            tail: $('#inpReplLogsTail').val(),
            files: fileList
        }

        try {
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            crawlAndExtract(res)

            if (logFiles.receiverData) {
                const $table = $('#tblReplLogsReceiver > tbody')
                let rows = ''

                $table.empty()

                logFiles.receiverData.file.forEach(row => {
                    let rowData = row.split(' : ')

                    let timestamp = rowData[0].split(' ')
                    timestamp.shift()
                    timestamp.pop()
                    timestamp = timestamp.join(' ')

                    delete rowData[0]
                    rowData = rowData.join(' ')

                    rows += '<tr>'
                    rows += '<td>' + timestamp + '</td><td>' + rowData + '</td>'
                    rows += '<tr>'
                })

                $table.append(rows)
            }

            if (logFiles.updaterData) {
                const $table = $('#tblReplLogsUpdater > tbody')
                let rows = ''

                $table.empty()

                logFiles.updaterData.file.forEach(row => {
                    let rowData = row.split(' : ')

                    let timestamp = rowData[0].split(' ')
                    timestamp.shift()
                    timestamp.pop()
                    timestamp = timestamp.join(' ')

                    delete rowData[0]
                    rowData = rowData.join(' ')

                    rows += '<tr>'
                    rows += '<td>' + timestamp + '</td><td>' + rowData + '</td>'
                    rows += '<tr>'
                })

                $table.append(rows)
            }

            if (logFiles.sourcesData) {
                for (let sourceName in logFiles.sourcesData) {
                    const $table = $('#tblReplLogsSource-' + sourceName + ' > tbody')
                    let rows = ''

                    $table.empty()

                    logFiles.sourcesData[sourceName].forEach(row => {
                        let rowData = row.split(' : ')

                        let timestamp = rowData[0].split(' ')
                        timestamp.shift()
                        timestamp.pop()
                        timestamp = timestamp.join(' ')

                        delete rowData[0]
                        rowData = rowData.join(' ')

                        rows += '<tr>'
                        rows += '<td>' + timestamp + '</td><td>' + rowData + '</td>'
                        rows += '<tr>'
                    })

                    $table.append(rows)
                }
            }

            this.logFiles = structuredClone(logFiles)

            app.ui.wait.hide()

        } catch (err) {
            app.ui.wait.hide()

            console.log(err)
        }
    },

    cleanUp: function () {
        $('#tblReplLogsReceiver > tbody').empty()
        $('#tblReplLogsUpdater > tbody').empty()
        if (this.logFiles.sourcesData) {
            for (let sourceName in this.logFiles.sourcesData) {
                const $table = $('#tblReplLogsSource-' + sourceName + ' > tbody')
            }
        }
    },

    help: function () {
        const page = 'replication/dynamic/dialogs/log-files-dialog'
        const path = window.location.origin + '/help/index.html?'

        window.open(path + page, 'ydbgui_help')
    },

    logFiles: null,
    data: {},
    instanceName: '',
}
