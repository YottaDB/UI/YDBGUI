/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
app.ui.replication.topology.joint = {
    init: async function (topology) {
        // initialize the paper
        const {shapes: defaultShapes} = joint
        const shapes = {...defaultShapes, Primary, Secondary, Supplementary, ForeignObjectElement, replLink}

        let graph = this.graph = new joint.dia.Graph({}, {cellNamespace: shapes});

        this.paper = new joint.dia.Paper({
            el: document.getElementById('divTopologyPaper'),
            model: graph,
            width: '5000', //$('#divTopologyPaper').width(),
            height: '5000', //$('#divTopologyPaper').height(),
            gridSize: 1,
            async: true,
            cellViewNamespace: shapes,
            snapLabels: true,
            interactive: {labelMove: true,}
        });

        app.ui.replication.topology.joint.events.mount()

        this.refreshFull(topology)

        app.ui.setTestClosure('tabReplTopology')
    },

    refreshFull: async function (topology) {
        this.topology = topology

        const crawlAndAdd = function (node, parent = null) {
            // create entries
            for (let instanceName in node) {
                //if (node[instanceName].done) break
                node[instanceName].instanceName = instanceName

                node[instanceName].isAlive = node[instanceName].ERROR === undefined

                let rect = app.ui.replication.topology.joint.stencils.instanceObject(node[instanceName], instanceName)

                rect.addTo(app.ui.replication.topology.joint.graph)
                //node[instanceName].done = true

                if (parent !== null) {

                    let link = app.ui.replication.topology.joint.stencils.instanceLinks(rect, parent)

                    link.addTo(app.ui.replication.topology.joint.graph);
                }

                if (node[instanceName].children) {
                    crawlAndAdd(node[instanceName].children, rect)
                }
            }
        }

        setTimeout(() => {
            app.ui.regeneratePopups()
        }, 500)

        this.paper.async = false

        // clear the existing graph
        this.graph.removeCells(this.graph.getCells());
        this.graph.removeLinks(this.graph.getLinks());

        // initiate data adding
        crawlAndAdd(topology)

        this.crc = this.computeCrc32()

        // check if there is an autoload file saved
        const file = app.userSettings.replicationFiles.filter(file => file.crc === app.ui.replication.topology.joint.crc && file.autoload === true)
        if (file.length > 0) {
            try {
                $('#divTopologyToolbarDesign').css('display', 'none')
                $('#divTopologyToolbarRun').css('display', 'block')

                app.ui.replication.storage.load.currentProfile = file[0]
                app.ui.replication.topology.joint.graph.fromJSON(file[0].graph)
                this.actions.fitToContent()
                app.ui.replication.topology.joint.paper.translate(file[0].origin.tx, file[0].origin.ty)
                app.ui.replication.topology.joint.paper.scale(file[0].scale.sx)

            } catch (err) {
                console.log(err)
            }

        } else {
            this.actions.arrange(app.userSettings.defaults.replication.topology.layout)
        }

        this.paper.async = true

        // initialize the undo / redo buffer
        app.ui.replication.topology.joint.undoRedo.init()

        app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
        try {
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)
            this.refresh(res)

        } catch (err) {
            console.log(err)

            let res = {}
            res[app.system.replication.instanceFile.flags.instanceName] = {
                ERROR: 'server_not_reachable'
            }

            this.refresh(res)
        }
    },

    refresh: function (res) {
        const crawlAndUpdate = function (node, parent = null, parentNode = null) {
            let ix = -1

            for (let instanceName in node) {
                ix++

                const rect = app.ui.replication.topology.joint.findShapeById(instanceName)

                // update log files list
                rect.attributes.data.logFiles = node[instanceName].logFiles
                rect.attr('data', node[instanceName])

                if (parent !== null) {
                    // this is a child, we can set up the link as well
                    const currentServer = node[instanceName]
                    const parentServer = parentNode

                    // find link
                    const link = app.ui.replication.topology.joint.findLinkById(parent.id + '-' + rect.id)
                    let lineState = (parentServer.isAlive === false || currentServer.isAlive === false) ? 'dead' : 'active'
                    let sourceState = lineState
                    let targetState = lineState
                    if (lineState === 'active') {
                        // pre-compute source index
                        let sourceIx = -1
                        parentServer.health.sources.forEach((source, ix) => {
                            if (source.instanceName === currentServer.instanceName) sourceIx = ix
                        })

                        // process here the health status
                        if (
                            (parentServer.health.receiverServer.isAlive !== null && parentServer.health.receiverServer.isAlive === false) ||
                            (parentServer.health.receiverServer.isAlive !== null && parentServer.health.updateProcess.isAlive === false) ||
                            parentServer.health.sources[sourceIx].isAlive === false
                        ) {
                            lineState = sourceState = 'broken'
                        }

                        if (
                            (currentServer.health.receiverServer.isAlive !== null && currentServer.health.receiverServer.isAlive === false) ||
                            (currentServer.health.receiverServer.isAlive !== null && currentServer.health.updateProcess.isAlive === false)
                        ) {
                            lineState = targetState = 'broken'
                        }

                    } else {
                        sourceState = parentServer.isAlive === true ? 'active' : 'broken'
                        targetState = currentServer.isAlive === true ? 'active' : 'broken'
                    }

                    // process here the health status
                    if (lineState === 'active') {
                        if (currentServer.health.updateProcess.isAlive !== true || currentServer.health.receiverServer.isAlive !== true) {
                            targetState = lineState = 'broken'
                        }
                    }

                    link.setLine(lineState)
                    link.setSourceState(sourceState)
                    link.setTargetState(targetState)

                    // update backlog
                    let backlogIx = -1
                    let backlog0, status0
                    let backlog1, status1

                    parentNode.health.sources.forEach((source, sourceIx) => {
                        if (currentServer.instanceName === source.instanceName) backlogIx = sourceIx
                    })

                    if (parentNode.backlog) {
                        backlog0 = parentNode.isAlive === true ? parentNode.backlog.source.data.streams[backlogIx] : '   '
                        status0 = app.userSettings.defaults.replication.topology.backlog.processRange === true ? parentNode.backlog.source.data.status[backlogIx] : false

                        if (parentServer.isAlive === true) link.setSourceBacklog(backlog0, status0)
                    }
                    if (node[instanceName].backlog) {
                        backlog1 = currentServer.isAlive === true && node[instanceName].backlog.receiver && node[instanceName].backlog.receiver.data ? node[instanceName].backlog.receiver.data.total : '   '
                        status1 = app.userSettings.defaults.replication.topology.backlog.processRange === true ? node[instanceName].backlog && node[instanceName].backlog.receiver && node[instanceName].backlog.receiver.status : false

                        if (currentServer.isAlive === true) link.setTargetBacklog(backlog1, status1)
                    }

                    // backlog on dashboard
                    if (parentServer.instanceName === app.system.replication.instanceFile.flags.instanceName) {
                        app.ui.dashboard.refreshBacklog.update(parentNode.backlog)
                    }

                    // health on rects
                    rect.setSourceServers(currentServer.health)
                    rect.setUpdateProcess(currentServer.health)
                    rect.setReceiverServer(currentServer.health)

                    parent.setSourceServers(parentServer.health)
                    parent.setUpdateProcess(parentServer.health)
                    parent.setReceiverServer(parentServer.health)

                } else {
                    // this is the root
                    rect.setSourceServers(node[instanceName].health)
                    rect.setUpdateProcess(node[instanceName].health)
                    rect.setReceiverServer(node[instanceName].health)

                }

                // override values on graph
                if (node[instanceName].isAlive === true) {
                    rect.setAlive()
                } else {
                    rect.setDead(node[instanceName].deadType)
                }

                // crawl if needed
                if (node[instanceName].children) {
                    crawlAndUpdate(node[instanceName].children, rect, node[instanceName])
                }
            }
        }

        const crawlAndRecon = async function (node, res, flagAsDead = false) {
            const DEAD_TYPE_NO_CONNECTIVITY = 1
            const DEAD_TYPE_INTERNAL_ERROR = 2
            const DEAD_TYPE_UNKNOWN_STATUS = 3
            const DEAD_TYPE_NO_REPL = 4
            const DEAD_BAD_RESPONSE = 5
            const DEAD_SSL_ERROR = 6
            const BAD_TLS_FILE = 7

            for (let instanceName in node) {
                // overwrite the logs, just in case they got changed due to services restart
                if (res[instanceName] && res[instanceName].logFiles) node[instanceName].logFiles = res[instanceName].logFiles

                node[instanceName].ERROR = res[instanceName].ERROR

                if (flagAsDead === true) {
                    // mark as dead all descendant, as the parent is dead
                    node[instanceName].isAlive = false
                    node[instanceName].errorType = 1                                // no connectivity
                    node[instanceName].deadType = DEAD_TYPE_UNKNOWN_STATUS          // unknown status

                    // and crawl the operation of children
                    if (node[instanceName].children) {
                        for (let child in node[instanceName].children) {
                            crawlAndRecon(node[instanceName].children, {}, true)
                        }
                    }

                    continue
                }

                if (res[instanceName].ERROR === undefined) {
                    node[instanceName].isAlive = true
                    node[instanceName].deadType = 0

                } else {
                    // error found
                    node[instanceName].isAlive = false
                    node[instanceName].ERROR = res[instanceName].ERROR

                    switch (res[instanceName].ERROR) {
                        // address lookup errors
                        case 'server_not_reachable':
                        case 'curl:-5':
                        case 'curl:-6':
                        case 'curl:-7':
                        case 'curl:-8':
                        case 'curl:-28':
                            node[instanceName].errorType = 1            // no connectivity
                            node[instanceName].deadType = DEAD_TYPE_NO_CONNECTIVITY
                            break

                        case 'tls_file_not_found':
                            node[instanceName].errorType = 1            // no connectivity due to SSL errors
                            node[instanceName].deadType = BAD_TLS_FILE
                            break

                        // SSL errors
                        case 'curl:-35':
                        case 'curl:-53':
                        case 'curl:-54':
                        case 'curl:-56':
                        case 'curl:-58':
                        case 'curl:-59':
                        case 'curl:-60':
                        case 'curl:-64':
                        case 'curl:-66':
                        case 'curl:-77':
                        case 'curl:-80':
                        case 'curl:-82':
                        case 'curl:-83':
                        case 'curl:-90':
                        case 'curl:-91':
                        case 'curl:-98':
                            node[instanceName].errorType = 1            // no connectivity due to SSL errors
                            node[instanceName].deadType = DEAD_SSL_ERROR
                            break

                        case 'status_500':
                            node[instanceName].errorType = 500          // internal error

                            if ((node[instanceName].ERROR_data && node[instanceName].ERROR_data.indexOf('NORPLINFO') > -1) || !node[instanceName].ERROR_data) {
                                node[instanceName].deadType = DEAD_TYPE_NO_REPL
                            } else {

                                try {
                                    node[instanceName].errorData = JSON.parse(node[instanceName].ERROR_data)
                                    // trap no replication structure error
                                    node[instanceName].deadType = node[instanceName].errorData.error.errors[0].message.message.indexOf('NORPLINFO') > -1 ? DEAD_TYPE_NO_REPL : DEAD_TYPE_INTERNAL_ERROR

                                } catch (err) {
                                    node[instanceName].errorData = node[instanceName].ERROR_data || null
                                    node[instanceName].deadType = DEAD_BAD_RESPONSE
                                }
                            }

                            break

                        // other errors
                        default:
                            node[instanceName].errorType = node[instanceName].ERROR && node[instanceName].ERROR.indexOf('curl') > -1 ? 2 : 3          // 2: generic curl error 3: generic error
                            node[instanceName].errorData = node[instanceName].ERROR_data || null
                            node[instanceName].deadType = DEAD_TYPE_INTERNAL_ERROR
                    }
                }

                if (node[instanceName].isAlive === true) {
                    // process backlog
                    node[instanceName].oldBacklog = structuredClone(node[instanceName].backlog)
                    node[instanceName].backlog = res[instanceName].backlog

                    // backlog status
                    if (node[instanceName].oldBacklog && node[instanceName].backlog) {
                        // receiver if exists
                        if (node[instanceName].backlog.receiver && node[instanceName].backlog.receiver.data) {
                            node[instanceName].backlog.receiver.status = node[instanceName].backlog.receiver.data.total >= node[instanceName].oldBacklog.receiver.data.total && node[instanceName].oldBacklog.receiver.data.total > 0
                        }

                        // streams
                        node[instanceName].backlog.source.data.status = []
                        node[instanceName].backlog.source.data.streams.forEach((stream, ix) => {
                            node[instanceName].backlog.source.data.status.push(stream >= node[instanceName].oldBacklog.source.data.streams[ix] && stream > 0)
                        })
                    }

                    // if local machine, copy to app.system
                    if (instanceName === app.system.replication.instanceFile.flags.instanceName) {
                        app.system.replication.backlog = node[instanceName].backlog
                    }

                    // perform the reconciliation
                    let baseline = node[instanceName].health
                    const incoming = res[instanceName].health

                    if (baseline === undefined) {
                        node[instanceName].health = structuredClone(incoming)

                    } else {
                        // update process
                        if (!baseline || (incoming.updateProcess && baseline.updateProcess && incoming.updateProcess.isAlive !== baseline.updateProcess.isAlive)) {
                            baseline.updateProcess.isAlive = incoming.updateProcess.isAlive

                        }

                        // receiver server
                        if (!baseline || (incoming.updateProcess && baseline.updateProcess && incoming.receiverServer.isAlive !== baseline.receiverServer.isAlive)) {
                            baseline.receiverServer.isAlive = incoming.receiverServer.isAlive
                        }

                        if (incoming.sources && baseline && (incoming.sources.length > baseline.sources.length)) {
                            // we have new entries, meaning some dead process has restarted, simply copy the new list
                            baseline.sources.length = incoming.sources.length
                            baseline.sourcesGlobal = incoming.sourcesGlobal

                        } else if (incoming.sources && baseline && (incoming.sources.length < baseline.sources.length)) {
                            // we have fewer entries, meaning something has died
                            baseline.sources.forEach(source => {
                                if (incoming.sources.find(newSource => source.instanceName === newSource.instanceName) === undefined) {
                                    source.isAlive = baseline.sourcesGlobal.allAlive = false
                                }
                            })

                        } else {
                            if (incoming.sources) {
                                let isAlive = true

                                baseline.sources.forEach(source => {
                                    const newSource = incoming.sources.find(newSource => source.instanceName === newSource.instanceName)
                                    if (newSource && newSource.isAlive === false) isAlive = false
                                })

                                baseline.sourcesGlobal.allAlive = isAlive
                            }
                        }
                    }

                    // crawl
                    if (node[instanceName].children) {
                        crawlAndRecon(node[instanceName].children, res[instanceName].children)

                    } else if (res[instanceName].children) {
                        app.userSettings.defaults.replication.discoveryService.responseType = 'F'
                        const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

                        app.ui.replication.topology.joint.refreshFull(res)
                    }

                } else {
                    // flag children as dead
                    if (node[instanceName].children) {
                        for (let child in node[instanceName].children) {
                            crawlAndRecon(node[instanceName].children, {}, true)
                        }
                    }

                }
            }
        }

        // data reconciliation
        crawlAndRecon(app.ui.replication.topology.joint.topology, res)

        // initiate health and backlog updating
        crawlAndUpdate(app.ui.replication.topology.joint.topology)
        this.getGraph()
    },

    findShapeById: function (id) {
        return this.graph.getElements().find(element => element.id === id)
    },

    findLinkById: function (id) {
        return this.graph.getLinks().find(link => link.id === id)
    },

    getGraph: function () {
        const ret = []

        this.graph.attributes.cells.models.forEach(model => {
            //console.log(model)
            ret.push({
                id: model.id,
                type: model.attributes.type,
                dead: model.dead,
                attr: model.attributes.attrs,
                backlog: model.attributes.labels,
                position: model.attributes.position,
                router: model.attributes.router ? model.attributes.router.name : '',
                connector: model.attributes.connector ? model.attributes.connector.name : ''
            })
        })

        return ret
    },

    getPaper: function () {
        return {
            origin: app.ui.replication.topology.joint.paper.translate(),
            scale: app.ui.replication.topology.joint.paper.scale(),
        }
    },

    actions: {
        arrange: function () {
            joint.layout.DirectedGraph.layout(
                app.ui.replication.topology.joint.graph,
                this.arrangeData
            )
            this.fitToContent()
        },
        arrangeData: {
            nodeSep: 200,
            edgeSep: 200,
            marginX: 5,
            marginY: 5,
            rankSep: 200,
            rankDir: 'TB',
            setLinkVertices: false,
        },


        fitToContent: function () {
            app.ui.replication.topology.joint.paper.transformToFitContent({
                padding: {
                    top: 15,
                    bottom: 15,
                    left: 5,
                    right: 5,
                },
                verticalAlign: 'middle',
                horizontalAlign: 'middle',
                useModelGeometry: true,
            })
        },
    },

    computeCrc32: function () {
        let crcString = ''
        this.graph.attributes.cells.models.forEach(model => {
            crcString += model.attributes.type + ':' + model.id + ':'
        })

        return app.ui.crc32(crcString)
    },

    topology: {},
    paper: {},
    graph: {},
    crc: '',
}
