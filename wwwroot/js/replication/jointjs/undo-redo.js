/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.topology.joint.undoRedo = {
    mountHandlers: function () {
        app.ui.replication.topology.joint.graph
            .on('change', (evt => {
                app.ui.replication.topology.joint.undoRedo.mouseButton.gotEvents = true
            }))

        app.ui.replication.topology.joint.paper
            .on('link:pointerdown', (linkView) => {
                this.state = this.MOUSE_BUTTON_DOWN
            })

            .on('link:pointerup', (linkView) => {
                this.pointerUp()
            })

            .on('element:pointerdown', () => {
                this.state = this.MOUSE_BUTTON_DOWN
            })

            .on('element:pointerup', () => {
                this.pointerUp()
            })
    },

    pointerUp: function () {
        this.mouseButton.state = this.MOUSE_BUTTON_UP

        if (this.mouseButton.gotEvents === true) {
            // reset the flag
            this.mouseButton.gotEvents = false

            // creates an entry
            this.saveEntry()
        }
    },

    saveEntry: function () {
        // create a undo entry
        const entry = {
            origin: app.ui.replication.topology.joint.paper.translate(),
            scale: app.ui.replication.topology.joint.paper.scale(),
            graph: app.ui.replication.topology.joint.graph.toJSON()
        }

        this.buffer.push(entry)
        this.currentBuffer = this.buffer.length - 2

        if (this.buffer.length > app.userSettings.defaults.replication.topology.undoBufferSize) this.buffer.shift()

        app.ui.button.enable($('#btnTopologyToolbarUndo'))
    },

    init: function () {
        this.buffer = []

        this.saveEntry()
        this.currentBuffer = 0
        this.mouseButton.state = this.MOUSE_BUTTON_UP
        this.mouseButton.gotEvents = false

        app.ui.button.disable($('#btnTopologyToolbarUndo'))
        app.ui.button.disable($('#btnTopologyToolbarRedo'))
    },

    undo: function () {
        const entry = this.buffer[this.currentBuffer]

        app.ui.replication.topology.joint.graph.fromJSON(entry.graph)
        app.ui.replication.topology.joint.paper.translate(entry.origin.tx, entry.origin.ty)
        app.ui.replication.topology.joint.paper.scale(entry.scale.sx)


        if (this.currentBuffer > 0) {
            this.currentBuffer--

        } else app.ui.button.disable($('#btnTopologyToolbarUndo'))

        app.ui.button.enable($('#btnTopologyToolbarRedo'))

        // regenerate popups (delayed, as the graph load is async)
        setTimeout(() => {
            app.ui.regeneratePopups()

        }, 200)
    },

    redo: function () {
        if (this.currentBuffer < this.buffer.length - 1) {
            this.currentBuffer++

            app.ui.button.enable($('#btnTopologyToolbarUndo'))
        } else app.ui.button.disable($('#btnTopologyToolbarRedo'))

        const entry = this.buffer[this.currentBuffer]

        app.ui.replication.topology.joint.graph.fromJSON(entry.graph)
        app.ui.replication.topology.joint.paper.translate(entry.origin.tx, entry.origin.ty)
        app.ui.replication.topology.joint.paper.scale(entry.scale.sx)

        // regenerate popups (delayed, as the graph load is async)
        setTimeout(() => {
            app.ui.regeneratePopups()

        }, 200)
    },

    MOUSE_BUTTON_DOWN: false,
    MOUSE_BUTTON_UP: true,

    enabled: false,
    buffer: [],
    maxBufferLength: 100,
    currentBuffer: -1,
    mouseButton: {
        state: this.MOUSE_BUTTON_UP,
        gotEvents: false
    },
}
