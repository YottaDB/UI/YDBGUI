/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.storage = {
    save: {
        init: function () {
            $('#btnReplSaveOk').on('click', () => this.okPressed())
            $('#btnReplSaveHelp').on('click', () => this.help())

            app.ui.setupDialogForTest('modalReplSave')
        },

        show: function () {
            $('#inpReplSaveName').val(app.ui.replication.storage.load.currentProfile.name ? app.ui.replication.storage.load.currentProfile.name : '')
            $('#inpReplSaveDescription').val('')
            $('#swReplSaveAutoload').prop('checked', false)

            // show the dialog
            $('#modalReplSave')
                .modal({show: true, backdrop: 'static'})
                .draggable({handle: '.modal-header'})
        },

        okPressed: function () {
            const name = $('#inpReplSaveName').val()
            const crc = app.ui.replication.topology.joint.computeCrc32()
            const autoload = $('#swReplSaveAutoload').prop('checked')
            const description = $('#inpReplSaveDescription').val()
            let duplicateFound = 0
            let autoloadExists = 0

            const performSave = function () {
                let layout = {
                    name: name,
                    description: description,
                    crc: crc,
                    autoload: autoload,
                    origin: app.ui.replication.topology.joint.paper.translate(),
                    scale: app.ui.replication.topology.joint.paper.scale(),
                    graph: app.ui.replication.topology.joint.graph.toJSON()
                }

                // remove autoload from other if needed
                if (autoloadExists > 0) {
                    app.userSettings.replicationFiles[autoloadExists].autoload = false
                }

                // replace or append
                if (duplicateFound > 0) {
                    app.userSettings.replicationFiles[duplicateFound] = layout
                } else {
                    app.userSettings.replicationFiles.push(layout)
                }

                // save to storage
                app.ui.storage.save('replicationFiles', app.userSettings.replicationFiles)
            }

            if (name === '') {
                app.ui.msgbox.show('You must enter a name...', app.ui.msgbox.INPUT_ERROR)

                return
            }

            app.userSettings.replicationFiles.forEach((file, ix) => {
                if (file.name === name && file.crc === crc) {
                    duplicateFound = ix
                }

                if (file.crc === crc && file.autoload === true) {
                    autoloadExists = ix
                }
            })

            if (duplicateFound > 0) {
                app.ui.inputbox.show('A layout with the same name already exists.<br><br>Do you want to proceed?', '', ret => {
                    if (ret === 'YES') {
                        performSave()
                        $('#modalReplSave').modal('hide')
                    }
                })

            } else {
                performSave()
                $('#modalReplSave').modal('hide')
            }
        },

        help: function () {
            const page = 'replication/save-dialog'
            const path = window.location.origin + '/help/index.html?'

            window.open(path + page, 'ydbgui_help')
        },
    },

    load: {
        init: function () {
            $('#btnReplLoadOk').on('click', () => this.okPressed())
            $('#btnReplLoadDelete').on('click', () => this.deletePressed())
            $('#selReplLoadNames')
                .on('click', () => this.optionSelected())
                .on('dblclick', () => this.optionDoubleClicked())
            $('#btnReplLoadHelp').on('click', () => this.help())

            app.ui.setupDialogForTest('modalReplLoad')
        },

        show: function () {
            this.loadList()

            // select first item
            if (this.list.length > 0) {
                $('#selReplLoadNames :first-child').prop('selected', true)
                this.optionSelected()

                app.ui.button.enable($('#btnReplLoadDelete'))
                app.ui.button.enable($('#btnReplLoadOk'))

            } else {
                app.ui.button.disable($('#btnReplLoadDelete'))
                app.ui.button.disable($('#btnReplLoadOk'))
            }

            // show the dialog
            $('#modalReplLoad')
                .modal({show: true, backdrop: 'static'})
                .draggable({handle: '.modal-header'})
        },

        loadList: function () {
            const $select = $('#selReplLoadNames')
            let options = ''
            this.list = app.userSettings.replicationFiles.filter(file => file.crc === app.ui.replication.topology.joint.crc)

            $select.empty()

            if (this.list.length === 0) {
                // no items found
                $select.append('<option disabled style="font-style: italic;">No items found</option>')

            } else {
                // populate the list
                this.list.forEach((entry, ix) => {
                    options += '<option value="' + ix + '">' + entry.name + '</option>'
                })

                $select.append(options)
            }
        },

        optionSelected: function () {
            const item = this.list[$('#selReplLoadNames').val()]

            $('#divReplLoadDescription').html(item.description.replaceAll('\n', '<br>'))
            $('#lblReplLoadAutoload').text(item.autoload === true ? ' YES' : 'NO')
        },

        optionDoubleClicked: function () {
            this.okPressed()
        },

        okPressed: function () {
            try {
                this.currentProfile = this.list[$('#selReplLoadNames').val()]
                app.ui.replication.topology.joint.graph.fromJSON(this.currentProfile.graph)
                app.ui.replication.topology.joint.actions.fitToContent()
                app.ui.replication.topology.joint.paper.translate(this.currentProfile.origin.tx, this.currentProfile.origin.ty)
                app.ui.replication.topology.joint.paper.scale(this.currentProfile.scale.sx)

                $('#divTopologyToolbarDesign').css('display', 'block')
                $('#divTopologyToolbarRun').css('display', 'none')

                app.ui.replication.topology.joint.undoRedo.init()

            } catch (err) {
                console.log(err)
            }

            $('#modalReplLoad').modal('hide')
        },

        deletePressed: function () {
            app.ui.inputbox.show('Are you sure you want to delete the selected layout?', '', ret => {
                if (ret === 'YES') {
                    // find entry across the entire set
                    this.currentProfile = this.list[$('#selReplLoadNames').val()]

                    const ix = app.userSettings.replicationFiles.findIndex(entry => entry.name === this.currentProfile.name && entry.crc === this.currentProfile.crc)
                    if (ix === -1) {
                        app.ui.msgbox.show('Item not found', 'Error')

                    } else {
                        // delete it
                        app.userSettings.replicationFiles.splice(ix, 1)

                        // save to storage
                        app.ui.storage.save('replicationFiles', app.userSettings.replicationFiles)
                    }

                    this.loadList()

                    // select first item
                    $('#selReplLoadNames :first-child').prop('selected', true)
                    this.optionSelected()
                }
            })
        },

        help: function () {
            const page = 'replication/load-dialog'
            const path = window.location.origin + '/help/index.html?'

            window.open(path + page, 'ydbgui_help')
        },

        list: [],
        currentProfile: {}
    }
}
