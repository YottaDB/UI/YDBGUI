/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.regionJournalFiles.init = () => {
    app.ui.setupDialogForTest('modalRegionJournalFiles')
    app.ui.setupDialogForTest('modalRegionJournalFilesDetails')
}

app.ui.regionJournalFiles.data = {};
app.ui.regionJournalFiles.show = async region => {
    try {
        const res = await app.REST._getJournalInfo(region)
        const ORPHAN = 999999
        let basePath = ''
        let totalDiskSpace = 0
        if (res.result == 'ERR') {
            $('#lblJournalFilesBasePath').text(res.error.description)
        } else {
            //sort the array by chainCnt
            res.data = res.data.sort((a, b) => a.chainCnt > b.chainCnt ? 1 : -1)

            // pointer for shared data (to display details)
            app.ui.regionJournalFiles.data = res.data

            // populate the table
            const $tbody = $('#tblRegionJournalFiles > tbody')

            $tbody.empty()

            res.data.forEach((file, ix) => {
                if (basePath === '') basePath = file.path

                let percentSize = 0
                let rangeColorClass = ''
                let popupContent = ''

                totalDiskSpace += file.fileSizeBytes

                const virtualFileSize = app.ui.regionJournalFiles.getFieldByName(file.params, 'Virtual file size').split(' ')[0]
                const totalFileSize = app.ui.regionJournalFiles.getFieldByName(file.params, 'Jnlfile SwitchLimit').split(' ')[0]

                // check size in %
                percentSize = virtualFileSize / totalFileSize * 100
                // only the active file gets colored based on the remaining size
                if (file.chainCnt === 1) {
                    app.userSettings.dashboard.activeJournalRanges.forEach(el => {
                        if (rangeColorClass !== '') return

                        if (percentSize >= el.min - 1 && percentSize <= el.max) {
                            rangeColorClass = el.class;
                        }
                    });
                }

                // compute popup text
                popupContent += '<strong>Used blocks: </strong>' + app.ui.formatThousands(virtualFileSize) + '<br>'
                popupContent += '<strong>Total blocks: </strong>' + app.ui.formatThousands(totalFileSize)

                let row = '<tr class="region-journal-files-row ' + (file.chainCnt === 1 ? 'bold stand-out' : '') + '"'
                    + (file.chainCnt < ORPHAN ? ' style="color: var(--ydb-status-green);"' : '') + '>'

                row += '<td style="text-align: center;">' + (file.chainCnt < ORPHAN ? file.chainCnt : 'N/A') + '</td>'
                row += '<td style="text-align: center;">' + (file.chainCnt === 1 ? 'Active' : (file.chainCnt < ORPHAN ? 'In chain' : 'Orphan')) + '</td>'
                row += '<td>' + file.filename + '</td>'
                row += '<td>' + app.ui.regionJournalFiles.getFieldByName(file.params, 'Journal Creation Time') + '</td>'
                row += '<td>' + app.ui.regionJournalFiles.getFieldByName(file.params, 'Time of last update') + '</td>'
                row += '<td style="text-align: center;">' +
                    // popup
                    '<a tabIndex="-1" data-toggle="popover" data-trigger="hover"' +
                    'data-content="' + popupContent + '" title="Space used" data-placement="bottom">' +
                    // progress bar
                    '<div class="progress">' +
                    '<div class="progress-bar journal-file-space-indicator ' + (file.chainCnt === 1 ? 'progress-bar-striped  ' + rangeColorClass : 'range-bg-green')
                    + '" role="progressbar" style="width: ' + percentSize.toString() + '%">' +
                    '</div>' +
                    '</div>  ' +
                    '</a>' +
                    '</td>'
                row += '<td style="text-align: center;"><button type="button" class="btn btn-outline-info btn-sm" id="btnJournalFilesDetails' + ix +
                    '" onclick="app.ui.regionJournalFiles.showDetails(\'' + file.filename + '\')">Details...</button></td>'

                row += '</tr>'
                $tbody.append(row)
            })

            // re-generate the popups
            app.ui.regeneratePopups()

            $('#lblJournalFilesBasePath').text(basePath)
            $('#lblRegionJournalFilesTotalSpace').text(app.ui.formatBytes(totalDiskSpace))
        }
    } catch (err) {
        app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
    }

    $('#lblRegionJournalFiles').html('&nbsp;' + region)

    $('#modalRegionJournalFiles').modal({show: true, backdrop: 'static', keyboard: true})
}

app.ui.regionJournalFiles.showDetails = filename => {
    let fileData = {};
    app.ui.regionJournalFiles.data.forEach(file => {
        if (file.filename === filename) fileData = file
    })

    // populate the table
    const $tbody = $('#tblRegionJournalFilesDetails > tbody')

    $tbody.empty()

    fileData.params.forEach((params, ix) => {
        const MULTI_FIELDS_DATA_INDEX = 39
        let row = '<tr class="region-journal-files-row" >'

        row += '<td>' + params.name + '</td>'
        if (ix < MULTI_FIELDS_DATA_INDEX) {
            row += '<td>' + params.value + '</td>'
        } else {
            row += '<td>User: ' + params.value.User
                + '<br>PID: ' + params.value.PID
                + '<br>Node: ' + params.value.Node
                + '<br>Timestamp: ' + params.value.JPV_TIME
        }

        row += '</tr>'
        $tbody.append(row)
    })

    $('#lblRegionJournalFilesDetails').html('&nbsp;' + filename)

    $('#modalRegionJournalFilesDetails').modal({show: true, backdrop: 'static', keyboard: true})
}

app.ui.regionJournalFiles.getFieldByName = (file, name) => {
    return file.find(el => el.name === name).value
}
