/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.tabs.init = () => {
    app.ui.tabs.cloneAddTab();
};

app.ui.tabs.currentTab = ''

app.ui.tabs.select = async (type, metadata = null, withPromise = false) => {
    const oldTab = $('#tab' + type);

    // select if exists
    if (oldTab.length) {
        oldTab.tab('show');
        app.ui.tabs.currentTab = type

        return;
    }

    // or create a new one
    const tabType = type.split('-')[1];

    // remove the add tab
    $('#liTabsAdd').remove();

    // tab header
    $('#tabsContainer').append($('<li/>', {
        class: 'nav-item',
        role: 'presentation',
        id: 'liTabs' + type
    }));

    $('#liTabs' + type).append($('<a/>', {
        class: "nav-link tab-link inconsolata",
        id: "tab" + type,
        'data-toggle': "tab",
        href: "#" + (tabType === 'G' ? 'tabGviewer' : 'tabRviewer') + type + 'Div',
        role: "tab",
        'aria-controls': type,
        'aria-selected': "false",
    }));

    const newTab = $('#tab' + type);
    $('a[data-toggle="tab"]')
        .on('hide.bs.tab', function (e) {
            $('#' + e.target.id + 'Div').css('display', 'none');
            $('#' + e.relatedTarget.id + 'Div').css('display', 'block');
        })
        .on('shown.bs.tab', function (e) {
            const id = e.currentTarget.id

            app.ui.tabs.currentTab = (id.indexOf('-') === -1) ? '' : id.slice(3)
        })

    let pillCaption
    let tabCaption

    if (metadata === null) {
        switch (tabType) {
            case 'G': {
                pillCaption = 'G'
                tabCaption = 'New global'

                break
            }
            case 'O': {
                pillCaption = 'O'
                tabCaption = 'New query'

                break
            }
            case 'S': {
                pillCaption = 'M'
                tabCaption = 'Monitor database <span id="lblStatsReportName"></span>'

                break
            }
            case 'T': {
                pillCaption = 'T'
                tabCaption = 'Repl Topology'

                break
            }
        }

    } else {
        pillCaption = 'R'
        tabCaption = metadata.routineName.replace(/_/g, "%")
    }

    const caption =
        '<span class="tab-pill">&nbsp;' + pillCaption + '&nbsp;' + '</span>&nbsp;<span name="tab-editable">' + tabCaption + '</span>';

    const editMode = tabType === 'O' ? '&nbsp;<i class="bi-pencil-square" onclick="app.ui.octoViewer.renameTab(\'' + type + '\')"></i>' : ''

    newTab
        .html(caption + editMode + '&nbsp;&nbsp;&nbsp;<span onclick="app.ui.tabs.close(\'tab' + type + '\')" class="tab-close-button close" title="">x</span>')
        .attr('title', tabType === 'G' ? caption : '')
    //newTab.find('[name="tab-editable"').attr('contenteditable', tabType === 'O' ? 'true' : 'false')

    // tab body
    // the type variable here indicated ONLY the type of tab.
    // once the .createInstance() function is executed, a unique id will be appended
    if (tabType === 'G') {
        app.ui.gViewer.createInstance(type);

    } else if (tabType === 'O') {
        app.ui.octoViewer.createInstance(type);

    } else if (tabType === 'R') {
        await app.ui.rViewer.createInstance(type, metadata);

    } else if (tabType === 'T') {
        await app.ui.replication.topology.createInstance(type);

    } else {
        app.ui.stats.createInstance(type);
    }

    newTab.addClass('tab-font');
    newTab.tab('show');

    app.ui.tabs.cloneAddTab();

    app.ui.tabs.currentTab = type

    if (withPromise === true) {
        return new Promise(async function (resolve) {
            resolve();
        })
    }
};

app.ui.tabs.close = async (tabId) => {
    const prevItem = $('#' + tabId).parent().prev();

    // terminate stats server if running
    if (tabId === 'tab-S') {
        try {
            await app.ui.stats.closeTab()
        } catch (e) {
            return
        }

        // re-enable stats menu
        const menuTabsStats = $('#liTabsAdd').find('#menuTabsStats')
        menuTabsStats.removeClass('disabled').removeClass('default').addClass('hand')
    }

    $('#' + tabId).parent().remove();
    $('#' + tabId + 'Div').remove();

    // terminate topology
    if (tabId.indexOf('T') > -1) {
        app.ui.replication.topology.closeTab()

        // re-enable topology menu
        const menuTabsStats = $('#liTabsAdd').find('#menuTabsTopology')
        menuTabsStats.removeClass('disabled').removeClass('default').addClass('hand')
    }


    if (tabId.indexOf('G') > -1) {
        // clear the memory
        delete app.ui.gViewer.instance[tabId];

    } else {
        // clear the memory
        delete app.ui.rViewer.instance[tabId];
    }

    if (prevItem.length !== 0) {
        // select prev sibling tab
        prevItem.find('a').tab('show');

        let prevItemDivId = 'tab-' + prevItem.attr('id').split('-')[1] + '-' + prevItem.attr('id').split('-')[2] + 'Div';
        if (prevItem.attr('id') === 'liTabsDashboard') {
            prevItemDivId = 'tabDashboardDiv'
        }
        if (prevItemDivId.indexOf('undefined')) $('#tabDashboard').tab('show')
        else $('#' + prevItemDivId).css('display', 'block')
    }
};

app.ui.tabs.cloneAddTab = () => {
    const newTabAdd = $('#liTabsAddTemplate').clone();

    newTabAdd
        .attr('id', 'liTabsAdd')
        .css('display', 'block');

    // show / hide octo menu
    const menuTabsOcto = newTabAdd.find('#menuTabsOcto')
    if (app.system.octo && app.system.octo.status === "ok") {
        menuTabsOcto.removeClass('disabled').removeClass('default').addClass('hand')
    } else {
        menuTabsOcto.addClass('disabled').addClass('default').removeClass('hand')
    }

    // show / hide stats menu
    const menuTabsStats = newTabAdd.find('#menuTabsStats')

    if (app.websockets.connected === false) {
        menuTabsStats.removeClass('disabled').removeClass('default').addClass('hand')
    } else {
        menuTabsStats.addClass('disabled').addClass('default').removeClass('hand')
    }

    if (app.ui.replication.topology.closed === false || !app.system.replication) {
        const menuTopology = newTabAdd.find('#menuTabsTopology')
        menuTopology.addClass('disabled').addClass('default').removeClass('hand')
    }

    $('#tabsContainer').append(newTabAdd)
};
