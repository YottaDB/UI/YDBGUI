/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

let StatsResult = class {
    constructor(source, ix) {
        Object.assign(this, {
            ix: ix,
            source: source,
        })
    }

    currentPage = 1
    numberOfPages = 0
    pageSize = 50

    displayEmptyDatasets = true
    tabId = ''
    zoomValue = 12

    $container = {}
    $table = {}
    table = ''

    data = []

    selection = {
        regions: [],
        processes: [],
        samples: [],
        sampleTypes: []
    }
}

app.ui.stats.results = {
    init: function () {
        // mount handlers for acquisition / results tabs
        $('#tabStatsAcquisition').on('click', () => app.ui.stats.results.show('acq'))
        $('#tabStatsResult').on('click', () => app.ui.stats.results.show('res'))

        // keep toolbar items open
        $(document).on('click', '.allow-focus', function (e) {
            e.stopPropagation();
        });
    },

    startAcquisition: function () {
        this.destroy()

        $('#tabStatsResult').attr('disabled', true)
    },

    endAcquisition: function () {
        this.populate()

        $('#tabStatsResult').attr('disabled', false)
    },

    show: function (what = 'res') {
        const navStatsStatusbarAcquisition = $('#navStatsStatusbarAcquisition')
        const navStatsStatusbarResults = $('#navStatsStatusbarResults')
        const divStatsToolbar = $('#divStatsToolbar')
        const divStatsToolbarResult = $('#divStatsToolbarResult')

        if (what === 'acq') {
            // acq =acquisition

            // switch toolbars
            divStatsToolbar.css('display', 'block')
            divStatsToolbarResult.css('display', 'none')

            // switch status bars
            navStatsStatusbarAcquisition.css('display', 'flex')
            navStatsStatusbarResults.css('display', 'none')

        } else {
            // res = result

            // switch toolbars
            divStatsToolbar.css('display', 'none')
            divStatsToolbarResult.css('display', 'block')

            // switch status bars
            navStatsStatusbarAcquisition.css('display', 'none')
            navStatsStatusbarResults.css('display', 'block')
        }

    },

    destroy: function () {
        // destroy all result tabs and the tabsData collection
        this.tabsData = []

        $('#navStatsResultsContent').empty()
        $('#divStatsTabResultsContainer').empty()

    },

    populate: function () {
        const sources = app.ui.stats.entry.data
        const selStatsResultsSource = $('#selStatsResultsSource')
        let options = ''
        let sourcesCount = 0

        // instance tabs
        let sourceIx = 0
        sources.forEach(source => {
            if (source.data.enabled === true) {
                const tabData = new StatsResult(source, sourceIx)

                this.tabsData.push(tabData)

                sourcesCount++

                // create the tab content div
                let tabContent = '<div class="tab-pane fade' + (sourceIx === 0 ? ' show active' : '') + '" id="divStatsResultsContent-' + sourceIx + '" role="tabpanel"></div>'

                $('#divStatsTabResultsContainer').append(tabContent)

                // create the select
                let sourceSummary = 'Reg: ' + source.data.regions.join(', ') + ' Proc: ' + source.data.processes.type
                if (source.data.processes.type === 'top') {
                    sourceSummary += ' ' + source.data.processes.count + ' of ' + source.data.processes.sampleCount
                }

                options += '<option value="optStatsResultSource_' + source.id + '">' + sourceSummary + '</option>'

                this.fullyPopulateArrayEntry(source, tabData)

                tabData.$container = $('#divStatsResultsContent-' + sourceIx)
                app.ui.stats.results.table.create(tabData.$container, tabData)

                // populate toolbar as first (selected) item by emulating an event
                if (sourceIx === 0) {
                    this.toolbar.populate(sourceIx)
                }

                sourceIx++
            }
        })

        selStatsResultsSource
            .empty()
            .append(options)
            .prop('disabled', sourcesCount === 1)
            .on('change', () => app.ui.stats.results.sourceSelected())
            .on('click', () => app.ui.stats.results.sourceSelected())
    },

    sourceSelected: function () {
        const item = $('#selStatsResultsSource').val()
        const sourceId = item.split('_')[1]
        const tabData = this.tabsData.find(tabData => tabData.source.id === sourceId)

        if (tabData.ix === this.tabIx) return

        this.tabIx = tabData.ix
        this.toolbar.populate(tabData.ix)

        this.tabsData.forEach(tabData => {
            tabData.$container.removeClass('show active')
        })

        tabData.$container.addClass('show active')
    },

    fullyPopulateArrayEntry: function (source, tabData) {
        // regions
        source.data.regions.forEach(region => {
            tabData.selection.regions.push(region)
        })

        // processes
        if (source.data.processes.type === 'pids') {
            source.data.processes.pids.forEach(pid => {
                tabData.selection.processes.push(pid)
            })

        } else if (source.data.processes.type === 'agg') tabData.selection.processes.push('*')

        else {
            // top
            const data = app.statistics.data
            data.forEach(source => {
                if (source.id === tabData.source.id) {
                    tabData.source.data.regions.forEach(region => {
                        source.regions[region].forEach(pid => {
                            if (typeof pid[0].pid === 'string' && pid[0].pid !== '') tabData.selection.processes.push(pid[0].pid.slice(1))
                        })
                    })
                }
            })
        }

        // samples
        source.data.samples.forEach(region => {
            tabData.selection.samples.push(region)
        })

        // sample types
        this.sampleTypes.forEach(region => {
            tabData.selection.sampleTypes.push(region)
        })
    },

    // toolbar handlers
    toolbar: {
        populate: function (tabIndex) {
            const tabData = app.ui.stats.results.tabsData[tabIndex]
            const source = tabData.source
            const selection = tabData.selection

            // regions
            const ddStatsResultsRegions = $('#ddStatsResultsRegions')
            const selStatsResultToolbarRegions = $('#selStatsResultToolbarRegions')

            if (source.data.regions.length === 1) {
                ddStatsResultsRegions.addClass('disabled')

            } else {
                ddStatsResultsRegions.removeClass('disabled')
                selStatsResultToolbarRegions.empty()

                let regions = ''
                source.data.regions.forEach(region => {
                    const checked = selection.regions.find(lRegion => lRegion === region)

                    regions += '<li><label style="font-size: 12px;"><input  id="chkStatsResultRegion-' + tabIndex + '-' + region + '" ' + (checked === undefined ? '' : ' checked ') + ' type="checkbox" onclick="app.ui.stats.results.toolbar.regionClicked(\'' + region + '\', ' + tabIndex + ')">' + region + '</label></li>'
                })

                selStatsResultToolbarRegions.append(regions)
            }

            // processes
            const ddStatsResultsProcesses = $('#ddStatsResultsProcesses')
            const selStatsResultToolbarProcesses = $('#selStatsResultToolbarProcesses')

            if (source.data.processes.type === 'agg') {
                ddStatsResultsProcesses.addClass('disabled')

            } else {
                ddStatsResultsProcesses.removeClass('disabled')
                selStatsResultToolbarProcesses.empty()

                let processes = ''
                let pidsArray

                if (source.data.processes.type === 'pids') pidsArray = source.data.processes.pids
                else pidsArray = tabData.selection.processes

                pidsArray.forEach(process => {
                    const checked = selection.processes.find(lProcess => lProcess === process)

                    processes += '<li><label style="font-size: 12px;"><input id="chkStatsResultProcess-' + tabIndex + '-' + process + '" ' + (checked === undefined ? '' : ' checked ') + ' type="checkbox" onclick="app.ui.stats.results.toolbar.processClicked(\'' + process + '\', ' + tabIndex + ')">' + process + '</label></li>'
                })

                selStatsResultToolbarProcesses.append(processes)
            }

            // samples
            const ddStatsResultsSamples = $('#ddStatsResultsSamples')
            const selStatsResultToolbarSamples = $('#selStatsResultToolbarSamples')

            ddStatsResultsSamples.removeClass('disabled')
            selStatsResultToolbarSamples.empty()

            let samples = ''
            source.data.samples.forEach(sample => {
                const checked = selection.samples.find(lSample => lSample === sample)

                samples += '<li><label style="font-size: 12px;"><input id="chkStatsResultSample-' + tabIndex + '-' + sample + '" ' + (checked === undefined ? '' : ' checked ') + ' type="checkbox" onclick="app.ui.stats.results.toolbar.sampleClicked(\'' + sample + '\', ' + tabIndex + ')">' + sample + '</label></li>'
            })

            selStatsResultToolbarSamples.append(samples)

            // sample types
            const ddStatsResultsSampleTypes = $('#ddStatsResultsSampleTypes')
            const selStatsResultToolbarSampleTypes = $('#selStatsResultToolbarSampleTypes')

            ddStatsResultsSampleTypes.removeClass('disabled')
            selStatsResultToolbarSampleTypes.empty()

            let sampleTypes = ''
            app.ui.stats.results.sampleTypes.forEach(sampleType => {
                const checked = selection.sampleTypes.find(lSampleType => lSampleType === sampleType)

                sampleTypes += '<li><label style="font-size: 12px;"><input id="chkStatsResultValue-' + tabIndex + '-' + sampleType + '"' + (checked === undefined ? '' : ' checked ') + ' type="checkbox" onclick="app.ui.stats.results.toolbar.sampleTypeClicked(\'' + sampleType + '\', ' + tabIndex + ')">' + sampleType + '</label></li>'
            })

            selStatsResultToolbarSampleTypes.append(sampleTypes)
        },

        regionClicked: function (region, tabIndex) {
            const tabData = app.ui.stats.results.tabsData[tabIndex]
            const foundIx = tabData.selection.regions.findIndex(lRegion => lRegion === region)

            if (tabData.selection.regions.length === 1 && foundIx > -1) {
                $('#chkStatsResultRegion-' + tabIndex + '-' + region).prop('checked', true)

                return
            }

            if (foundIx === -1) tabData.selection.regions.push(region)
            else tabData.selection.regions.splice(foundIx, 1)

            app.ui.stats.results.table.createHeader(tabData)
        },

        processClicked: function (process, tabIndex) {
            const tabData = app.ui.stats.results.tabsData[tabIndex]
            const foundIx = tabData.selection.processes.findIndex(lProcess => lProcess === process)

            if (tabData.selection.processes.length === 1 && foundIx > -1) {
                $('#chkStatsResultProcess-' + tabIndex + '-' + process).prop('checked', true)

                return
            }

            if (foundIx === -1) tabData.selection.processes.push(process)
            else tabData.selection.processes.splice(foundIx, 1)

            app.ui.stats.results.table.createHeader(tabData)
        },

        sampleClicked: function (sample, tabIndex) {
            const tabData = app.ui.stats.results.tabsData[tabIndex]
            const foundIx = tabData.selection.samples.findIndex(lSample => lSample === sample)

            if (tabData.selection.samples.length === 1 && foundIx > -1) {
                $('#chkStatsResultSample-' + tabIndex + '-' + sample).prop('checked', true)

                return
            }

            if (foundIx === -1) tabData.selection.samples.push(sample)
            else tabData.selection.samples.splice(foundIx, 1)

            app.ui.stats.results.table.createHeader(tabData)
        },

        sampleTypeClicked: function (sampleType, tabIndex) {
            const tabData = app.ui.stats.results.tabsData[tabIndex]
            const foundIx = tabData.selection.sampleTypes.findIndex(lSampleType => lSampleType === sampleType)

            if (tabData.selection.sampleTypes.length === 1 && foundIx > -1) {
                $('#chkStatsResultValue-' + tabIndex + '-' + sampleType).prop('checked', true)

                return
            }

            if (foundIx === -1) tabData.selection.sampleTypes.push(sampleType)
            else tabData.selection.sampleTypes.splice(foundIx, 1)

            app.ui.stats.results.table.createHeader(tabData)
        },
    },

    tabsData: [],
    tabIx: 0,
    sampleTypes: [
        'Abs',
        'Δ',
        'Δ Avg',
        'Δ Min',
        'Δ Max'
    ]
}
