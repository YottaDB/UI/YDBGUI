/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// This file manages the creation / edit /delete of sources

app.ui.stats.sources = {
    init: function () {
        $('#btnStatsSourcesYgbStatsAdd').on('click', () => this.addClicked('ygbl'))
        $('#btnStatsSourcesFheadAdd').on('click', () => this.addClicked('fhead'))
        $('#btnStatsSourcesPeekByNameAdd').on('click', () => this.addClicked('peekByName'))
        $('#btnStatsSourcesEdit').on('click', () => this.editClicked())
        $('#btnStatsSourcesDelete').on('click', () => this.deleteClicked())
        $('#btnStatsSourcesClearAll').on('click', () => this.clearAllClicked())
        $('#btnStatsSourcesHelp').on('click', () => app.ui.help.show('stats/sources'))

        $('#tblStatsSources')
            .on('click', 'tbody tr', function () {
                $(this).addClass('table-row-highlight').siblings().removeClass('table-row-highlight')

                app.ui.button.enable($('#btnStatsSourcesDelete'))
                app.ui.button.enable($('#btnStatsSourcesEdit'))

            })
            .on('dblclick', 'tbody tr', () => {
                app.ui.button.enable($('#btnStatsSourcesDelete'))
                app.ui.button.enable($('#btnStatsSourcesEdit'))

                this.editClicked()
            })

        $('#btnStatsSourcesOk').on('click', () => this.okClicked())

        this.data = app.ui.storage.get('stats')
        if (this.data === null) this.data = []

        app.ui.setupDialogForTest('modalStatsSources')

        // set the disabled status of the New icon in the toolbar
        $('#modalStatsSources').on('hide.bs.modal', () => {
            app.ui.button.set($('#btnStatsNew'), app.ui.stats.entry.data.length === 0 ? app.ui.button.DISABLED : app.ui.button.ENABLED)
        })

    },

    show: function () {
        $('#modalStatsSourcesTitle').text('Sources')

        this.entry = JSON.parse(JSON.stringify(app.ui.stats.entry))

        this.refresh()

        $('#modalStatsSources')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    addClicked: function (type) {
        switch (type) {
            case 'ygbl': {
                app.ui.stats.sources.ygblstats.show()

                break
            }
            case 'fhead':
            case 'peekByName': {
                app.ui.stats.sources.fheadPeekByName.show(type)
            }
        }
    },

    editClicked: function () {
        const rows = $('#tblStatsSources tbody > tr')
        let sel

        if (rows.length === 1) sel = rows[0].id
        else sel = $('#tblStatsSources tbody').find('.table-row-highlight').attr('id')

        if (sel) {
            const selIx = sel.split('-')[1]
            const type = $('#tblStatsSources tbody').find('.table-row-highlight').attr('data-type')

            switch (type) {
                case 'YGBL': {
                    app.ui.stats.sources.ygblstats.show(selIx)

                    break
                }
                case 'FHEAD': {
                    app.ui.stats.sources.fheadPeekByName.show('fhead', selIx)

                    break
                }
                case 'PEEKBYNAME': {
                    app.ui.stats.sources.fheadPeekByName.show('peekByName', selIx)

                    break
                }
            }

        } else {
            app.ui.msgbox.show('Nothing is selected', app.ui.msgbox.INPUT_ERROR)
        }
    },

    deleteClicked: function () {
        const sel = $('#tblStatsSources tbody').find('.table-row-highlight').attr('id')
        if (sel) {
            const selIx = sel.split('-')[1]
            if (app.ui.stats.sources.entry.data[selIx].data.views.length > 0) {
                app.ui.inputbox.show('This source has views in the Spark Chart. By deleting it it will delete the views as well.<br><br>Are you sure you want this?', 'WARNING', sel => {
                    if (sel === 'YES') {
                        app.ui.stats.sources.entry.data.splice(selIx, 1)
                        app.ui.stats.sources.refresh()
                    }
                })

            } else {
                app.ui.stats.sources.entry.data.splice(selIx, 1)
                app.ui.stats.sources.refresh()
            }

        } else {
            app.ui.msgbox.show('Nothing is selected', app.ui.msgbox.INPUT_ERROR)
        }
    },

    clearAllClicked: function () {
        this.entry = app.ui.stats.utils.createNewEntry()

        this.refresh()

        $('#modalStatsSourcesTitle').text('Sources: [untitled]')
    },

    okClicked: function () {
        // if type = graph, we need to check if we reached the overall limit of 2 graphs...
        let graphCount = 0
        this.entry.data.forEach(source => {
            if (source.data.enabled === true) {
                source.data.views.forEach(view => {
                    if (view.type === 'graph') graphCount++
                })
            }
        })

        if (graphCount > 2) {
            app.ui.msgbox.show('You have more than two graphs in this selection.<br><br>Disable one or more sources', 'Warning')

            return
        }

        app.ui.stats.entry = this.entry

        $('#modalStatsSources').modal('hide')

        app.statistics.initData()
        app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))

        app.ui.stats.tab.toolbar.status = this.sourcesStatus() ? 'stopped' : 'noSources'
        app.ui.stats.tab.toolbar.computeToolbarStatus()
        app.ui.stats.utils.computeStatusBarSampleRate()

        $('#lblStatsReportName').html('[' + app.ui.stats.entry.name + '<span style="color: red;">*</span>]')
        app.ui.stats.entryDirty = true
    },

    sourcesStatus: function (withViews = true) {
        let enabled = false

        app.ui.stats.entry.data.forEach(item => {
            if (withViews === true) {
                if (item.data.enabled === true && item.data.views.length > 0) {
                    item.data.views.forEach(view => {
                        if (app.ui.stats.utils.mapObjToArray(view).length > 0) enabled = true
                    })
                }

            } else {
                if (item.data.enabled === true) enabled = true

            }
        })

        return app.ui.stats.entry.data.length > 0 && enabled === true
    },

    checkBoxClicked: function (ix) {
        this.entry.data[ix].data.enabled = !this.entry.data[ix].data.enabled
    },

    refresh: function () {
        const tblStatsSources = $('#tblStatsSources > tbody')
        let rows = ''

        tblStatsSources.empty()

        this.entry.data.forEach((source, ix) => {
                rows += '<tr id="row-' + ix + '" data-type="' + source.type.toUpperCase() + '">'

            // check box
            rows += '<td>' +
                '<div class="custom-control custom-checkbox" style="text-align: center;">' +
                '<input class="custom-control-input" id="sourcesEntryEnabled-' + ix + '" type="checkbox"' + (source.data.enabled === true ? ' checked' : '') +
                ' onclick="app.ui.stats.sources.checkBoxClicked(' + ix + ')">' +
                '<label class="custom-control-label" for="sourcesEntryEnabled-' + ix + '"></label>' +
                '</div>' +
                '</td>'

            // type
            const type = source.type === 'ygbl' ? 'YGBLSTAT' : source.type.toUpperCase()
            rows += '<td style="text-align: center;">' + type + '</td>'

            // samples
            rows += '<td>' + source.data.samples.join(', ') + '</td>'

            // type
            rows += '<td>'
            if (source.type === 'ygbl') {
                switch (source.data.processes.type) {
                    case 'agg':
                        rows += 'Aggregate';
                        break
                    case 'pids':
                        rows += 'PIDs: ' + source.data.processes.pids.join(', ');
                        break
                    case 'top':
                        rows += 'Top ' + source.data.processes.count + ' of ' + source.data.processes.sampleCount
                }
            }
            rows += '</td>'

            // regions
            rows += '<td>' + (source.type === 'ygbl' ? source.data.regions.join(', ') : source.data.regions) + '</td>'

            // sample rate
            rows += '<td>' + source.sampleRate.toFixed(3) + '</td>'

            rows += '</tr>'
            }
        )

        tblStatsSources.append(rows)

        app.ui.button.set($('#btnStatsSourcesDelete'), this.entry.data.length === 0 ? app.ui.button.DISABLED : app.ui.button.ENABLED)
        app.ui.button.set($('#btnStatsSourcesEdit'), this.entry.data.length === 0 ? app.ui.button.DISABLED : app.ui.button.ENABLED)
        app.ui.button.set($('#btnStatsSourcesSave'), this.entry.data.length === 0 ? app.ui.button.DISABLED : app.ui.button.ENABLED)
        app.ui.button.set($('#btnStatsSourcesClearAll'), this.entry.data.length === 0 ? app.ui.button.DISABLED : app.ui.button.ENABLED)
    },

    entry: {}
}
