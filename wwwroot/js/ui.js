/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// ************************************
// add handlers
// ************************************

app.ui.addHandlers = () => {
    // permalink
    app.ui.permalinks.init()

    // login
    app.ui.login.init()

    // msgbox
    app.ui.msgbox.init();

    // input box
    app.ui.inputbox.init();

    // wait
    app.ui.wait.init();

    // help
    app.ui.help.init();

    // dashboard
    app.ui.dashboard.init();

    // systemInfo
    app.ui.systemInfo.init();

    //deviceInfo
    app.ui.deviceInfo.init();
    app.ui.deviceInfo.treeMap.init();

    // region view
    app.ui.regionView.init();

    //region add
    app.ui.regionAdd.init();
    app.ui.regionAdd.name.init();
    app.ui.regionFilename.init();

    //region edit
    app.ui.regionEdit.init();
    app.ui.regionEdit.Overview.init();

    // region names
    app.ui.regionNames.add.init();

    // region delete
    app.ui.regionDelete.init();

    // region select
    app.ui.regionSelect.init();

    // region extend
    app.ui.regionExtend.init();

    // regionJournalFiles
    app.ui.regionJournalFiles.init();

    // regionCreateDbFile
    app.ui.regionCreateDbFile.init();

    // region defrag
    app.ui.defrag.init();

    // region integ
    app.ui.integ.init();

    // region backup
    app.ui.backup.init();

    // locksManager
    app.ui.locksManager.init();
    app.ui.locksManager.filter.init();

    // local storage
    app.ui.storage.init();

    // menu handlers
    app.ui.menu.init();

    // tabs
    app.ui.tabs.init();

    // dirSelect
    app.ui.dirSelect.init();

    // fileSelect
    app.ui.fileSelect.init();

    // globals
    app.ui.globalsManager.init();
    app.ui.gViewer.moreItems.init();
    app.ui.gViewer.settings.init();
    app.ui.gViewer.settings.confirm.init();
    app.ui.globalSize.init()

    // routines
    app.ui.rViewer.select.init();
    app.ui.rViewer.searchPath.init();
    app.ui.rViewer.searchPath.initAdd();

    // global Mapping
    app.ui.globalMappings.init();

    // octo
    app.ui.octoViewer.init();
    app.ui.octoTree.init();

    // env settings
    app.ui.envSettings.init()
    app.ui.gldFileSelect.init()
    app.ui.cwdSelector.init()

    //prefs
    app.ui.prefs.init()
    app.ui.prefs.rangePercent.init()
    app.ui.prefs.rangeExt.init()
    app.ui.envEnvVars.init()

    // stats
    app.ui.stats.init()
    app.ui.stats.sources.init()
    app.ui.stats.sources.ygblstats.init()
    app.ui.stats.storage.save.init()
    app.ui.stats.storage.load.init()
    app.ui.stats.sparkChart.init()
    app.ui.stats.sparkChart.mapping.init()
    app.ui.stats.sparkChart.graph.init()
    app.ui.stats.sparkChart.graph.series.init()
    app.ui.stats.sparkChart.graphSettings.init()
    app.ui.stats.sparkChart.reportLine.init()
    app.ui.stats.sparkChart.highlighters.init()
    app.ui.stats.sparkChart.renderer.sections.graphs.tools.range.init()
    app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.init()
    app.ui.stats.pids.init()
    app.ui.stats.results.init()
    app.ui.stats.results.table.init()
    app.ui.stats.results.export.init()
    app.ui.stats.results.sourceProperties.init()
    app.ui.stats.sources.fheadPeekByName.init()

    // replication
    app.ui.replication.instanceFile.init()
    app.ui.replication.logs.init()
    app.ui.replication.storage.save.init()
    app.ui.replication.storage.load.init()
}

// ************************************
// msgbox
// ************************************
app.ui.msgbox.init = () => {
    $('#btnMsgboxOk').on('click', (e) => app.ui.msgbox.hide());
    // TEST
    app.ui.setupDialogForTest('modalMsgbox')
    $('#modalMsgbox').on('shown.bs.modal', () => {
        $('#btnMsgboxOk').focus()
        $('#modalMsgbox').css('animation-name', 'shown')
    });
};

app.ui.msgbox.show = (msg, title = '', withPromise = false) => {
    $('#txtMsgboxText').html(msg);
    $('#txtMsgboxTitle').text(title !== '' && title !== undefined ? title : 'WARNING');

    const msgboxHeader = $('#msgboxHeader');
    switch (title) {
        case 'ERROR':
        case 'FATAL': {
            msgboxHeader.css('background', 'var(--ydb-status-red)');
            msgboxHeader.css('color', 'white');
            break

        }
        case 'WARNING': {
            msgboxHeader.css('background', 'var(--ydb-status-amber)');
            msgboxHeader.css('color', 'var(--jhTertiary3)');
            break

        }
        default: {
            msgboxHeader.css('background', 'var(--ydb-status-green)');
            msgboxHeader.css('color', 'white');
        }
    }

    $('#modalMsgbox').modal({show: true, backdrop: 'static', keyboard: true});
    $('#modalMsgBox').css('z-index', '9997483647')
    if (withPromise) {
        return new Promise(function (resolve) {
            $('#modalMsgbox').on('hide.bs.modal', () => {
                resolve()
            })
        })
    }
};

app.ui.msgbox.hide = () => {
    $('#modalMsgbox').modal({show: false});
};

app.ui.msgbox.INPUT_ERROR = 'Input error, please fix'


// ************************************
// msgbox with checkbox
// ************************************
app.ui.msgboxCheck.init = () => {
    $('#btnMsgboxCheckOk').on('click', (e) => app.ui.msgbox.hide());
    // TEST
    app.ui.setupDialogForTest('modalMsgboxCheck')
    $('#modalMsgboxCheck').on('shown.bs.modal', () => {
        $('#btnMsgboxCheckOk').focus()
        $('#modalMsgboxCheck').css('animation-name', 'shown')
    });
};

app.ui.msgboxCheck.show = (msg, title = '', checkCaption = '', checkStatus = false) => {
    $('#txtMsgboxCheckText').html(msg);
    $('#txtMsgboxCheckTitle').text(title !== '' && title !== undefined ? title : 'WARNING');

    const msgboxHeader = $('#msgboxCheckHeader');

    switch (title) {
        case 'ERROR':
        case 'FATAL': {
            msgboxHeader.css('background', 'var(--ydb-status-red)');
            msgboxHeader.css('color', 'white');
            break

        }
        case 'WARNING': {
            msgboxHeader.css('background', 'var(--ydb-status-amber)');
            msgboxHeader.css('color', 'var(--ydb-status-amber)');
            break

        }
        default: {
            msgboxHeader.css('background', 'var(--ydb-status-green)');
            msgboxHeader.css('color', 'white');
        }
    }

    $('#lblMsgboxCheck').text(checkCaption)

    $('#chkMsgboxCheck').attr('checked', checkStatus)

    $('#modalMsgboxCheck').modal({show: true, backdrop: 'static', keyboard: true});
    $('#modalMsgboxCheck').css('z-index', '9997483647')

    return new Promise(function (resolve) {
        $('#modalMsgboxCheck').on('hide.bs.modal', () => {
            resolve($('#chkMsgboxCheck').is(':checked'))
        })
    })
};

app.ui.msgboxCheck.hide = () => {
    $('#modalMsgboxCheck').modal({show: false});
};

// ************************************
// Inputbox
// ************************************

app.ui.inputbox.init = () => {
    $('#btnInputboxYes').on('click', (e) => app.ui.inputbox.yesSelected());
    $('#btnInputboxNo').on('click', (e) => app.ui.inputbox.noSelected());

    app.ui.setupDialogForTest('modalInputbox')
    $('#modalInputbox').on('shown.bs.modal', () => {
        $('#modalInputbox').css('animation-name', 'shown')
        $('#btnInputboxNo').focus()
    });
};

app.ui.inputbox.show = (msg, title, callback, options) => {
    options = options || {};

    if (options.buttons === undefined) {
        options.buttons = {
            left: 'Yes', right: 'No'
        }
    }

    const inputboxHeader = $('#inputboxHeader');
    switch (title) {
        case 'ERROR':
        case 'WARNING':
        case 'FATAL': {
            inputboxHeader.css('background', 'var(--ydb-status-red)');
            inputboxHeader.css('color', 'white');
            break

        }
        case 'Warning': {
            inputboxHeader.css('background', 'var(--ydb-status-amber)');
            inputboxHeader.css('color', 'var(--ydb-purple)');
            break

        }
        default: {
            inputboxHeader.css('background', 'var(--ydb-status-green)');
            inputboxHeader.css('color', 'white');
        }
    }

    $('#btnInputboxYes').html(options.buttons.left);
    $('#btnInputboxNo').html(options.buttons.right);
    $('#txtInputboxText').html(msg);
    $('#txtInputboxTitle').text(title !== '' && title !== undefined ? title : 'WARNING');

    app.ui.inputbox.callback = callback;

    $('#modalInputbox').modal({show: true, backdrop: 'static'});
};

app.ui.inputbox.hide = () => {
    $('#modalInputbox').modal({show: false});
};

app.ui.inputbox.yesSelected = () => {
    app.ui.inputbox.callback("YES")
};

app.ui.inputbox.noSelected = () => {
    app.ui.inputbox.callback("NO")
};

// ************************************
// Wait
// ************************************
app.ui.wait.init = () => {
    app.ui.setupDialogForTest('modalWait')
}

app.ui.wait.show = (message, abortCallback = false, backgroundDrop = true) => {
    return new Promise(function (resolve, reject) {
        const modalWait = $('#modalWait');

        $('#txtWaitText').text(message);

        $('#divWaitAbortButton').css('display', abortCallback === false ? 'none' : 'block')

        if (abortCallback !== false) {
            $('#btnWaitAbortButton').on('click', () => {
                app.ui.wait.hide();
                abortCallback()
            })
        }

        modalWait.modal({show: true, backdrop: !backgroundDrop, keyboard: false});

        resolve();

        modalWait.on('shown.bs.modal', function (e) {
            resolve()
        });
    })
};

app.ui.wait.hide = () => {
    $('#modalWait').modal('hide');

};

// *************************************************
// Libraries
// *************************************************

app.ui.formatBytes = (a, b = 2) => {
    if (0 === a) return "0 Bytes";
    const c = 0 > b ? 0 : b, d = Math.floor(Math.log(a) / Math.log(1024));
    let value = parseFloat((a / Math.pow(1024, d)).toFixed(c)).toString().split('.');
    if (value[1] === undefined) value.push('00'); else if (value[1].length === 1) value[1] += '0';

    return value.join('.') + " " + ["bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"][d]
};

app.ui.formatThousands = x => {
    if (isNaN(x) === true && x !== 0) return x

    //inserts a comma as thousands separator each 3 chars
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
};

app.ui.getKeyValue = (array, key) => {
    if (Array.isArray(array)) {
        const flag = array.find(el => {
            return !(el[key] === undefined)
        });

        if (flag !== undefined) {
            return flag[key]
        } else return ''
    } else return ''
};

app.ui.countChar = (string, char) => {
    return string.split(char).length - 1;
};

app.ui.nthIndex = (string, pattern, n) => {
    let L = string.length, i = -1;

    while (n-- && i++ < L) {
        i = string.indexOf(pattern, i);
        if (i < 0) break;
    }
    return i;
};

app.ui.GUID = () => {
    // http://www.ietf.org/rfc/rfc4122.txt
    let s = [];
    let hexDigits = "0123456789abcdef";
    for (let i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    let uuid = s.join("");
    return uuid;
}

app.ui.crc32 = (str) => {
    const a_table = "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D";
    const b_table = a_table.split(' ').map(function (s) { return parseInt(s, 16) });
    let crc = -1;

    for (let i = 0, iTop = str.length; i < iTop; i++) {
        crc = (crc >>> 8) ^ b_table[(crc ^ str.charCodeAt(i)) & 0xFF];
    }

    return ((crc ^ (-1)) >>> 0).toString(16)
}

// TEST utils
app.ui.setupDialogForTest = dialogId => {
    $('#' + dialogId)
        .on('show.bs.modal', () => {
            $('#' + dialogId).css('animation-name', 'showing')
        })
        .on('shown.bs.modal', () => {
            $('#' + dialogId).css('animation-name', 'shown')
        })
        .on('hide.bs.modal', () => {
            $('#' + dialogId).css('animation-name', 'hiding')
        })
        .on('hidden.bs.modal', () => {
            $('#' + dialogId).css('animation-name', 'hidden')
        })
}

app.ui.setTestClosure = dialogId => {
    $('#' + dialogId).css('animation-name', 'shown')
}

app.ui.resetTestClosure = dialogId => {
    $('#' + dialogId).css('animation-name', 'hidden')
}

app.ui.getEnvVar = envvar => {
    let val = null

    app.system.systemInfo.envVars.forEach(envVar => {
        if (envVar.name === envvar) {
            val = envVar.value
        }
    })

    return val
}

app.ui.copyToClipboard = (clipboardText, $button = null) => {
    if (location.protocol === 'https:') {
        navigator.clipboard.writeText(clipboardText)

        if ($button !== null) {
            const oldText = $button.html()
            $button.html('Copied to clipboard')

            setTimeout(() => {
                $button.html(oldText)

            }, 1500, $button, oldText)
        } else {
            app.ui.msgbox.show('The text has been copied to the clipboard...', 'Notification')
        }

    } else {
        alert(clipboardText)
    }
}

app.ui.regeneratePopups = (newClass = '') => {
    // re-generate the popups
    $('[data-toggle="popover"]').popover({
        html: true,
        container: 'body',
        template: '<div class="popover" role="tooltip"><div class="arrow" style="z-index: 10000000"></div><h3 class="popover-header ydb-popover-header"></h3><div class="popover-body ' + newClass + '"></div></div>'
    });

}

app.ui.checkSystemErrors = () => {
    let found = false

    if (typeof app.system.regions === 'object') {
        Object.keys(app.system.regions).forEach((region, ix) => {
            const status = app.system.regions[region].dbFile.status.code
            const STATUS_CORRUPTED = 1
            const STATUS_SHMEM_ERROR = 2
            const STATUS_FILE_MISSING = 5

            if (status === STATUS_CORRUPTED || status === STATUS_SHMEM_ERROR || status === STATUS_FILE_MISSING) {
                found = true
            }
        })
    }

    if (found === true) {
        app.ui.msgbox.show('There are database errors preventing this functionality from being executed.<br><br>Fix the errors and try again...', 'ERROR')

        return true
    }

    return false
}

// enable / disable buttons
app.ui.button = {
    enable: function ($button) {
        $button
            .removeClass('disabled')
            .attr('disabled', false)
    },

    disable: function ($button) {
        $button
            .addClass('disabled')
            .attr('disabled', true)

    },

    set: function ($button, status) {
        switch (status) {
            case this.ENABLED:
                this.enable($button);
                break
            case this.DISABLED:
                this.disable($button);
                break
            default:
                return
        }
    },

    ENABLED: 'enabled',
    DISABLED: 'disabled'
}

app.ui.help = {
    show: function (page) {
        const path = window.location.origin + '/help/index.html?'
        window.open(path + page, 'ydbgui_help')
    },
}
