/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.deviceInfo.treeMap = {
    init: function () {
        $('#checkDeviceTreeMapDb').on('click', e => this.checkPressed(e, 'db'))
        $('#checkDeviceTreeMapDbDetails').on('click', e => this.checkPressed(e, 'dbDetails'))
        $('#checkDeviceTreeMapJournals').on('click', e => this.checkPressed(e, 'journal'))
        $('#checkDeviceTreeMapJournalsDetails').on('click', e => this.checkPressed(e, 'journalDetails'))
        $('#checkDeviceTreeMapOther').on('click', e => this.checkPressed(e, 'other'))
        $('#checkDeviceTreeMapFreeSpace').on('click', e => this.checkPressed(e, 'freeSpace'))

        // TEST
        app.ui.setupDialogForTest('modalDeviceTreeMap')
    },

    show: async function () {
        // compute disk allocation
        this.initSpace()

        app.ui.wait.show('Computing data...')
        for (const entry of app.ui.deviceInfo.device.usedBy) {
            if (entry.file.indexOf('.dat') > -1) {
                const diskSpace = app.system.regions[entry.region].dbFile.flags.fileSizeBytes

                this.space.databases.total += diskSpace
                this.space.databases.list.push({
                    file: app.system.regions[entry.region].dbFile.flags.file,
                    size: diskSpace,
                    region: entry.region
                })

            } else {
                let totSpace = 0

                // journal, get file list
                const res = await app.REST._getJournalInfo(entry.region)

                if (res.data) {
                    res.data.forEach(jFile => {
                        totSpace += jFile.fileSizeBytes
                    })

                    this.space.journals.list.push({
                        region: entry.region,
                        size: totSpace
                    })
                    this.space.journals.total += totSpace
                }
            }
        }

        app.ui.wait.hide()

        this.space.other = app.ui.deviceInfo.device.bytesUsed - this.space.journals.total - this.space.databases.total
        this.space.free = app.ui.deviceInfo.device.bytesAvailable

        $('#checkDeviceTreeMapDb').prop('checked', true)
        $('#checkDeviceTreeMapJournals').prop('checked', true)
        $('#checkDeviceTreeMapDbDetails')
            .attr('disabled', false)
            .prop('checked', false)
        $('#checkDeviceTreeMapJournalsDetails')
            .attr('disabled', false)
            .prop('checked', false)
        $('#checkDeviceTreeMapOther').prop('checked', false)
        $('#checkDeviceTreeMapFreeSpace').prop('checked', false)

        this.initChart()

        $('#modalDeviceTreeMap').modal({show: true, backdrop: 'static'});
    },

    checkPressed: function (e, type) {
        const $el = $(e.currentTarget)

        if (type === 'db') {
            $('#checkDeviceTreeMapDbDetails')
                .attr('disabled', !$el.is(':checked'))
                .prop('checked', false)
        }

        if (type === 'journal') {
            $('#checkDeviceTreeMapJournalsDetails')
                .attr('disabled', !$el.is(':checked'))
                .prop('checked', false)
        }

        this.redraw()
    },

    redraw: function () {
        const plotData = []

        // other
        if ($('#checkDeviceTreeMapOther').is(':checked') === true) {
            plotData.push({
                x: 'Other',
                y: this.space.other
            })
        }

        // free
        if ($('#checkDeviceTreeMapFreeSpace').is(':checked') === true) {
            plotData.push({
                x: 'Free',
                y: this.space.free
            })
        }

        // db all
        if ($('#checkDeviceTreeMapDb').is(':checked') === true && $('#checkDeviceTreeMapDbDetails').is(':checked') === false) {
            plotData.push({
                x: 'Databases',
                y: this.space.databases.total
            })
        }

        // db details
        if ($('#checkDeviceTreeMapDbDetails').is(':checked') === true) {
            this.space.databases.list.forEach(db => {
                plotData.push({
                    x: 'Db: ' + db.region,
                    y: db.size
                })
            })
        }

        // journal all
        if ($('#checkDeviceTreeMapJournals').is(':checked') === true && $('#checkDeviceTreeMapJournalsDetails').is(':checked') === false) {
            plotData.push({
                x: 'Journals',
                y: this.space.journals.total
            })
        }

        // journal details
        if ($('#checkDeviceTreeMapJournalsDetails').is(':checked') === true) {
            this.space.journals.list.forEach(j => {
                plotData.push({
                    x: 'Journal: ' + j.region,
                    y: j.size
                })
            })
        }

        this.chart.updateSeries([
            {
                data: plotData
            }
        ])
    },

    initChart: function () {
        this.chart = new ApexCharts(document.querySelector("#chartDeviceTreeMap"), {
            chart: {
                height: 550,
                type: 'treemap'
            },
            title: {
                text: 'Mount point: ' + app.ui.deviceInfo.device.mountPoint,
                offsetY: 5
            },
            subtitle: {
                text: 'Device ID: ' + app.ui.deviceInfo.device.id
            },
            dataLabels: {
                enabled: true,
                style: {
                    fontSize: '13px',
                },
                formatter: function (text, op) {
                    return [text, app.ui.formatBytes(op.value)]
                },
                offsetY: -4
            },
            series: [
                {
                    data: [{x: 10, y: 10}]
                }
            ],
            plotOptions: {
                treemap: {
                    enableShades: false,
                    distributed: true
                }
            },
            tooltip: {
                y: {
                    formatter: function (value, {series, seriesIndex, dataPointIndex, w}) {
                        return app.ui.formatBytes(value)
                    }
                }
            }
        });
        this.chart.render();
        this.redraw()

    },

    initSpace: function () {
        this.space = {
            databases: {
                total: 0,
                list: []
            },
            journals: {
                total: 0,
                list: []
            },
            other: 0,
            free: 0
        }
    },

    space: {},
    chart: {}
}
