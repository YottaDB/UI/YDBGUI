/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

//*********************************************
// init()
//*********************************************
app.ui.dashboard.init = () => {
    $('#lblYottadbVersion').text('');

    app.ui.dashboard.resetGlobalStatus();
    app.ui.dashboard.resetRegions();
    app.ui.dashboard.resetMenus();

    $('#optDashboardRefreshTime').on('change', () => app.ui.dashboard.autorefresh.onChange())
    app.ui.dashboard.autorefresh.onLoad()
    $('#liTabsDashboard').on('show.bs.tab', e => {
        $('#divDashboardRefreshTime').css('display', 'flex')
        $('#tabDashboardDiv').css('display', 'block')
    })
    $('#liTabsDashboard').on('hide.bs.tab', e => $('#divDashboardRefreshTime').css('display', 'none'))
};

//*********************************************
// refresh()
//*********************************************
app.ui.dashboard.flashList = [];
app.ui.dashboard.firstTimeRefresh = false

app.ui.dashboard.refresh = async () => {

    // button status
    const STATUS_HEALTHY = null;
    const STATUS_ISSUES = false;
    const STATUS_CRITICAL = true;
    const STATUS_DISABLED = -1;
    const STATUS_UNKNOWN = -2;
    const STATUS_NO_GLD = -4;
    const STATUS_ENABLED = -5;

    const JOURNAL_STATE_DISABLED = 0;
    const JOURNAL_STATE_ENABLED_OFF = 1;
    const JOURNAL_STATE_ENABLED_ON = 2;

    const REPL_STATUS_DISABLED = 0;
    const REPL_STATUS_ENABLED = 1;
    const REPL_STATUS_WASON = 2;

    app.ui.dashboard.flashList = [];

    const lblDashStatusDatabases = $('#lblDashStatusDatabases');
    const lblDashStatusJournals = $('#lblDashStatusJournals');
    const lblDashStatusReplication = $('#lblDashStatusReplication');

    if (testMode === true) {
        // **********************
        // TEST MODE
        // **********************
        try {
            app.system = await getSystemData();
            if (app.system.result === 'WARNING') {
                let errors = '';
                if (Array.isArray(app.system.data.warnings)) {
                    app.system.data.warnings.forEach(warning => {
                        errors += warning + '<br>'
                    });
                }
                app.ui.msgbox.show('The following warnings occurred while fetching the data:<br>' + errors, 'WARNING')
            }

            app.system = app.system.data

        } catch (err) {
            console.error(err);

            app.ui.wait.hide();
            app.ui.dashboard.init();

            $('#menuSystemInfo').addClass('disabled').addClass('default').removeClass('hand');
            $('#menuSystemAdministration').addClass('disabled').addClass('default').removeClass('hand');
            $('#menuDevelopment').addClass('disabled').addClass('default').removeClass('hand');

            await app.ui.msgbox.show(app.REST.parseError(err), 'ERROR', true);
            return
        }

    } else {
        // **********************
        //execute REST call
        // **********************
        await app.ui.wait.show('Fetching dashboard information...');

        try {
            const response = await app.REST._dashboardGetAll();
            if (app.system.result === 'WARNING') {
                let errors = '';
                if (Array.isArray(app.system.data.warnings)) {
                    app.system.data.warnings.forEach(warning => {
                        errors += warning + '<br>'
                    });
                }
                await app.ui.msgbox.show(app.REST.parseError(err), 'ERROR', true);

            } else if (response.result === 'ERROR') {
                app.ui.wait.hide();

                await app.ui.msgbox.show(app.REST.parseError(err), 'ERROR', true);

                return
            }

            app.system = response.data;
            app.serverMode = response.serverMode;
            app.system.remoteSettingsLoaded = false

            if (app.ui.dashboard.firstTimeRefresh === false && app.system.environment) {
                app.systemEnvironment = JSON.parse(JSON.stringify(app.system.environment))
                app.ui.dashboard.firstTimeRefresh = true
            }

        } catch (err) {
            console.error(err);

            app.ui.wait.hide();
            app.ui.dashboard.init();

            $('#menuSystemInfo').addClass('disabled').addClass('default').removeClass('hand');
            $('#menuSystemAdministration').addClass('disabled').addClass('default').removeClass('hand');
            $('#menuDevelopment').addClass('disabled').addClass('default').removeClass('hand');

            await app.ui.msgbox.show(app.REST.parseError(err), 'ERROR', true);

            return
        }
    }

    // ********************************
    // Load user setting file if needed
    // ********************************
    if (app.system.webServer && app.system.webServer.prefsConfigData !== '' && app.system.remoteSettingsLoaded === false) {
        const res = app.system.webServer.prefsConfigData
        if (typeof res === 'string') {
            // errors found
            const error = res.split('|')[0]

            switch (error) {
                case '1':
                    app.ui.msgbox.show('The client config file:<br>' + app.system.webServer.prefsConfigFile + '<br>was not found on the server.<br><br>Aborting import...', 'Warning')
                    break
                case '2':
                    app.ui.msgbox.show('A parsing error occurred while parsing the client config file:<br>' + app.system.webServer.prefsConfigFile + '<br><br>Aborting import...', 'Warning')
                    break
                case '3':
                    app.ui.msgbox.show('The client config file:<br>' + app.system.webServer.prefsConfigFile + '<br>was not found on the server.<br><br>Aborting import...', 'Warning')
            }

        } else {
            // verify file integrity
            if (res.activeJournalRanges && res.autoExtend && res.flashInterval && res.manualExtend && res.regionRanges && res.storageRanges) {
                // adjust regions if needed
                if (!res.regions) res.regions = {}

                // load the file into the settings
                app.userSettings.dashboard = res

                // and save them
                app.ui.storage.save('dashboard', app.userSettings.dashboard)

                console.log('%cThe file: ' + app.system.webServer.prefsConfigFile + ' has been imported in the preferences...', 'color: lightgreen;')
            } else {
                app.ui.msgbox.show('The client configuration file: ' + app.system.webServer.prefsConfigFile + ' was found, but the content is not correct.<br><br>Aborting import...', 'Warning')
            }
        }

        app.system.remoteSettingsLoaded = true
    }

    // **********************
    // .gld file missing
    // **********************
    if (app.system.gld.exist === false) {
        // Update screen
        app.ui.dashboard.setGlobalStatus(lblDashStatusDatabases, STATUS_NO_GLD);
        app.ui.dashboard.setGlobalStatus(lblDashStatusJournals, STATUS_UNKNOWN);
        app.ui.dashboard.setGlobalStatus(lblDashStatusReplication, STATUS_UNKNOWN);
        app.ui.dashboard.resetRegions();

        $('#menuSystemAdministrationRegion').addClass('disabled hand')
        $('#menuSystemAdministrationLocksManager').addClass('disabled hand')
        $('#menuSystemAdministrationGlobalMappings').addClass('disabled hand')
        $('#menuSystemAdministrationBackup').addClass('disabled hand')
        $('#menuSystemAdministrationIntegrity').addClass('disabled hand')
        $('#menuSystemAdministrationDefrag').addClass('disabled hand')
        $('#menuPreferences').addClass('disabled hand')

        $('#menuDevelopmentGlobalsViewerNew').addClass('disabled hand')
        $('#menuDevelopmentGlobalsViewer').addClass('disabled hand')
        $('#menuDevelopmentOcto').addClass('disabled hand')

        // Clear all device panels
        $('#divDashStorageDevice0').empty();

        // init routine search paths
        app.ui.rViewer.searchPath.initPaths()

        app.ui.wait.hide();

        app.ui.envSettings.show(true)

        return
    }

    // Process refresh
    app.ui.menu.processRefresh();

    // **********************
    // YDB version
    // **********************
    $('#lblYottadbVersion').html('<nobr>' + app.system.ydb_version + '<span style="font-size: 10px;"> ( YDBGUI v' + app.version + ' )</span></nobr>');

    // **********************
    // User Mode
    // **********************
    const popupUserMode = app.serverMode === 'RW' ? 'You have Read / Write access to the server.' : 'You have Read Only access to the server.<br>Restart the server with the \'readwrite\' flag to enter Read Write mode.';
    $('#lblUserMode').text(app.serverMode + ' mode');
    /*$('#puUserMode')
        .attr('data-original-title', app.serverMode + ' mode')
        .attr('data-content', popupUserMode);
     */

    // **********************
    // HOST name + replication instance name
    // **********************
    // check if env var exist
    let hostName = ''
    let replText = 'Not configured...'

    app.system.systemInfo.envVars.forEach(envVar => {
        if (envVar.name === 'HOSTNAME') {
            hostName = envVar.value
        }
    })

    if (app.system.replication && app.system.replication.instanceFile) {
        if (app.system.replication.instanceFile.header) {
            replText = app.system.replication.instanceFile.header.find(el => el.name === 'InstanceName').value
        }
    }

    $('#lblDashboardHostname').text(hostName)
    $('#lblDashboardReplInstanceName')
        .text(replText)
        .css('color', replText !== 'Not configured...' ? 'var(--ydb-orange)' : '(var(--ydb-darkgray)')

    // **********************
    // browser tab title
    // **********************
    document.title = replText !== 'Not configured...' ? 'YottaDB GUI: ' + replText : 'YottaDB GUI'

    app.ui.wait.hide();

    // **********************
    // Regions
    // **********************

    // Clear the table
    const tblDashRegionsBody = $('#tblDashRegions> tbody');
    $(tblDashRegionsBody).empty();


    Object.keys(app.system.regions).forEach((region, ix) => {
            const reg = app.system.regions[region];
            reg.name = region

            const DB_STATUS_HEALTHY = 0
            const DB_STATUS_CORRUPTED = 1
            const DB_STATUS_SHMEM = 2
            const DB_STATUS_FROZEN = 3
            const DB_STATUS_REORG_IN_PROGRESS = 4
            const DB_STATUS_NO_FILE = 5

            // detailed info for subsequent popups
            const result = {
                database: {},
                journaling: {},
                replication: {}
            };

            let filename = app.ui.getKeyValue(reg.dbFile.data, 'FILE_NAME');
            let row = '' +
                '<tr>' +
                '<td style="text-align: center;">' +
                '<button id="btnDashRegionView' + ix + '"" class="btn btn-outline-info btn-sm dash-plus-button" onclick="app.ui.regionView.show(\'' + region + '\')" type="button">' +
                '<i class="bi-zoom-in"></i>' +
                '</button>' +
                '</td>' +
                '<td>' + region + '</td>' +
                '<td id="txtDashboardRegionTableFilename' + ix + '" class="inconsolata">' + filename + '</td>';

            // Database status
            if (reg.dbFile.flags.fileExist) {
                if (reg.dbFile.flags.fileBad === true) {
                    // File exists and is BAD
                    result.database.caption = 'Critical';
                    result.database.class = 'ydb-status-red';
                    result.database.popup = {
                        visible: true,
                        title: 'Critical',
                        caption: 'The database file is corrupted'
                    }

                    reg.dbFile.status = {
                        code: DB_STATUS_CORRUPTED,
                        description: result.database.popup.caption
                    }

                } else {
                    if (reg.dbFile.flags.shmenHealthy === false) {
                        // shmem is BAD
                        result.database.caption = 'Critical';
                        result.database.class = 'ydb-status-red';
                        result.database.popup = {
                            visible: true,
                            title: 'Critical',
                            caption: 'The database shared memory is corrupted'
                        }

                        reg.dbFile.status = {
                            code: DB_STATUS_SHMEM,
                            description: result.database.popup.caption
                        }

                    } else {
                        // File exist and is GOOD
                        // check extension
                        const dbStatus = app.ui.dashboard.computeRegionSpace(reg);
                        if (dbStatus.class !== 'ydb-status-green') {
                            result.database = dbStatus

                            reg.dbFile.status = {
                                code: DB_STATUS_HEALTHY,
                                description: 'Healthy'
                            }

                        } else {
                            if (reg.dbFile.flags.freeze > 0) {
                                result.database.caption = 'Issues';
                                result.database.class = 'ydb-status-amber';
                                result.database.popup = {
                                    visible: true,
                                    title: 'ISSUES',
                                    caption: 'The database is frozen by user: ' + reg.dbFile.flags.freeze
                                }

                                reg.dbFile.status = {
                                    code: DB_STATUS_FROZEN,
                                    description: result.database.popup.caption
                                }

                            } else if (reg.dbFile.flags.pendingReorg === true) {
                                result.database.caption = 'Issues';
                                result.database.class = 'ydb-status-amber';
                                result.database.popup = {
                                    visible: true,
                                    title: 'Critical',
                                    caption: 'The database has an interrupted REORG status.<br>Use the REORG menu option to fix it. '
                                }

                                reg.dbFile.status = {
                                    code: DB_STATUS_REORG_IN_PROGRESS,
                                    description: result.database.popup.caption
                                }

                            } else {

                                result.database.caption = 'Healthy';
                                result.database.class = 'ydb-status-green';
                                result.database.popup = {
                                    visible: false
                                }

                                reg.dbFile.status = {
                                    code: DB_STATUS_HEALTHY,
                                    description: 'Healthy'
                                }
                            }
                        }
                    }
                }

            } else {
                result.database.caption = app.ui.getKeyValue(reg.dbFile.data, 'AUTO_DB') ? 'AutoDB, No File' : 'Critical';
                result.database.class = app.ui.getKeyValue(reg.dbFile.data, 'AUTO_DB') ? 'ydb-status-amber' : 'ydb-status-red';
                result.database.popup = {
                    visible: true,
                    title: 'Critical',
                    caption: app.ui.getKeyValue(reg.dbFile.data, 'AUTO_DB') ? 'Database file doesn\'t exist. Please create it' : 'Database file missing'
                }

                reg.dbFile.status = {
                    code: DB_STATUS_NO_FILE,
                    description: result.database.popup.caption
                }
            }

            row += '<td class="table-row-pill">' +
                (result.database.popup.visible ? '<a tabindex="-1" role="button" data-toggle="popover" data-trigger="hover" title="ISSUES" data-content="' + result.database.popup.caption + '">' : '') +
                '<div id="pillDashboardRegionTableDb' + ix + '" class="table-pill  ' + result.database.class + (result.database.popup.visible ? ' hand ' : '') + '">' +
                result.database.caption +
                '</div>' +
                (result.database.popup.visible ? '</a>' : '') +
                '</td>';

            // Journaling status
            result.journaling.badgeId = 'bdgSplashRegionsJournal' + ix;

            const replStatus = reg.replication !== undefined;

            if (reg.dbFile.flags.fileExist === false) {
                result.journaling.class = 'ydb-status-gray';
                result.journaling.caption = 'Disabled';
                result.journaling.popup = {
                    visible: false
                }

            } else {

                if (reg.journal.flags.state === 2 && (reg.journal.flags.fileExist === false || app.ui.getKeyValue(reg.journal.data, 'JFILE_NAME') === '')) {
                    result.journaling.class = 'ydb-status-red';
                    result.journaling.caption = 'NO FILE';
                    result.journaling.popup = {
                        visible: true,
                        title: 'Issues',
                        caption: 'The file is missing'
                    }

                } else {

                    if (replStatus && reg.replication.flags.status === REPL_STATUS_WASON) {
                        result.journaling.class = 'ydb-status-red';
                        result.journaling.caption = 'WAS ON';
                        result.journaling.popup = {
                            visible: true,
                            title: 'Issues',
                            caption: 'Replication is in WAS ON status'
                        }

                    } else {
                        if (replStatus && reg.replication.flags.status > 0 && reg.dbFile.flags.sessions > 0) {
                            // replicated region and active (sessions > 0)
                            if (reg.journal.flags.state === 1) {
                                result.journaling.class = 'ydb-status-red';
                                result.journaling.caption = 'Critical';
                                result.journaling.popup = {
                                    visible: true,
                                    title: 'Critical',
                                    caption: 'Journaling must be turned on in an active replicated region'
                                }

                            } else if (reg.journal.flags.state === JOURNAL_STATE_DISABLED) {
                                result.journaling.class = 'ydb-status-red';
                                result.journaling.caption = 'Critical';
                                result.journaling.popup = {
                                    visible: true,
                                    title: 'Critical',
                                    caption: 'Journaling must be enabled and turned on in an active replicated region'
                                }

                            } else {
                                result.journaling.caption = app.ui.getKeyValue(reg.journal.data, 'BEFORE') ? 'BeforeImage' : 'Nobefore Image';
                                result.journaling.popup = {
                                    visible: false
                                };

                                result.journaling.class = 'ydb-status-green';

                            }
                        } else {
                            // not replicated region

                            if (reg.journal.flags === undefined) reg.journal.flags = {state: 0};

                            if (reg.journal.flags.state === JOURNAL_STATE_DISABLED) {
                                result.journaling.class = 'ydb-status-gray';
                                result.journaling.caption = 'Disabled';
                                result.journaling.popup = {
                                    visible: false
                                }

                            } else if (reg.journal.flags.state === JOURNAL_STATE_ENABLED_OFF) {
                                result.journaling.class = 'ydb-status-amber';
                                result.journaling.caption = 'Enabled/Off';
                                result.journaling.popup = {
                                    visible: true,
                                    title: 'Issues',
                                    caption: 'Journaling needs to be turned on'
                                }

                            } else {
                                result.journaling.caption = app.ui.getKeyValue(reg.journal.data, 'BEFORE') ? 'BeforeImage' : 'Nobefore Image';
                                result.journaling.popup = {
                                    visible: false
                                };

                                result.journaling.class = 'ydb-status-green';
                            }
                        }
                    }
                }
            }

            reg.popup = result;

            row += '<td class="table-row-pill">' +
                (result.journaling.popup.visible ? '<a tabindex="-1" role="button" data-toggle="popover" data-trigger="hover" title="ISSUES" data-content="' + result.journaling.popup.caption + '">' : '') +
                '<div class="table-pill ' + result.journaling.class + (result.database.popup.visible ? ' hand ' : '') + '" id="' + result.journaling.badgeId + '">' + result.journaling.caption +
                '</div>' +
                (result.journaling.popup.visible ? '</a>' : '') +
                '</td>';

            row += '</tr>';
            tblDashRegionsBody.append(row);

            // check flash status and push into list if needed
            if (reg.replication !== undefined && replStatus && reg.replication.flags === 2) app.ui.dashboard.flashList.push($('#' + result.journaling.badgeId));
            if (result.database.flash !== undefined && result.database.flash === true) app.ui.dashboard.flashList.push($('#pillDashboardRegionTableDb' + ix));
        }
    );

    // **********************
    // Global status
    // **********************

    const status = {
        regionCount: 0,
        replication: STATUS_DISABLED,
        database: {
            each: [],
            overall: null
        },
        journaling: {
            each: [],
            eachDisabled: [],
            overall: null
        },
        popup: {
            database: '',
            journaling: '',
            replication: '',
            replBacklog: ''
        }
    };

    // Compute replication data
    status.replication = STATUS_DISABLED;

    let replStatus = {
        enabled: false,
        wasOn: false
    };

    Object.keys(app.system.regions).forEach(region => {
        const reg = app.system.regions[region];
        if (reg.replication !== undefined && reg.replication.flags.status > 0) {
            status.replication = reg.replication.flags.status === REPL_STATUS_ENABLED ? replStatus.enabled = true : replStatus.wasOn = true;
        }
    });

    if (replStatus.enabled) status.replication = STATUS_ENABLED;
    if (replStatus.wasOn) status.replication = STATUS_CRITICAL;

    // Compute region data
    Object.keys(app.system.regions).forEach(region => {
        status.regionCount++;

        const reg = app.system.regions[region];

        // Database
        if ((reg.dbFile.flags.fileExist === false) || reg.dbFile.flags.fileBad) {
            status.database.each.push(STATUS_CRITICAL);
            status.popup.database += '<strong>' + region + ':&nbsp;</strong>' + reg.popup.database.popup.caption + '<br>'
        } else {
            const dbStatus = app.ui.dashboard.computeRegionSpace(reg);
            if (dbStatus.class !== 'ydb-status-green') {
                if (dbStatus.caption === 'Critical') status.database.each.push(STATUS_CRITICAL)
                else if (dbStatus.caption === 'Issues') status.database.each.push(STATUS_ISSUES)
                else status.database.each.push(STATUS_HEALTHY)

                status.popup.database += '<strong>' + region + ':&nbsp;</strong>' + dbStatus.popup.caption + '<br>'

            } else if (reg.popup.database.popup.visible) {
                status.database.each.push(reg.popup.database.class === 'ydb-status-red' ? STATUS_CRITICAL : STATUS_ISSUES);
                status.popup.database += '<strong>' + region + ':&nbsp;</strong>' + reg.popup.database.popup.caption + '<br>'
            }

            // Journal
            if (reg.replication !== undefined && reg.replication.flags.status === REPL_STATUS_WASON) {
                status.journaling.each.push(STATUS_CRITICAL);
                status.journaling.overall = STATUS_CRITICAL;

            } else if (reg.journal.flags.state === 1 && status.journaling === null) {
                status.journaling.each.push(STATUS_ISSUES);

            } else if (reg.journal.flags.state === 0) {
                status.journaling.eachDisabled.push(STATUS_DISABLED);

            } else if (reg.popup.journaling.popup.visible) {
                status.popup.journaling += '<strong>' + region + ':&nbsp;</strong>' + reg.popup.journaling.popup.caption + '<br>';
                status.journaling.each.push(STATUS_ISSUES);
            }
        }
    });

    if (status.database.each.length === 0) {
        status.database.overall = STATUS_HEALTHY
    } else {
        const globalStatus = {issues: false, critical: false}

        status.database.each.forEach(status => {
            if (status === STATUS_ISSUES) globalStatus.issues = true
            if (status === STATUS_CRITICAL) globalStatus.critical = true
        })
        if (globalStatus.critical === true) status.database.overall = STATUS_CRITICAL
        else if (globalStatus.issues === true) status.database.overall = STATUS_ISSUES
        else status.database.overall = STATUS_HEALTHY

    }

    if (status.journaling.each.length === 0) {
        status.journaling.overall = STATUS_HEALTHY;

        if (status.journaling.eachDisabled.length === status.regionCount) {
            let allDisabled = true

            status.journaling.eachDisabled.forEach(status => {
                if (status !== STATUS_DISABLED) allDisabled = false
            })
            if (allDisabled === true) status.journaling.overall = STATUS_DISABLED
        }
    } else {
        status.journaling.overall = status.journaling.overall === STATUS_CRITICAL ? STATUS_CRITICAL : (status.journaling.each.length === status.regionCount ? STATUS_ISSUES : false);
    }

    // Update screen
    app.ui.dashboard.setGlobalStatus(lblDashStatusDatabases, status.database.overall);
    app.ui.dashboard.setGlobalStatus(lblDashStatusJournals, status.journaling.overall);
    app.ui.dashboard.setGlobalStatus(lblDashStatusReplication, status.replication);

    // **********************************************************
    // process replication popup and backlog visibility and data
    // **********************************************************
    if (app.system.replication) {
        let replPopup = ''
        let backlogPopup = ''

        // replication
        replPopup += '<div class="row" style="display: flex; width: 200px!important;">'
        replPopup += '<div class="col-6"><strong>Instance:</strong></div><div class="col-6 align-right">' + app.system.replication.instanceFile.header.find(el => el.name === 'InstanceName').value + '</div>'
        replPopup += '</div>'
        replPopup += '<div class="row" style="display: flex;">'
        replPopup += '<div class="col-4"><strong>Role:</strong></div><div class="col-8 align-right">' + (app.system.replication.instanceFile.flags.isSupplementary === 1 ? 'Supplementary' : app.system.replication.instanceFile.flags.isPrimaryRoot === 1 ? 'Primary root' : 'Secondary') + '</div>'
        replPopup += '</div>'
        replPopup += '<div class="row" style="display: flex;">'
        replPopup += '<div class="col-6"><strong>R/W:</strong></div><div class="col-6 align-right">' + (app.system.replication.instanceFile.flags.isReadWrite === 1 ? 'Yes' : 'No') + '</div>'
        replPopup += '</div>'
        // next line is filling a row with non-breaking spacing, to extend the popup
        replPopup += '&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'

        $('#lblDashStatusReplicationPopup')
            .attr('data-original-title', 'Instance info')
            .attr('data-content', replPopup)
            .css('cursor', 'pointer')

        // backlog
        if (app.userSettings.defaults.replication.dashboard.refreshBacklog === 'off') {
            $('#spanDashboardStatusReplicationBacklog').css('display', 'none')

            app.ui.dashboard.refreshBacklog.stop()

        } else {
            $('#spanDashboardStatusReplicationBacklog').css('display', 'inline')
            $('#lblDashStatusReplication2').css('display', 'inline')

            app.ui.dashboard.refreshBacklog.update(app.system.replication.backlog)

            app.ui.dashboard.refreshBacklog.start()
        }

    } else {
        $('#spanDashboardStatusReplicationBacklog').css('display', 'none')
        $('#lblDashStatusReplicationPopup')
            .attr('title', '')
            .data('content', '')
            .css('cursor', 'default')
    }

    // Popups
    const lblDashStatusDatabasesPopup = $('#lblDashStatusDatabasesPopup');
    const lblDashStatusJournalsPopup = $('#lblDashStatusJournalsPopup');

    if (status.popup.database === '') {
        lblDashStatusDatabasesPopup.attr('role', '');
        lblDashStatusDatabasesPopup.attr('data-content', '');
        lblDashStatusDatabasesPopup.attr('data-toggle', '');

        lblDashStatusDatabases.removeClass('hand');

    } else {
        lblDashStatusDatabasesPopup.attr('role', 'button');
        lblDashStatusDatabasesPopup.attr('data-content', status.popup.database);
        lblDashStatusDatabasesPopup.attr('data-toggle', 'popover');

        lblDashStatusDatabases.addClass('hand');
    }

    if (status.popup.journaling === '') {
        lblDashStatusJournalsPopup.attr('role', '');
        lblDashStatusJournalsPopup.attr('data-content', '');
        lblDashStatusJournalsPopup.attr('data-toggle', 'popover');

        lblDashStatusJournals.removeClass('hand');

    } else {
        lblDashStatusJournalsPopup.attr('role', 'button');
        lblDashStatusJournalsPopup.attr('data-content', status.popup.journaling);
        lblDashStatusJournalsPopup.attr('data-toggle', 'popover');

        lblDashStatusJournals.addClass('hand');
    }

    // **********************
    // Storage
    // **********************

    // Clear all panels
    $('#divDashStorageDevice0').empty();

    // Loop through the devices
    app.system.devices.forEach((device, ix) => {

        let chunk = '<div class="bg-white rounded-lg p-3 shadow ydb-back6">' +
            '<div class="row">' +
            '<div class="col-2 ydb-fore3">' +
            '<b>' + (ix === 0 ? 'Storage Usage:' : '') + '</b>' +
            '</div>' +
            '<div class="col-1">' +
            '<button class="btn btn-outline-info btn-sm dash-plus-button" type="button" id="btnDashStorageView' + ix + '"><i class="bi-zoom-in"></i></button>' +
            '</div>' +
            '<div class="col-4">' +
            '<span id="lblDashStorageList' + ix + '" style="font-size: 12px"></span></div>' +
            '<div class="col-5">' +
            '<a tabindex="-1" role="button" data-toggle="popover" data-trigger="hover" title="Device info" id="hlpDashStorageUsage' + ix + '">' +
            '<div class="progress" style="height: 20px;">' +
            '<div class="progress-bar ydb-status-green hand" role="progressbar" id="pgsDashStorageUsage' + ix + '" style="width: 23%; color: white; font-size: 15px; padding: 2px;"></div>' +
            '</div></a></div></div></div>';

        $('#divDashStorageDevice0').append(chunk);

        $('#btnDashStorageView' + ix).on('click', () => app.ui.deviceInfo.show(ix));
        // usedBy list
        let usedByString = '';
        device.usedBy.forEach(usedByElement => {
            usedByString += '<span class="' + (usedByElement.file.slice(-3) === 'dat' ? 'ydb-purple-fore' : 'ydb-orange-fore') + '">'
            usedByString += '<strong>' + usedByElement.region + ':</strong> <span class="inconsolata">' + usedByElement.file + '</span><BR></span>'
        });
        $('#lblDashStorageList' + ix).html(usedByString);

        // range
        const pgsDashStorageUsage = $('#pgsDashStorageUsage' + ix);
        pgsDashStorageUsage
            .css('width', device.percentUsed + '%')
            .text(device.percentUsed + ' %');

        const rangeStyle = {};
        app.userSettings.dashboard.storageRanges.forEach(el => {
            if (rangeStyle.class) return

            if (device.percentUsed >= el.min - 1 && device.percentUsed <= el.max) {
                rangeStyle.class = el.class;
                rangeStyle.flash = el.flash;
            }
        });

        // range help
        let help = '<strong>Mount point: </strong>' + device.mountPoint + '<br>';
        const divideFactor = device.fsBlockSize / 1024;
        help += '<strong>Total blocks: </strong>' + app.ui.formatThousands(device.totalBlocks / divideFactor) + ' (' + app.ui.formatBytes(device.totalBlocks * 1024) + ')<br>';
        help += '<strong>Used blocks: </strong>' + app.ui.formatThousands(device.usedBlocks / divideFactor) + ' (' + app.ui.formatBytes(device.usedBlocks * 1024) + ')<br>';
        help += '<strong>Free blocks: </strong>' + app.ui.formatThousands(device.freeBlocks / divideFactor) + ' (' + app.ui.formatBytes(device.freeBlocks * 1024) + ')';

        let title = '';
        switch (rangeStyle.class) {
            case 'ydb-status-red': {
                title = 'Critical : Low space condition';
                help = 'You are running out of disk space on this device. Take action immediately !<br>' + help;
                break
            }

            case 'ydb-status-amber': {
                title = 'Warning : Low space condition';
                help = 'This device is running low on disk space. <br>' + help;
                break
            }

            default: {
            }
        }

        $('#hlpDashStorageUsage' + ix)
            .attr('title', title)
            .attr('data-content', help);

        // Perform the refresh
        pgsDashStorageUsage
            .removeClass('ydb-status-red ydb-status-green ydb-status-amber ydb-status-gray')
            .addClass(rangeStyle.class);

        // push element in flash list for flashing effect
        if (rangeStyle.flash) app.ui.dashboard.flashList.push(pgsDashStorageUsage);

        // display the section
        $('#divDashStorageDevice' + ix).css('display', 'block');

    });

    // re-generate the popups
    app.ui.regeneratePopups()

    // setup or reset flash as needed
    if (app.ui.dashboard.flashList.length) app.ui.dashboard.flash()
    else app.ui.dashboard.resetFlash()

    // init routine search paths
    app.ui.rViewer.searchPath.initPaths()

    // show / hide octo menu
    const menuTabsOcto = $('#menuTabsOcto')
    if (app.system.octo && app.system.octo.status === "ok") {
        menuTabsOcto.removeClass('disabled').removeClass('default').addClass('hand')
    } else {
        menuTabsOcto.addClass('disabled').addClass('default').removeClass('hand')
    }

    // show / hide replication menu
    const menuTabsTopology = $('#menuTabsTopology')
    if (app.system.replication) {
        menuTabsTopology.removeClass('disabled').removeClass('default').addClass('hand')
    } else {
        menuTabsTopology.addClass('disabled').addClass('default').removeClass('hand')
    }

    // TEST closure
    app.ui.setTestClosure('modalDashboard')
};

app.ui.dashboard.setGlobalStatus = (element, statusValue) => {
    let className;
    let caption;

    const STATUS_HEALTHY = null;
    const STATUS_ISSUES = false;
    const STATUS_CRITICAL = true;
    const STATUS_DISABLED = -1;
    const STATUS_UNKNOWN = -2;
    const STATUS_NO_GLD = -4;
    const STATUS_ENABLED = -5;

    if (statusValue === STATUS_HEALTHY) {
        className = 'ydb-status-green';
        caption = 'Healthy';

    } else if (statusValue === STATUS_ISSUES) {
        className = 'ydb-status-amber';
        caption = 'Issues';

    } else if (statusValue === STATUS_DISABLED) {
        className = 'ydb-status-gray';
        caption = 'Disabled';

    } else if (statusValue === STATUS_UNKNOWN) {
        className = 'ydb-status-gray';
        caption = 'Unknown';

    } else if (statusValue === STATUS_NO_GLD) {
        className = 'ydb-status-amber';
        caption = 'NO .gld ';

    } else if (statusValue === STATUS_ENABLED) {
        className = 'ydb-status-green';
        caption = 'Enabled';

    } else {
        className = 'ydb-status-red';
        caption = 'Critical';
    }

    element
        .removeClass('ydb-status-red ydb-status-green ydb-status-amber ydb-status-gray')
        .addClass(className)
        .text(caption);
};

// Reset code
app.ui.dashboard.resetGlobalStatus = () => {
    // Global status
    $('#lblDashStatusDatabases')
        .removeClass('ydb-status-red ydb-status-green ydb-status-amber ydb-status-gray')
        .addClass('ydb-status-gray')
        .text('Unknown');
    $('#lblDashStatusJournals')
        .removeClass('ydb-status-red ydb-status-green ydb-status-amber ydb-status-gray')
        .addClass('ydb-status-gray')
        .text('Unknown');
    $('#lblDashStatusReplication')
        .removeClass('ydb-status-red ydb-status-green ydb-status-amber ydb-status-gray')
        .addClass('ydb-status-gray')
        .text('Unknown');
};

app.ui.dashboard.resetRegions = () => {
    // Regions
    $("#tblDashRegions > tbody").empty();
};

app.ui.dashboard.resetMenus = () => {
    $('#menuSystemInfo').removeClass('disabled').removeClass('default').addClass('hand');
    $('#menuSystemAdministration').removeClass('disabled').removeClass('default').addClass('hand');
    $('#menuSystemDevelopment').removeClass('disabled').removeClass('default').addClass('hand');

};

// Flashing
app.ui.dashboard.hFlash = null

app.ui.dashboard.resetFlash = () => {
    if (app.ui.dashboard.hFlash !== null) clearInterval(app.ui.dashboard.hFlash)
}
app.ui.dashboard.flash = () => {
    let toggle = false;

    app.ui.dashboard.resetFlash()

    app.ui.dashboard.hFlash = setInterval(() => {
        if (toggle) {
            toggle = false;
            app.ui.dashboard.flashList.forEach(el => {
                el.removeClass('ydb-status-red').addClass('ydb-status-gray-no-border')
            })

        } else {
            toggle = true;
            app.ui.dashboard.flashList.forEach(el => {
                el.removeClass('ydb-status-gray-no-border').addClass('ydb-status-red')
            })

        }
    }, app.userSettings.dashboard.flashInterval)
};

// compute alerts for db file free space vs device free space
app.ui.dashboard.computeRegionSpace = region => {
    // determine what kind of check has to be done
    const extension = app.ui.getKeyValue(region.dbFile.data, 'EXTENSION_COUNT');
    const freeSpaceBytes = region.dbFile.flags.device.split(' ')[3] * 1024;
    let rangeStyle = {};
    let baseNode = app.userSettings.dashboard

    // check if there is a custom region object and adjust the pointer according
    if (baseNode.regions[region.name] !== undefined) {
        baseNode = baseNode.regions[region.name]
    }

    if (extension === 0) {
        const dbSizeBytesPercent = region.dbFile.usage.usedPercent

        baseNode.manualExtend.forEach(el => {
            if (rangeStyle.class) return

            if (dbSizeBytesPercent >= el.min - 1 && dbSizeBytesPercent <= el.max) {
                if (el.class !== 'ydb-status-green') {
                    rangeStyle = {
                        class: el.class,
                        flash: el.flash,
                        caption: el.class === 'ydb-status-red' ? 'Critical' : 'Issues',
                        popup: {
                            visible: true,
                            title: el.class === 'ydb-status-red' ? 'Critical' : 'Issues',
                            caption: 'The database is almost full, you need to extend it'
                        }
                    };

                } else {
                    rangeStyle = {
                        class: el.class,
                        flash: el.flash,
                        caption: 'Healthy',
                        popup: {
                            visible: false,
                            title: '',
                            caption: ''
                        }
                    }
                }
            }
        });

    } else {
        const extensionBytes = extension * app.ui.getKeyValue(region.dbFile.data, 'BLOCK_SIZE');
        const maxNumberOfExtensions = 99999999
        let numberOfExtensions = Math.round(freeSpaceBytes / extensionBytes);

        if (numberOfExtensions < 0) numberOfExtensions = 0;

        baseNode.autoExtend.forEach(el => {
            if (rangeStyle.class) return
            if (el.max === 101) el.max = maxNumberOfExtensions

            if (numberOfExtensions >= el.min - 1 && numberOfExtensions <= el.max) {
                if (el.class !== 'ydb-status-green') {
                    rangeStyle = {
                        class: el.class,
                        flash: el.flash,
                        caption: el.class === 'ydb-status-red' ? 'Critical' : 'Issues',
                        popup: {
                            visible: true,
                            title: 'ydb-status-red' ? 'Critical' : 'Issues',
                            caption: 'You have ' + numberOfExtensions + ' extensions left on this device'
                        }
                    };

                } else {
                    rangeStyle = {
                        class: el.class,
                        flash: el.flash,
                        caption: 'Healthy',
                        popup: {
                            visible: false,
                            title: '',
                            caption: ''
                        }
                    }
                }
            }
        });
    }

    return rangeStyle
};

app.ui.dashboard.autorefresh = {
    onLoad: function () {
        // select current value
        $('#optDashboardRefreshTime option[value="' + app.userSettings.defaults.mainScreenRefresh + '"]').attr("selected", "selected")
    },

    onChange: function () {
        // save value in storage
        app.userSettings.defaults.mainScreenRefresh = $('#optDashboardRefreshTime').find(':selected').val()
        app.ui.storage.save('defaults', app.userSettings.defaults)

        // and refresh the timeout routine
        dashboardTimeout.set()
    },
}

app.ui.dashboard.refreshBacklog = {
    start: function () {
        this.handle = setInterval(async () => {

            try {
                const res = await app.REST._replGetBacklog()

                this.update(res.data.backlog)

            } catch (err) {
                console.log(err)
            }

        }, parseInt(app.userSettings.defaults.replication.dashboard.refreshBacklog) * 1000)
    },

    stop: function () {
        if (this.handle === 0) return
        clearInterval(this.handle)

        this.handle = 0
    },

    update: function (backlog) {
        app.system.replication.backlog = backlog
        let backlogPopup = ''
        let backClass = 'repl-pill-bgnd-green'
        let foreClass = 'repl-pill-fore-white'

        //***********************************
        // backlog status
        //***********************************
        if (this.previous.source) {
            // receiver if exists
            if (backlog.receiver) {
                // select activity class
                if (backlog.receiver.data.total > 0) backClass = 'repl-pill-bgnd-amber'

                // range status
                backlog.receiver.status = backlog.receiver.data.total >= this.previous.receiver.data.total && this.previous.receiver.data.total > 0
                if (backlog.receiver.status === true && app.userSettings.defaults.replication.dashboard.processRange === true) foreClass = 'repl-pill-fore-red'
            }

            // streams
            backlog.source.data.status = []
            backlog.source.data.streams.forEach((stream, ix) => {
                if (app.system.replication.instanceFile.slots[ix] && app.system.replication.instanceFile.slots[ix].state !== 0) {
                    // select activity class
                    if (stream > 0) backClass = 'repl-pill-bgnd-amber'

                    // range status
                    if (stream >= this.previous.source.data.streams[ix] && stream > 0 && app.userSettings.defaults.replication.dashboard.processRange === true) foreClass = 'repl-pill-fore-red'
                    backlog.source.data.status.push(stream >= this.previous.source.data.streams[ix] && stream > 0)
                }
            })
        }

        //***********************************
        // build the popup
        //***********************************
        if (this.previous.source && app.system.replication.backlog.source) {
            app.system.replication.instanceFile.slots.forEach((slot, ix) => {
                if (app.system.replication.instanceFile.slots[ix].state !== 0) {
                    backlogPopup += '<div class="row" style="display: flex;">'
                    backlogPopup += '<div class="col-7 pu-width-100 ' +
                        (backlog.source.data.status[ix] === true && app.userSettings.defaults.replication.dashboard.processRange === true ? 'repl-pill-fore-red' : '') +
                        '"><strong>' + slot['Secondary Instance Name'] + ':</strong></div><div class="col-5 align-right pu-width-250 ' +
                        (backlog.source.data.status[ix] === true && app.userSettings.defaults.replication.dashboard.processRange === true ? 'repl-pill-fore-red' : '') +
                        '">' + app.ui.formatThousands(app.system.replication.backlog.source.data.streams[ix]) + '</div>'
                    backlogPopup += '</div>'
                }
            })
        }

        if (app.system.replication.backlog.receiver) {
            if (app.system.replication.backlog.source) backlogPopup += '---------------------------<br>'
            backlogPopup += '<div class="row" style="display: flex;">'
            backlogPopup += '<div class="col-7 pu-width-100 ' +
                (backlog.receiver.status === true && app.userSettings.defaults.replication.dashboard.processRange === true ? 'repl-pill-fore-red' : '') +
                '"><strong>Receiver:</strong></div><div class="col-5 align-right pu-width-250 ' +
                (backlog.receiver.status === true && app.userSettings.defaults.replication.dashboard.processRange === true ? 'repl-pill-fore-red' : '') + '">' +
                app.ui.formatThousands(app.system.replication.backlog.receiver.data.total) + '<div>'
            backlogPopup += '</div>'
        }

        $('#lblDashStatusDatabasesBacklog')
            .attr('data-original-title', 'Backlog')
            .attr('data-content', backlogPopup)
            .css('cursor', 'pointer')

        // re-generate the popups
        app.ui.regeneratePopups()

        $('#lblDashStatusReplication2')
            .removeClass(['repl-pill-bgnd-green', 'repl-pill-bgnd-amber', 'repl-pill-fore-white', 'repl-pill-fore-red'])
            .addClass([backClass, foreClass])
            .css('display', 'in-line')

        this.previous = structuredClone(backlog)
    },

    previous: {},
    handle: 0,
    value: 0
}
