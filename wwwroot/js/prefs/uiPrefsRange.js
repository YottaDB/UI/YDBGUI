/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

Object.assign(app.ui.prefs, {
        rangePercent: {
            init: function () {
                $('#inpPrefsRangePercent1')
                    .on('change', () => app.ui.prefs.rangePercent.percentChanged(1))
                    .on('keydown', () => app.ui.prefs.rangePercent.blockKeyEvents())
                $('#inpPrefsRangePercent2')
                    .on('change', () => app.ui.prefs.rangePercent.percentChanged(2))
                    .on('keydown', () => app.ui.prefs.rangePercent.blockKeyEvents())
                $('#inpPrefsRangePercent3')
                    .on('change', () => app.ui.prefs.rangePercent.percentChanged(3))
                    .on('keydown', () => app.ui.prefs.rangePercent.blockKeyEvents())
            },

            populate: function (items) {
                app.ui.prefs.rangePercent.items = items

                items[0].newValue.forEach((value, ix) => {
                    $('#tdPrefsRangePercent' + (ix + 1)).css('width', (value.max - value.min) + '%')
                    $('#lblPrefsRangePercent' + (ix + 1)).text('From ' + value.min + ' to ' + value.max + ' %')
                    if (ix < 3) $('#inpPrefsRangePercent' + (ix + 1)).val(value.max)
                })

                $('#inpPrefsRangePercent1')
                    .attr('min', 0)
                    .attr('max', items[0].newValue[1].max)

                $('#inpPrefsRangePercent2')
                    .attr('min', items[0].newValue[1].min)
                    .attr('max', items[0].newValue[2].max - 1)

                $('#inpPrefsRangePercent3')
                    .attr('min', items[0].newValue[2].min)
                    .attr('max', 98)

                $('#divPrefsItemsContainer').css('display', 'none')
                $('#divPrefsRangePercentContainer').css('display', 'block')
                $('#divPrefsRangeExtensionsContainer').css('display', 'none')

                $('#h5PrefsRangePercentHeader').text(items[0].field)

                $('#lblPrefsRangePercentHelp').text(items[0].description)

            },

            percentChanged: function (ix) {
                const items = app.ui.prefs.rangePercent.items
                const value = parseInt($('#inpPrefsRangePercent' + ix).val())

                items[0].dirty = true

                switch (ix) {
                    case 1: {
                        const nextRange = items[0].newValue[1].max

                        $('#lblPrefsRangePercent1').text('From 0 to ' + value + ' %')

                        $('#lblPrefsRangePercent2').text('From ' + (value + 1) + ' to ' + nextRange + ' %')
                        $('#inpPrefsRangePercent2').attr('min', value + 1)

                        app.ui.prefs.rangePercent.items[0].newValue[ix - 1].max = value
                        app.ui.prefs.rangePercent.items[0].newValue[ix].min = value + 1
                        app.ui.prefs.rangePercent.redrawBar()

                        break
                    }

                    case 2: {
                        const nextRange = items[0].newValue[2].max

                        $('#lblPrefsRangePercent2').text('From ' + (items[0].newValue[0].max + 1) + ' to ' + value + ' %')
                        $('#inpPrefsRangePercent1').attr('max', value - 1)

                        $('#lblPrefsRangePercent3').text('From ' + (value + 1) + ' to ' + nextRange + ' %')
                        $('#inpPrefsRangePercent3').attr('min', value + 1)

                        app.ui.prefs.rangePercent.items[0].newValue[ix - 1].max = value
                        app.ui.prefs.rangePercent.items[0].newValue[ix].min = value + 1
                        app.ui.prefs.rangePercent.redrawBar()

                        break
                    }

                    case 3: {
                        const nextRange = items[0].newValue[3].max

                        $('#inpPrefsRangePercent2').attr('max', value - 1)

                        $('#lblPrefsRangePercent3').text('From ' + (items[0].newValue[1].max + 1) + ' to ' + value + ' %')

                        $('#lblPrefsRangePercent4').text('From ' + (value + 1) + ' to ' + nextRange + ' %')

                        app.ui.prefs.rangePercent.items[0].newValue[ix - 1].max = value
                        app.ui.prefs.rangePercent.items[0].newValue[ix].min = value + 1
                        app.ui.prefs.rangePercent.redrawBar()
                    }
                }
            },

            blockKeyEvents: function (e) {
                return false
            },

            redrawBar: function () {
                app.ui.prefs.rangePercent.items[0].newValue.forEach((value, ix) => {
                    $('#tdPrefsRangePercent' + (ix + 1)).css('width', (value.max - value.min) + '%')
                })

            },

            startFlash: () => {
                let flip = true
                app.ui.prefs.rangePercent.hInterval = setInterval(() => {
                    flip = !flip

                    $('#lblPrefsRangePercentFlash').css('background', flip === true ? 'var(--ydb-status-red)' : 'white')
                    $('#tdPrefsRangePercent4').css('background', flip === true ? 'var(--ydb-status-red)' : 'white')

                }, app.userSettings.dashboard.flashInterval)

            },

            items: null,
            hInterval: null
        },

        rangeExt: {
            init: function () {
                $('#inpPrefsRangeExt1')
                    .on('change', () => app.ui.prefs.rangeExt.extChanged(1))
                    .on('keydown', () => app.ui.prefs.rangeExt.blockKeyEvents())
                $('#inpPrefsRangeExt2')
                    .on('change', () => app.ui.prefs.rangeExt.extChanged(2))
                    .on('keydown', () => app.ui.prefs.rangeExt.blockKeyEvents())
                $('#inpPrefsRangeExt3')
                    .on('change', () => app.ui.prefs.rangeExt.extChanged(3))
                    .on('keydown', () => app.ui.prefs.rangeExt.blockKeyEvents())
            },

            populate: function (items) {
                app.ui.prefs.rangeExt.items = items

                let newArray = items[0].newValue.reverse()
                newArray.forEach((value, ix) => {
                    // re-adjust the highest max value (normally it is 999999 for maximum value, but we need 101 to properly scale the bar)
                    if (value.max > 100) value.max = 101

                    $('#tdPrefsRangeExt' + (ix + 1)).css('width', (value.max - value.min) + '%')
                    if (ix === 0) $('#lblPrefsRangeExt' + (ix + 1)).text('From "unlimited" to ' + value.min + ' extensions')
                    else $('#lblPrefsRangeExt' + (ix + 1)).text('From ' + value.max + ' to ' + value.min + ' extensions')

                    if (ix < 3) $('#inpPrefsRangeExt' + (ix + 1)).val(value.min)
                })
                newArray = newArray.reverse()

                $('#inpPrefsRangeExt1')
                    .attr('min', items[0].newValue[2].max + 1)
                    .attr('max', 101)

                $('#inpPrefsRangeExt2')
                    .attr('min', items[0].newValue[1].min)
                    .attr('max', items[0].newValue[2].max - 1)
                $('#inpPrefsRangeExt3')
                    .attr('min', 2)
                    .attr('max', items[0].newValue[1].max)

                $('#divPrefsItemsContainer').css('display', 'none')
                $('#divPrefsRangePercentContainer').css('display', 'none')
                $('#divPrefsRangeExtensionsContainer').css('display', 'block')

                $('#lblPrefsRangeExtHelp').text(items[0].description)
            },

            extChanged: function (ix) {
                const items = app.ui.prefs.rangeExt.items
                const value = parseInt($('#inpPrefsRangeExt' + ix).val())

                items[0].dirty = true

                switch (ix) {
                    case 1: {
                        const nextRange = items[0].newValue[1].max + 1

                        $('#lblPrefsRangeExt1').text('From "unlimited" to ' + value + ' extensions')

                        $('#lblPrefsRangeExt2').text('From ' + (value - 1) + ' to ' + nextRange + ' extensions')
                        $('#inpPrefsRangeExt2').attr('max', value - 1)

                        app.ui.prefs.rangeExt.items[0].newValue[3].min = value
                        app.ui.prefs.rangeExt.items[0].newValue[2].max = value - 1
                        app.ui.prefs.rangeExt.redrawBar()

                        break
                    }

                    case 2: {
                        const nextRange = items[0].newValue[1].min

                        $('#inpPrefsRangeExt1').attr('min', value + 1)

                        $('#lblPrefsRangeExt2').text('From ' + (items[0].newValue[2].max) + ' to ' + value + ' extensions')

                        $('#lblPrefsRangeExt3').text('From ' + (value - 1) + ' to ' + items[0].newValue[1].min + ' extensions')
                        $('#inpPrefsRangeExt3').attr('max', value - 1)

                        app.ui.prefs.rangeExt.items[0].newValue[2].min = value
                        app.ui.prefs.rangeExt.items[0].newValue[1].max = value - 1
                        app.ui.prefs.rangeExt.redrawBar()

                        break
                    }

                    case 3: {
                        const nextRange = items[0].newValue[3].max

                        $('#inpPrefsRangeExt2').attr('min', value + 1)

                        $('#lblPrefsRangeExt3').text('From ' + (items[0].newValue[1].max) + ' to ' + value + ' extensions')

                        $('#lblPrefsRangeExt4').text('From ' + (value - 1) + ' to 0 extensions')

                        app.ui.prefs.rangeExt.items[0].newValue[1].min = value
                        app.ui.prefs.rangeExt.items[0].newValue[0].max = value + 1
                        app.ui.prefs.rangeExt.redrawBar()
                    }
                }
            },

            blockKeyEvents: function (e) {
                return false
            },

            redrawBar: function () {
                let newArray = app.ui.prefs.rangeExt.items[0].newValue.reverse()
                newArray.forEach((value, ix) => {
                    // adjust the max value to allow plotting
                    if (value.max > 101) value.max = 101
                    $('#tdPrefsRangeExt' + (ix + 1)).css('width', (value.max - value.min) + '%')
                })
                newArray = newArray.reverse()
            },

            startFlash: () => {
                let flip = true
                app.ui.prefs.rangeExt.hInterval = setInterval(() => {
                    flip = !flip

                    $('#lblPrefsRangeExtFlash').css('background', flip === true ? 'var(--ydb-status-red)' : 'white')
                    $('#tdPrefsRangeExt4').css('background', flip === true ? 'var(--ydb-status-red)' : 'white')

                }, app.userSettings.dashboard.flashInterval)
            },

            items: null,
            hInterval: null
        }
    },
)
