/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.prefs = {
    init: function () {
        $('#modalPrefs')
            .on('hide.bs.modal', () => {
                clearInterval(this.rangePercent.hInterval)
                clearInterval(this.rangeExt.hInterval)
            })
            .on('show.bs.modal', () => {
                this.rangePercent.startFlash()
                this.rangeExt.startFlash()
            })

        $('#btnPrefsOk').on('click', () => this.okPressed())
        $('#btnPrefsExportRanges').on('click', () => this.exportPressed())


        app.ui.setupDialogForTest('modalPrefs')
    },

    show: function (altSettings = {}) {
        // altSettings: { title: '', manifest: {}, target: {}, name }

        $('#divPrefsItemsContainer').css('display', 'none')
        $('#divPrefsRangePercentContainer').css('display', 'none')
        $('#divPrefsRangeExtensionsContainer').css('display', 'none')

        $('#lblPreferencesTitle').text(altSettings && altSettings.title ? altSettings.title : 'Preferences')

        this.altSettings = altSettings

        // add shadow if in detail mode and remove it if in preferences mode
        if (this.altSettings.name) {
            $('#divPreferencesContent')
                .addClass('modal-content-shadow')

            $('#btnPrefsExportRanges').css('display', 'none')

        } else {
            $('#divPreferencesContent')
                .removeClass('modal-content-shadow')

            $('#btnPrefsExportRanges').css('display', 'inline')
        }

        this.firstId = ''

        this.populateTree()

        // auto select first item
        setTimeout(() => {
            $('#treePrefsTree').jstree(true).select_node(this.firstId)

        }, 200)

        $('#modalPrefs').modal({show: true, backdrop: 'static'})
    },

    // ************************************
    // populate tree
    // ************************************
    populateTree: function () {
        const $tree = $('#treePrefsTree')
        let treeData = []
        let firstNode = false

        if (this.altSettings.title) {
            app.ui.stats.manifest.manifestRebuild(this.altSettings.name)

        } else {
            app.prefs.manifestRebuild()
        }

        // start looping into the categories
        app.prefs.manifest.forEach(cat => {
            this.loadItemsValues(cat.items)

            let catData = {
                id: cat.name.replaceAll(' ', ''),
                text: cat.name,
                data: cat.items || null,
                children: []
            }

            if (firstNode === false && this.altSettings.title) {
                firstNode = true

                catData.state = {opened: true}
            }

            // store the first item id for auto selection
            if (this.firstId === '') this.firstId = catData.id

            // then sub categories
            if (cat.children) {
                cat.children.forEach(cat2 => {
                    this.loadItemsValues(cat2.items)

                    let catData2 = {
                        id: catData.id + '-' + cat2.name.replaceAll(' ', ''),
                        text: cat2.name,
                        data: cat2.items || null,
                        children: []
                    }

                    if (cat2.children) {
                        cat2.children.forEach(cat3 => {
                            this.loadItemsValues(cat3.items)

                            let catData3 = {
                                id: catData2.id + '-' + cat3.name.replaceAll(' ', ''),
                                text: cat3.name,
                                data: cat3.items || null,
                                children: []
                            }

                            if (cat3.children) {
                                // manage [regions] placeholder
                                cat3.children.forEach((cat2, ix) => {

                                    if (cat2.name === '[regions]') {
                                        Object.keys(app.system.regions).forEach((region) => {
                                            cat2.children[0].items[0].region = cat2.children[1].items[0].region = region.toUpperCase()

                                            let regionDisplayText = 'Region '
                                            if (app.userSettings.dashboard.regions[region.toUpperCase()]) {
                                                regionDisplayText += '<span style="color: var(--ydb-orange);">' + region.toUpperCase() + '</span>'

                                            } else {
                                                regionDisplayText += region.toUpperCase()
                                            }

                                            cat3.children.push({
                                                name: regionDisplayText,
                                                children: JSON.parse(JSON.stringify(cat2.children)),
                                            })
                                        })

                                        // remove placeholder
                                        cat3.children.splice(ix, 1)
                                    }
                                })

                                cat3.children.forEach(cat4 => {
                                    this.loadItemsValues(cat4.items)

                                    // this is to remove the HTML code from the id
                                    let idByName = cat4.name
                                    idByName = idByName.replace('<span style="color: var(--ydb-orange);">', '')
                                    idByName = idByName.replace('</span>', '')

                                    let catData4 = {
                                        id: catData3.id + '-' + idByName.replaceAll(' ', ''),
                                        text: cat4.name,
                                        data: cat4.items || null,
                                        region: cat4.region || null,
                                        children: []
                                    }

                                    if (cat4.children) {
                                        cat4.children.forEach(cat5 => {
                                            this.loadItemsValues(cat5.items)

                                            let catData5 = {
                                                id: catData4.id + '-' + cat5.name.replaceAll(' ', ''),
                                                text: cat5.name,
                                                data: cat5.items || null,
                                                children: []
                                            }

                                            if (cat5.children) {
                                                cat5.children.forEach(cat6 => {
                                                    this.loadItemsValues(cat6.items)

                                                    let catData6 = {
                                                        id: catData5.id + '-' + cat6.name.replaceAll(' ', ''),
                                                        text: cat6.name,
                                                        data: cat6.items || null,
                                                        children: []
                                                    }

                                                    if (cat6.children) {
                                                        cat6.children.forEach(cat7 => {
                                                            this.loadItemsValues(cat7.items)

                                                            let catData7 = {
                                                                id: catData6.id + '-' + cat7.name.replaceAll(' ', ''),
                                                                text: cat7.name,
                                                                data: cat7.items || null,
                                                                children: []
                                                            }

                                                            catData6.children.push(catData7)
                                                        })
                                                    }

                                                    catData5.children.push(catData6)
                                                })
                                            }

                                            catData4.children.push(catData5)
                                        })
                                    }

                                    catData3.children.push(catData4)
                                })
                            }

                            catData2.children.push(catData3)
                        })
                    }

                    catData.children.push(catData2)
                })
            }

            treeData.push(catData)
        })

        // configure the tree
        $tree
            .jstree("destroy")
            .jstree({
                'core': {
                    'check_callback': true,
                    'multiple': false,
                    'data': treeData,
                    'themes': {
                        'name': 'proton',
                    }
                },
            })
            .on("select_node.jstree", function (node, selected) {
                // determine what to do when node is clicked
                if (selected.node.data !== null) {
                    app.ui.prefs.currentNode = selected.node
                    app.ui.prefs.currentPath = selected.node.id

                    if (selected.node.data.length === 1) {
                        if (selected.node.data[0].type === 'rangeExtension') {
                            // display range extension
                            app.ui.prefs.rangeExt.populate(selected.node.data)

                        } else if (selected.node.data[0].type === 'rangePercent') {
                            // display range percent
                            app.ui.prefs.rangePercent.populate(selected.node.data)

                        } else {
                            // display table
                            app.ui.prefs.populateTable(selected.node.data)

                        }
                    } else {
                        // display table
                        app.ui.prefs.populateTable(selected.node.data)
                    }
                } else {
                    // clear table view
                    $('#divPrefsItemsContainer').css('display', 'none')
                    $('#divPrefsRangePercentContainer').css('display', 'none')
                    $('#divPrefsRangeExtensionsContainer').css('display', 'none')
                }

            })
            .on('open_node.jstree', (e, node) => {
                $tree.jstree(true).deselect_all()
                $tree.jstree(true).select_node(node.node.id)
            })
            .on('close_node.jstree', (e, node) => {
                $tree.jstree(true).deselect_all()
                $tree.jstree(true).select_node(node.node.id)
            })
    },

    getValueFromSettings: function (item) {
        let lValue = app.userSettings

        if (this.altSettings.title) {
            lValue = this.altSettings.target
        }

        if (item.region) {
            // adjust path with region
            item.path = item.path.replace('{region}', item.region)

            // try to get the value
            item.path.split('/').forEach(node => {
                if (node.split('@').length === 1) {
                    try {
                        lValue = lValue[node]
                    } catch {
                        lValue = null
                    }
                }
            })

            // if not found, use defaults
            if (lValue === null) {
                if (item.path.indexOf('autoExtend') > -1) {
                    lValue = JSON.parse(JSON.stringify(this.defaultRange.auto))

                } else {
                    lValue = JSON.parse(JSON.stringify(this.defaultRange.manual))
                }
            }

        } else {
            item.path.split('/').forEach(node => {
                if (node.split('@').length === 1) {
                    lValue = lValue[node]
                } else if (item.path.indexOf('autoExtend') > -1) {
                    this.defaultRange.auto = lValue

                } else if (item.path.indexOf('manualExtend') > -1) {
                    this.defaultRange.manual = lValue
                }
            })
        }

        return lValue
    },

    loadItemsValues: function (items) {
        if (items && items.length > 0) {
            items.forEach(item => {
                item.oldValue = item.newValue = this.getValueFromSettings(item)
                item.dirty = false
            })
        }
    },

    exportPressed: function () {
        app.ui.inputbox.show('This option allows you to export the range values to an external file.<br>Are you sure you want to proceed?', 'Warning', async ret => {
            if (ret === 'YES') {
                this.okPressed(async () => {
                    const data = JSON.stringify(app.userSettings.dashboard)

                    if (navigator.userAgent.indexOf("Firefox") != -1) {
                        // *************************************
                        // firefox
                        // *************************************
                        try {
                            let blob = new Blob([data], {
                                type: "script"
                            })
                            let a = document.createElement("a")
                            a.href = URL.createObjectURL(blob)
                            a.download = 'ydbgui-range-settings.json'
                            a.hidden = true
                            document.body.appendChild(a)
                            a.innerHTML =
                                ""
                            a.click()
                            a.remove()
                        } catch (err) {
                            app.ui.msgbox.show(app.REST.parseError(err), 'Warning')
                        }

                    } else if (navigator.userAgent.indexOf("Chrome") != -1) {
                        // *************************************
                        // chrome
                        // *************************************
                        try {
                            const handle = await showSaveFilePicker({
                                suggestedName: 'ydbgui-range-settings.json',
                                types: [{
                                    description: 'YottaDB range export',
                                    accept: {'text/json': ['.json']},
                                }],
                            });

                            const blob = new Blob([data]);

                            const writableStream = await handle.createWritable();
                            await writableStream.write(blob);
                            await writableStream.close();

                        } catch (err) {
                            if (err.toString().indexOf('aborted') > -1) return

                            const errorMessage = app.REST.parseError(err)

                            app.ui.msgbox.show(errorMessage.indexOf('showSaveFilePicker') > -1 ? 'This option is available only when using HTTP. Consider using Firefox for this operation' : errorMessage, 'Warning')
                        }

                    } else {
                        app.ui.msgbox.show('Your browser is not supported for this operation.<br><br>Please report your browser type and vendor to YDB support.', 'Error')
                    }
                })
            }
        })
    },

    okPressed: function (callback) {
        // create a list of changed fields
        const changed = []
        const hives = []

        const tree = $('#treePrefsTree').data().jstree.get_json()

        tree.forEach(node => {
            if (node.data && node !== null && node.data.length > 0) {
                node.data.forEach(item => {
                    if (item.dirty === true) changed.push(item)
                })
            }

            if (node.children && node.children.length > 0) {
                node.children.forEach(node2 => {
                    if (node2.data && node2 !== null && node2.data.length > 0) {
                        node2.data.forEach(item => {
                            if (item.dirty === true) changed.push(item)
                        })
                    }

                    if (node2.children && node2.children.length > 0) {
                        node2.children.forEach(node3 => {
                            if (node3.data && node3 !== null && node3.data.length > 0) {
                                node3.data.forEach(item => {
                                    if (item.dirty === true) changed.push(item)
                                })
                            }

                            if (node3.children && node3.children.length > 0) {
                                node3.children.forEach(node4 => {
                                    if (node4.data && node4 !== null && node4.data.length > 0) {
                                        node4.data.forEach(item => {
                                            if (item.dirty === true) changed.push(item)
                                        })
                                    }

                                    if (node4.children && node4.children.length > 0) {
                                        node4.children.forEach(node5 => {
                                            if (node5.data && node5 !== null && node5.data.length > 0) {
                                                node5.data.forEach(item => {
                                                    if (item.dirty === true) changed.push(item)
                                                })
                                            }

                                            if (node5.children && node5.children.length > 0) {
                                                node5.children.forEach(node6 => {
                                                    if (node6.data && node6 !== null && node6.data.length > 0) {
                                                        node6.data.forEach(item => {
                                                            if (item.dirty === true) changed.push(item)
                                                        })
                                                    }

                                                    if (node6.children && node6.children.length > 0) {
                                                        node6.children.forEach(node7 => {
                                                            if (node7.data && node7 !== null && node7.data.length > 0) {
                                                                node7.data.forEach(item => {
                                                                    if (item.dirty === true) changed.push(item)
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })

        if (changed.length === 0) {
            if (typeof callback === 'function') {
                callback()

            } else {
                app.ui.msgbox.show('Nothing has changed...', app.ui.msgbox.INPUT_ERROR)
            }

            return
        }

        // update the settings object
        changed.forEach(item => {
            let lValue = app.userSettings

            if (this.altSettings.title) {
                lValue = this.altSettings.target
            }
            let last = {
                path: {},
                node: ''
            }

            if (item.region && item.region !== '') {        //  && !this.altSettings.title
                // first check if region node already exists
                if (app.userSettings.dashboard.regions[item.region] === undefined) {
                    // create it and populate it with defaults
                    app.userSettings.dashboard.regions[item.region] = {
                        manualExtend: JSON.parse(JSON.stringify(this.defaultRange.manual)),
                        autoExtend: JSON.parse(JSON.stringify(this.defaultRange.auto))
                    }
                }
            }

            item.path.split('/').forEach(node => {
                if (node.split('@').length === 1) {
                    last.path = lValue
                    last.node = node
                    lValue = lValue[node]
                } else {
                    // re-adjust the highest max value (normally it is 999999 for maximum value, but we need 101 to properly scale the bar)
                    item.newValue.forEach(item => {
                        if (item.max === 101) item.max = 999999
                    })
                }
            })

            last.path[last.node] = item.newValue
            if (hives.find(el => el === item.hive) === undefined) hives.push(item.hive)

            if (typeof item.callback === 'function') item.callback()
        })

        // update the hives
        if (!this.altSettings.title) {
            hives.forEach(hive => {
                let node = {}

                switch (hive) {
                    case 'dashboard': {
                        node = app.userSettings.dashboard

                        break
                    }
                    case 'gViewer': {
                        node = app.userSettings.globalViewer

                        break
                    }
                    case 'defaults': {
                        node = app.userSettings.defaults

                        break
                    }
                    case 'rest': {
                        node = app.userSettings.rest

                        break
                    }
                    case 'stats': {
                        node = app.userSettings.stats

                        break
                    }
                }

                app.ui.storage.save(hive, node)
            })

            changed.forEach(item => {
                if (typeof item.postProcess === 'string') {
                    eval(item.postProcess)
                }
            })

            app.ui.dashboard.refresh()

        } else {
            switch (this.altSettings.name) {
                case 'statsSparkChartReportLine': {
                    app.ui.stats.sparkChart.reportLine.preview()

                    break
                }
                case 'statsSparkChartGraphSeriesLine':
                case 'statsSparkChartGraphSeriesYaxis':
                case 'statsSparkChartGraphSeriesArea': {
                    app.ui.stats.sparkChart.graph.series.chartUpdate()

                    break
                }
                case 'statsSparkChartGraphGlobals':
                case 'statsSparkChartGraphTitle':
                case 'statsSparkChartGraphSubTitle':
                case 'statsSparkChartGraphShadow':
                case 'statsSparkChartGraphLegend':
                case 'statsSparkChartGraphToolbar':
                case 'statsSparkChartGraphGrid':
                case 'statsSparkChartGraphxAxis':
                case 'statsSparkChartGraphTooltips':
                case 'statsSparkChartGraphYaxisCommon': {
                    app.ui.stats.sparkChart.graph.graph.update()

                    break
                }
                case 'statsSparkChartGraphYaxisIndividualTools': {
                    app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.refreshGraph()

                    break
                }
                case 'statsSparkChartGraphYaxisSharedTools': {
                    app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.refreshGraph()

                    $('#divStatsGraphToolbarYaxis').css('display', 'none')

                    break
                }
            }
        }

        $('#modalPrefs').modal('hide')

    },

// *********************
// properties
// *********************
    currentNode: {},
    currentPath: '',
    defaultRange: {manual: {}, auto: {}},

    altSettings: {},

    firstId: '',
}
