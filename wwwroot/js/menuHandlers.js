/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.menu.init = () => {
    // connect
    $('#menuDashboard').on('click', () => app.ui.dashboard.refresh());

    // system info
    $('#menuSystemInfo').on('click', () => app.ui.systemInfo.show());

    // Database administration
    $('#menuSystemAdministrationLogoutLi').on('click', () => app.ui.login.logout());
    $('#menuSystemAdministrationRegionAddLi').on('click', () => app.ui.regionAdd.name.show());
    $('#menuSystemAdministrationRegionAdd').on('click', () => app.ui.regionAdd.name.show());
    $('#menuSystemAdministrationLocksManagerLi').on('click', () => app.ui.locksManager.show());
    $('#menuSystemAdministrationGlobalMappingsLi').on('click', () => app.ui.globalMappings.show());
    $('#menuSystemAdministrationDefragLi').on('click', () => app.ui.defrag.show());
    $('#menuSystemAdministrationIntegrityLi').on('click', () => app.ui.integ.show());
    $('#menuSystemAdministrationBackupLi').on('click', () => app.ui.backup.show());
    $('#menuGldFileSelectLi').on('click', () => app.ui.envSettings.show());
    $('#menuStatsLi').on('click', () => app.ui.stats.showTab());
    $('#menuPreferencesLi').on('click', () => app.ui.prefs.show());

    // Development
    $('#menuDevelopmentGlobalsViewerNewLi').on('click', () => app.ui.gViewer.addNew());
    $('#menuDevelopmentRoutinesViewerNewLi').on('click', () => app.ui.rViewer.select.show());
    $('#menuDevelopmentRoutinesViewerNewChangePathLi').on('click', () => app.ui.rViewer.searchPath.show());
    $('#menuDevelopmentOctoNewQueryLi').on('click', () => app.ui.octoViewer.addNew());

    // Replication
    $('#menuReplicationInstanceFileLi').on('click', () => app.ui.replication.instanceFile.show());
    $('#menuReplicationLogFilesLi').on('click', () => app.ui.replication.logs.show(app.system.replication.logFiles, app.system.replication.instanceFile.flags.instanceName))
    $('#menuReplicationTopologyLi').on('click', () => app.ui.replication.topology.showTab());
    $('#menuReplicationConfigurationImportLi').on('change', (e) => app.ui.replication.topology.configuration.import(e))
    $('#menuReplicationConfigurationExportLi').on('click', () => app.ui.replication.topology.configuration.export());

    // documentation
    $('#menuDocumentationAdministration').on('click', () => window.open('https://docs.yottadb.com/AdminOpsGuide/index.html', '_blank'));
    $('#menuDocumentationMessages').on('click', () => window.open('https://docs.yottadb.com/MessageRecovery/index.html', '_blank'));
    $('#menuDocumentationMprogrammer').on('click', () => window.open('https://docs.yottadb.com/ProgrammersGuide/index.html', '_blank'));
    $('#menuDocumentationMultiLanguage').on('click', () => window.open('https://docs.yottadb.com/MultiLangProgGuide/index.html', '_blank'));
    $('#menuDocumentationOcto').on('click', () => window.open('https://docs.yottadb.com/Octo/index.html', '_blank'));
    $('#menuDocumentationPlugins').on('click', () => window.open('https://docs.yottadb.com/Plugins/index.html', '_blank'));
    $('#menuDocumentationAcculturation').on('click', () => window.open('https://docs.yottadb.com/AcculturationGuide/acculturation.html', '_blank'));
    $('#menuDebugInformation').on('click', () => {
        const text = JSON.stringify(app.system, null, 4);

        // converts the spaces into non-breaking spaces and new lines as <BR>
        $('#txtDumpResponse').html(text.replace(/ /g, "&nbsp;").replace(/\n/g, "<br/>"));

        $('#modalDumpResponse').modal({show: true, backdrop: 'static', keyboard: true});
    });

    $('#menuHelp').on('click', () => app.ui.menu.help.display());

    // global viewer
    app.ui.menu.rebuildGlobalViewerLastUsed();
};

app.ui.menu.processRefresh = () => {
    $('#menuSystemAdministrationRegion').removeClass('disabled hand')
    $('#menuSystemAdministrationLocksManager').removeClass('disabled hand')
    $('#menuSystemAdministrationGlobalMappings').removeClass('disabled hand')
    $('#menuSystemAdministrationBackup').removeClass('disabled hand')
    $('#menuSystemAdministrationIntegrity').removeClass('disabled hand')
    $('#menuSystemAdministrationDefrag').removeClass('disabled hand')
    $('#menuPreferences').removeClass('disabled hand')

    $('#menuDevelopmentGlobalsViewerNew').removeClass('disabled hand')
    $('#menuDevelopmentGlobalsViewer').removeClass('disabled hand')
    $('#menuDevelopmentOcto').removeClass('disabled hand')

    $('#menuSystemInfo').removeClass('disabled').removeClass('default').addClass('hand');
    $('#menuSystemAdministration').removeClass('disabled').removeClass('default').addClass('hand');
    $('#menuDevelopment').removeClass('disabled').removeClass('default').addClass('hand');

    let status = {
        journal: [],          // used to disable the journaling menu and build the list for the select menu
        dbFile: [],           // used to disable the create db file menu and build the list for the select menu
        extendDbFile: [],     // used to build the list for the extend menu (or disabled if all is bad)
        regions: []           // list of regions for view / edit / delete
    };

    Object.keys(app.system.regions).forEach((regionName) => {
        const region = app.system.regions[regionName];

        status.regions.push(regionName);

        // check if at least one database needs to have the file created
        if (region.dbFile.flags.fileExist === false) status.dbFile.push(regionName);

        // check if at least one database have a journal
        if (region.journal !== undefined && region.journal.flags !== undefined && region.journal.flags.state > 0) status.journal.push(regionName);

        // check if db can be extended
        if (region.dbFile.flags.fileExist) status.extendDbFile.push(regionName);
    });

    // journaling start / stop
    const menuSystemAdministrationRegionJournaling = $('#menuSystemAdministrationRegionJournaling');
    const menuSystemAdministrationRegionJournalingLi = $('#menuSystemAdministrationRegionJournalingLi');

    if (status.journal.length === 0 || app.serverMode === 'RO') {
        menuSystemAdministrationRegionJournaling.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionJournalingLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionJournaling.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionJournalingLi.removeClass('disabled').removeClass('default').addClass('hand');

        // add handler
        menuSystemAdministrationRegionJournalingLi.on('click', () => app.ui.regionSelect.show(status.journal, app.ui.regionJournalSwitch.show));
    }

    // journaling switch file
    const menuSystemAdministrationRegionJournalingSwitch = $('#menuSystemAdministrationRegionJournalingSwitch');
    const menuSystemAdministrationRegionJournalingSwitchLi = $('#menuSystemAdministrationRegionJournalingSwitchLi');

    if (status.journal.length === 0 || app.serverMode === 'RO') {
        menuSystemAdministrationRegionJournalingSwitch.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionJournalingSwitchLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionJournalingSwitch.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionJournalingSwitchLi.removeClass('disabled').removeClass('default').addClass('hand');

        // add handler
        menuSystemAdministrationRegionJournalingSwitchLi.on('click', () => app.ui.regionSelect.show(status.journal, app.ui.regionJournalSwitch.switchFiles));
    }

    // Create db file
    const menuSystemAdministrationRegionCreateDatabaseLi = $('#menuSystemAdministrationRegionCreateDatabaseLi');
    const menuSystemAdministrationRegionCreateDatabase = $('#menuSystemAdministrationRegionCreateDatabase');

    if (status.dbFile.length === 0 || app.serverMode === 'RO') {
        menuSystemAdministrationRegionCreateDatabase.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionCreateDatabaseLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionCreateDatabase.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionCreateDatabaseLi.removeClass('disabled').removeClass('default').addClass('hand');

        // add handler
        menuSystemAdministrationRegionCreateDatabaseLi.on('click', () => app.ui.regionSelect.show(status.dbFile, app.ui.regionCreateDbFile.show));
    }

    // extend db file
    const menuSystemAdministrationRegionExtendDatabaseLi = $('#menuSystemAdministrationRegionExtendDatabaseLi');
    const menuSystemAdministrationRegionExtendDatabase = $('#menuSystemAdministrationRegionExtendDatabase');

    if (status.extendDbFile.length === 0 || app.serverMode === 'RO') {
        menuSystemAdministrationRegionExtendDatabase.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionExtendDatabaseLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionExtendDatabase.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionExtendDatabaseLi.removeClass('disabled').removeClass('default').addClass('hand');

        // add handler
        menuSystemAdministrationRegionExtendDatabaseLi.on('click', () => app.ui.regionSelect.show(status.extendDbFile, app.ui.regionExtend.show));
    }

    // create region
    const menuSystemAdministrationRegionAddLi = $('#menuSystemAdministrationRegionAddLi');
    const menuSystemAdministrationRegionAdd = $('#menuSystemAdministrationRegionAdd');

    if (app.serverMode === 'RO') {
        menuSystemAdministrationRegionAdd.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionAddLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionAdd.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionAddLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    // edit region
    const menuSystemAdministrationRegionEditLi = $('#menuSystemAdministrationRegionEditLi');
    const menuSystemAdministrationRegionEdit = $('#menuSystemAdministrationRegionEdit');

    if (app.serverMode === 'RO') {
        menuSystemAdministrationRegionEdit.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionEditLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionEdit.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionEditLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    // delete region
    const menuSystemAdministrationRegionDeleteLi = $('#menuSystemAdministrationRegionDeleteLi');
    const menuSystemAdministrationRegionDelete = $('#menuSystemAdministrationRegionDelete');

    if (status.extendDbFile.length === 0 || app.serverMode === 'RO') {
        menuSystemAdministrationRegionDelete.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationRegionDeleteLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationRegionDelete.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationRegionDeleteLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    // region view handlers
    $('#menuSystemAdministrationRegionViewLi').on('click', () => app.ui.regionSelect.show(status.regions, app.ui.regionView.show));

    // region delete handlers
    $('#menuSystemAdministrationRegionDeleteLi').on('click', () => app.ui.regionSelect.show(status.regions, app.ui.regionDelete.show));
    $('#menuSystemAdministrationRegionEditLi').on('click', () => app.ui.regionSelect.show(status.regions, app.ui.regionEdit.show));

    // Reorg
    const menuSystemAdministrationDefragLi = $('#menuSystemAdministrationDefragLi');
    const menuSystemAdministrationDefrag = $('#menuSystemAdministrationDefrag');

    if (app.serverMode === 'RO') {
        menuSystemAdministrationDefrag.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationDefragLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationDefrag.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationDefragLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    //Integ
    const menuSystemAdministrationIntegrityLi = $('#menuSystemAdministrationIntegrityLi');
    const menuSystemAdministrationIntegrity = $('#menuSystemAdministrationIntegrity');

    if (app.serverMode === 'RO') {
        menuSystemAdministrationIntegrity.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationIntegrityLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationIntegrity.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationIntegrityLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    // Backup
    const menuSystemAdministrationBackupLi = $('#menuSystemAdministrationBackupLi');
    const menuSystemAdministrationBackup = $('#menuSystemAdministrationBackup');

    if (app.serverMode === 'RO') {
        menuSystemAdministrationBackup.addClass('disabled').addClass('default').removeClass('hand');
        menuSystemAdministrationBackupLi.addClass('disabled').addClass('default').removeClass('hand');

    } else {
        menuSystemAdministrationBackup.removeClass('disabled').removeClass('default').addClass('hand');
        menuSystemAdministrationBackupLi.removeClass('disabled').removeClass('default').addClass('hand');
    }

    // Octo
    const menuDevelopmentOctoLi = $('#menuDevelopmentOctoLi');
    const menuDevelopmentOcto = $('#menuDevelopmentOcto');

    const menuDevelopmentOctoNewQuery = $('#menuDevelopmentOctoNewQuery')
    const menuDevelopmentOctoNewQueryLi = $('#menuDevelopmentOctoNewQueryLi')

    if (app.system.octo && app.system.octo.status === 'ok') {
        menuDevelopmentOcto.removeClass('disabled').removeClass('default').addClass('hand');
        menuDevelopmentOctoLi.removeClass('disabled').removeClass('default').addClass('hand');

        menuDevelopmentOctoNewQuery.removeClass('disabled').removeClass('default').addClass('hand');
        menuDevelopmentOctoNewQueryLi.removeClass('disabled').removeClass('default').addClass('hand');

    } else {
        menuDevelopmentOcto.addClass('disabled').addClass('default').removeClass('hand');
        menuDevelopmentOctoLi.addClass('disabled').addClass('default').removeClass('hand');

        menuDevelopmentOctoNewQuery.addClass('disabled').addClass('default').removeClass('hand');
        menuDevelopmentOctoNewQueryLi.addClass('disabled').addClass('default').removeClass('hand');
    }

    // Replication
    const menuReplicationLi = $('#menuReplicationLi');
    const menuReplication = $('#menuReplication');

    if (app.system.replication) {
        menuReplicationLi
            .removeClass('disabled')
            .addClass('dropdown-submenu')

        menuReplication.removeClass('disabled')

    } else {
        menuReplicationLi
            .removeClass('dropdown-submenu')
            .addClass('disabled')

        menuReplication.addClass('disabled')
    }

    app.ui.menu.adjustLeftDynamicMenus($('#menuReplicationConfiguration'));


};

app.ui.menu.rebuildGlobalViewerLastUsed = () => {
    const items = [...app.userSettings.globalViewer.lastUsed].reverse();
    const numItemsInMenu = app.userSettings.globalViewer.options.menuLastUsedItemsCount;
    const menuDevelopmentGlobalsViewerRecentListEntries = $('#menuDevelopmentGlobalsViewerRecentListEntries');

    if (items.length === 0) {
        // no items, disable everything
        $('#menuDevelopmentGlobalsViewerRecentList')
            .empty()
            .append(
                '<li class="disabled" id="menuDevelopmentGlobalsViewerRecentListNoEntriesLi">' +
                '<a class="disabled" href="#" id="menuDevelopmentGlobalsViewerRecentListNoEntries" tabindex="-1">No entries found...</a>' +
                '</li>'
            )

    } else {
        // items found, display list + manager item
        $('#menuDevelopmentGlobalsViewerRecentLi')
            .removeClass('disabled');

        $('#menuDevelopmentGlobalsViewerRecentLi > a').removeClass('disabled');

        let menuItems = '';
        let limit = items.length > numItemsInMenu ? numItemsInMenu : items.length;

        // create the menu entries
        for (let ix = 0; ix < limit; ix++) {
            menuItems += '<li class="dropdown-item" id="menuDevelopmentGlobalsViewerRecentListEntry-' + ix + '"  onclick="app.ui.gViewer.addLastUsed(\'' + items[ix] + '\')">' +
                '<a class="dropdown-item" href="#" id="menuDevelopmentGlobalsViewerRecentListEntry-' + ix + '" tabindex="-1">' + items[ix] + '</a>' +
                '</li>'
        }

        // eventual extra item dialog
        if (items.length > numItemsInMenu) {
            menuItems += '<li class="dropdown-item" id="menuDevelopmentGlobalsViewerRecentListManagerMoreLi">' +
                '<a class="dropdown-item" href="#" id="menuDevelopmentGlobalsViewerRecentListManagerMore" tabindex="-1" onclick="app.ui.gViewer.moreItems.show()">More...</a>' +
                '</li>';
        }

        // and the extra menu options
        menuItems += '<li class="dropdown-divider"></li>' +
            '<li class="dropdown-item" id="menuDevelopmentGlobalsViewerRecentListManagerLi" onclick="app.ui.globalsManager.show()">' +
            '<a class="dropdown-item" href="#" id="menuDevelopmentGlobalsViewerRecentListManager" tabindex="-1" >Manage list...</a>' +
            '</li>';

        menuDevelopmentGlobalsViewerRecentListEntries
            .addClass('dropdown-menu')
            .empty()
            .append(menuItems)
    }

    app.ui.menu.adjustLeftDynamicMenus(menuDevelopmentGlobalsViewerRecentListEntries);
};

app.ui.menu.adjustLeftDynamicMenus = $element => {
    // this code re-adjusts the dynamic menus to the left (as we have Rto L menus)
    const menuEl = $element.clone().appendTo("body").css({'display': 'block', 'visibility': 'hidden'});
    const menuWidth = menuEl.width();
    menuEl.remove();

    $element.css('left', -menuWidth + 'px')
};

app.ui.menu.help = {
    display: function () {
        const page = 'home/index'
        const path = window.location.origin + '/help/index.html?'

        window.open(path + page, 'ydbgui_help')
    }
}


