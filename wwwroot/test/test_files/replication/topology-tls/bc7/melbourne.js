/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts, configTls} = require('../../helper')

describe("CLIENT: REPL > Topology tls > bc7 > Melbourne", async () => {
    it("Test # 3190: verify graph links and rects are correct and machines are not dead", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await configTls()

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        const foundArray = []

        graph.forEach(cell => {
            if (cell.type === 'Primary' && cell.id === 'melbourne') {
                foundArray.push(true)
            }

            if (cell.type === 'Secondary' && (
                    cell.id === 'paris' ||
                    cell.id === 'amsterdam' ||
                    cell.id === 'london' ||
                    cell.id === 'madrid' ||
                    cell.id === 'rome' ||
                    cell.id === 'santiago' ||
                    cell.id === 'tokio') &&
                cell.dead === false) {
                foundArray.push(true)
            }

            if (cell.type === 'replLink' && (
                cell.id === 'melbourne-paris' ||
                cell.id === 'melbourne-amsterdam' ||
                cell.id === 'melbourne-madrid' ||
                cell.id === 'melbourne-london' ||
                cell.id === 'melbourne-santiago' ||
                cell.id === 'melbourne-tokio' ||
                cell.id === 'melbourne-rome'
            )) {
                foundArray.push(true)
            }
        })

        expect(foundArray.length === graph.length).to.be.true
    })
})
