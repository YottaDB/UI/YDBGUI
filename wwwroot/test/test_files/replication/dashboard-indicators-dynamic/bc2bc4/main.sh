#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '***************************************'
echo '---Dashboard indicators dynamic > bc2bc4'
echo '***************************************'
echo

$PWD/replication/repl run bc2bc4
while
	! docker logs --tail 1 melbourne | grep -q "Starting Server at port" ||
	! docker logs --tail 1 paris | grep -q "Starting Server at port" ||
	! docker logs --tail 1 santiago | grep -q "Starting Server at port" ||
	! docker logs --tail 1 rome | grep -q "Starting Server at port" ||
	! docker logs --tail 1 amsterdam | grep -q "Starting Server at port" ||
	! docker logs --tail 1 london | grep -q "Starting Server at port" ||
	! docker logs --tail 1 tokio | grep -q "Starting Server at port";
do sleep 1
done

docker exec melbourne bash  -c '. /opt/yottadb/current/ydb_env_set && mumps -run %XCMD "for x=1:1:8E6 set ^x(x)=x"' &
docker exec -e ydb_in_repl=paris paris npm test -- wwwroot/test/test_files/replication/dashboard-indicators-dynamic/bc2bc4/paris.js
script1=$?

$PWD/replication/repl down
exit $script1
