/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Topology UI > bc1 > Design toolbar", async () => {
    it("Test # 3500: Zoom in: open layout, , zoom in, verify scale() has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get paper info
        const paper = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale = paper.scale.sx

        // zoom in
        let cell = await page.$('#btnTopologyToolbarZoomIn')
        await cell.click()

        // get paper info
        const paper2 = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale2 = paper2.scale.sx

        expect(scale !== scale2).to.be.true
    })

    it("Test # 3502: Zoom out: open layout, , zoom out, verify scale() has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get paper info
        const paper = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale = paper.scale.sx

        // zoom in
        let cell = await page.$('#btnTopologyToolbarZoomOut')
        await cell.click()

        // get paper info
        const paper2 = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale2 = paper2.scale.sx

        expect(scale !== scale2).to.be.true
    })

    it("Test # 3505: Fit To Content: Open layout, modify layout, select FitToContent, verify scale() has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get paper info
        const paper = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale = paper.scale.sx

        // modify layout
        await page.evaluate(() => {
            const shape = app.ui.replication.topology.joint.findShapeById('paris')
            shape.position(500, 500)
        })

        // click on fitToContent
        let cell = await page.$('#btnTopologyToolbarFitToContent')
        await cell.click()

        // get paper info
        const paper2 = await page.evaluate(() => app.ui.replication.topology.joint.getPaper())
        const scale2 = paper2.scale.sx

        expect(scale !== scale2).to.be.true
    })

    it("Test #  3506: Router: open layout, change router, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get paper info
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        let router
        graph.forEach(model => {
            if (model.type === 'replLink') {
                router = model.router
            }
        })

        // change router
        await page.evaluate(() => {
            $('#selTopologyToolbarRouter').val('metro')
            app.ui.replication.topology.toolbar.linkRouterChange()
        })

        // get paper info
        const graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        let router2
        graph2.forEach(model => {
            if (model.type === 'replLink') {
                router2 = model.router
            }
        })

        expect(router !== router2).to.be.true
    })

    it("Test #  3507: Connector: open layout, change connector, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get paper info
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        let connector
        graph.forEach(model => {
            if (model.type === 'replLink') {
                connector = model.connector
            }
        })

        // change connector
        await page.evaluate(() => {
            $('#selTopologyToolbarConnector').val('curve')
            app.ui.replication.topology.toolbar.linkConnectorChange()
        })

        // get paper info
        const graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        let connector2
        graph2.forEach(model => {
            if (model.type === 'replLink') {
                connector2 = model.connector
            }
        })

        expect(connector !== connector2).to.be.true
    })

})

describe("CLIENT: REPL > Topology UI > bc1 > Design toolbar > Undo", async () => {
    it("Test # 3510: Open layout, at launch it should be empty: icon disabled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // check icons status
        let disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.true
    })

    it("Test # 3511: Open layout, make changes, so it will become enabled, close tab, open again, should be disabled again", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // emulate a change
        await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.saveEntry())

        // check icons status
        let disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true

        // close the tab
        await page.evaluate(() => app.ui.tabs.close('tab-T'))

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // check icons status
        disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true
    })

    it("Test # 3512: Open layout, perform two changes, click undo, verify, click again, verify again", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // modify layout
        await page.evaluate(() => {
            const shape = app.ui.replication.topology.joint.findShapeById('paris')
            shape.position(500, 500)
        })
        await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.saveEntry())

        await page.evaluate(() => {
            const shape = app.ui.replication.topology.joint.findShapeById('paris')
            shape.position(200, 200)
        })
        await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.saveEntry())

        // undo
        let cell = await page.$('#btnTopologyToolbarUndo')
        await cell.click()

        // verify position has changed
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph.forEach(model => {
            if (model.id === 'paris') {
                expect(model.position.x === 500).to.be.true
                expect(model.position.y === 500).to.be.true
            }
        })

        cell = await page.$('#btnTopologyToolbarUndo')
        await cell.click()

        // verify position has changed
        graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph.forEach(model => {
            if (model.id === 'paris') {
                expect(model.position.x !== 500).to.be.true
                expect(model.position.y !== 500).to.be.true
            }
        })
    })

    it("Test # 3513: Open layout, zoom in, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // zoom in
        let cell = await page.$('#btnTopologyToolbarZoomIn')
        await cell.click()

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3514: Open layout, zoom out, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // zoom out
        let cell = await page.$('#btnTopologyToolbarZoomOut')
        await cell.click()

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3515: Open layout, change router, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // change router
        await page.evaluate(() => app.ui.replication.topology.toolbar.linkRouterChange())

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3516: Open layout, change connector, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // change router
        await page.evaluate(() => app.ui.replication.topology.toolbar.linkConnectorChange())

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3517: Open layout, pop Auto-layout, change direction, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3518: Open layout, pop Auto-layout, change rank up, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.rankSeparation('up'))

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3519: Open layout, pop Auto-layout, change rank down, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.rankSeparation('down'))

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3520: Open layout, pop Auto-layout, change node up, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.nodeSeparation('up'))

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })

    it("Test # 3521: Open layout, pop Auto-layout, change node down, verify buffer is bigger", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store undo buffer size
        const bufferLength = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.nodeSeparation('down'))

        // store undo buffer size
        const bufferLength2 = await page.evaluate(() => app.ui.replication.topology.joint.undoRedo.buffer.length)

        expect(bufferLength2 > bufferLength).to.be.true
    })
})

describe("CLIENT: REPL > Topology UI > bc1 > Design toolbar > Redo", async () => {
    it("Test # 3525: Open layout, at launch it should be empty: icon disabled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        let disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true

    })

    it("Test # 3526: Open layout, make changes, so it will become enabled, close tab, open again, should be disabled again", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // emulate changes
        await page.evaluate(() => {
            app.ui.replication.topology.joint.undoRedo.saveEntry()
            app.ui.replication.topology.joint.undoRedo.saveEntry()
            app.ui.replication.topology.joint.undoRedo.saveEntry()
        })

        // check icons status
        let disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true

        // perform an undo
        let cell = await page.$('#btnTopologyToolbarUndo')
        await cell.click()

        // check icons status
        disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.false
    })

    it("Test #  3527: Open layout, perform one change, click undo, then redo, verify, then redo once again, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // emulate changes
        await page.evaluate(() => {
            app.ui.replication.topology.joint.undoRedo.saveEntry()
        })

        // check icons status
        let disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true

        // perform an undo
        cell = await page.$('#btnTopologyToolbarUndo')
        await cell.click()

        // perform an redo
        cell = await page.$('#btnTopologyToolbarRedo')
        await cell.click()

        // check icons status
        disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.false

        // perform an redo
        cell = await page.$('#btnTopologyToolbarRedo')
        await cell.click()

        // check icons status
        disabled = await page.evaluate(() => $('#btnTopologyToolbarUndo').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnTopologyToolbarRedo').hasClass('disabled'))
        expect(disabled).to.be.true
    })
})

describe("CLIENT: REPL > Topology UI > bc1 > Design toolbar > Redo", async () => {
    it("Test # 3528: Open layout, click on arrow, should display popup", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop the auto layout
        cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // verify it is visible
        const show = await page.evaluate(() => $('#dmReplArrangeMenu').hasClass('show'))
        expect(show).to.be.true
    })

    it("Test # 3529: Open layout, click on arrow, should display popup, select Left To Right, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store the graph
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        // change direction
        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // compare the graph to verify the change in layout
        let graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph2.forEach((model, ix) => {
            if (model.id === 'paris') {
                expect(model.position.x !== graph[ix].position.x).to.be.true
                expect(model.position.y !== graph[ix].position.y).to.be.true
            }
        })
    })

    it("Test # 3530: Open layout, click on arrow, change rank down, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store the graph
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        cell = await page.$('#btnReplLayoutArrangeRankSepDown')
        await cell.click()

        // change direction

        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // compare the graph to verify the change in layout
        let graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph2.forEach((model, ix) => {
            if (model.id === 'paris') {
                expect(model.position.x !== graph[ix].position.x).to.be.true
                expect(model.position.y !== graph[ix].position.y).to.be.true
            }
        })
    })

    it("Test # 3531: Open layout, click on arrow, change rank up, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store the graph
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        cell = await page.$('#btnReplLayoutArrangeRankSepUp')
        await cell.click()

        // change direction

        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // compare the graph to verify the change in layout
        let graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph2.forEach((model, ix) => {
            if (model.id === 'paris') {
                expect(model.position.x !== graph[ix].position.x).to.be.true
                expect(model.position.y !== graph[ix].position.y).to.be.true
            }
        })
    })

    it("Test # 3532: Open layout, click on arrow, change node down, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store the graph
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        cell = await page.$('#btnReplLayoutArrangeNodeSepDown')
        await cell.click()

        // change direction

        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // compare the graph to verify the change in layout
        let graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph2.forEach((model, ix) => {
            if (model.id === 'paris') {
                expect(model.position.x !== graph[ix].position.x).to.be.true
                expect(model.position.y !== graph[ix].position.y).to.be.true
            }
        })
    })

    it("Test # 3533: Open layout, click on arrow, change node up, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // store the graph
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // pop the auto layout
        let cell = await page.$('#btnTopologyToolbarAutoArrange')
        await cell.click()

        cell = await page.$('#btnReplLayoutArrangeNodeSepUp')
        await cell.click()

        // change direction

        await page.evaluate(() => app.ui.replication.topology.toolbar.autoArrange('LR'))

        // compare the graph to verify the change in layout
        let graph2 = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph2.forEach((model, ix) => {
            if (model.id === 'paris') {
                expect(model.position.x !== graph[ix].position.x).to.be.true
                expect(model.position.y !== graph[ix].position.y).to.be.true
            }
        })
    })
})
