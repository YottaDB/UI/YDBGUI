/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Topology static > bc7 > tokio", async () => {
    it("Test # 3162: verify graph links and rects are correct", async () => {
        await page.goto(`https://localhost:${browserPorts.TOKIO}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        const foundArray = []

        graph.forEach(cell => {
            if (cell.type === 'Secondary' && cell.id === 'tokio') {
                foundArray.push(true)
            }
        })

        expect(foundArray.length === graph.length).to.be.true
    })
})
