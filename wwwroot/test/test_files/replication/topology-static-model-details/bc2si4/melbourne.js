/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Topology static model details > bc2si4 > Melbourne", async () => {
    it("Test # 3280: verify Melbourne header", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'melbourne')

        expect(cell.attr.header.html).to.have.string('MELBOURNE')
    })

    it("Test # 3281: verify Melbourne role", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'melbourne')

        expect(cell.attr.role.html).to.have.string('Primary')
    })

    it("Test # 3282: verify Melbourne data access", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'melbourne')

        expect(cell.attr['data-access'].html).to.have.string('RW')
    })

    it("Test # 3283: verify Paris header", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'paris')

        expect(cell.attr.header.html).to.have.string('PARIS')
    })

    it("Test # 3284: verify Paris role", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'paris')

        expect(cell.attr.role.html).to.have.string('Secondary')
    })

    it("Test # 3285: verify Paris data access", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'paris')

        expect(cell.attr['data-access'].html).to.have.string('RO')
    })


    it("Test # 3286: verify Santiago header", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'santiago')

        expect(cell.attr.header.html).to.have.string('SANTIAGO')
    })

    it("Test # 3287: verify Santiago role", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'santiago')

        expect(cell.attr.role.html).to.have.string('Secondary')
    })

    it("Test # 3288: verify Santiago data access", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'santiago')

        expect(cell.attr['data-access'].html).to.have.string('RO')
    })

    it("Test # 3289: verify Rome header", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'rome')

        expect(cell.attr.header.html).to.have.string('ROME')
    })

    it("Test # 3290: verify Rome role", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'rome')

        expect(cell.attr.role.html).to.have.string('Supplementary')
    })

    it("Test # 3291: verify Rome data access", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // find the correct object
        const cell = graph.find(model => model.id === 'rome')

        expect(cell.attr['data-access'].html).to.have.string('RW')
    })
})
