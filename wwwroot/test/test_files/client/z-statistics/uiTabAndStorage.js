/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require('./utils')

describe("Statistics: Stats: Tab management", async () => {
    it("Test # 2210: Open tab, verify connection", async () => {
        await utils.initStats()
    })

    it("Test # 2211: Open tab, verify no second tab can be open", async () => {
        await utils.initStats()

        const disabled = await page.evaluate(() => $('#menuTabsStats').hasClass('disabled'))
        expect(disabled).to.be.true
    })


    it("Test # 2212: Open tab, close tab, verify tab can be opened again", async () => {
        await utils.initStats()

        await page.evaluate(() => app.ui.tabs.close('tab-S'))

        await libs.delay(500)

        await utils.initStats()
    })

    it("Test # 2213: Open tab, verify no name and brackets are displayed", async () => {
        await utils.initStats()

        const text = await page.evaluate(() => $('#lblStatsReportName').text())

        expect(text === '').to.be.true
    })

    it("Test # 2214: open a file, verify name is displayed in brackets", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        const text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1]').to.be.true
    })

    it("Test # 2215: open a file, close a file, verify name AND brackets are gone", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        let text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1]').to.be.true

        await page.evaluate(() => app.ui.tabs.close('tab-S'))

        await libs.delay(500)

        text = await page.evaluate(() => $('#lblStatsReportName').text())

        expect(text === '').to.be.true
    })
    it("Test # 2216: open a file, modify it, verify that dirty * appears", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // make it dirty
        await page.evaluate(() => app.ui.stats.tab.toolbar.switchTheme())

        const text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1*]').to.be.true
    })

    it("Test # 2217: open a file, modify it, save it, verify that dirty * disappears", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // make it dirty
        await page.evaluate(() => app.ui.stats.tab.toolbar.switchTheme())

        // save it
        await page.evaluate(() => app.ui.stats.storage.test.saveTest())

        // app.ui.stats.storage
        const text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1]').to.be.true
    })
})

describe("Statistics: Stats: Storage management: New", async () => {
    it("Test # 2220: Verify inputbox is displayed", async () => {
        await utils.initStats()

        await page.evaluate(() => app.ui.stats.tab.toolbar.newStat())

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('This will clear all the sources and related reports. Are you sure')

    })

    it("Test # 2221: If prev doc is dirty, verify double msg box is displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // make it dirty
        await page.evaluate(() => app.ui.stats.tab.toolbar.switchTheme())

        page.evaluate(() => app.ui.stats.tab.toolbar.newStat())

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The report file has been modified.')
    })

    it("Test # 2222: load report, clear report verify it gets deleted", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // verify that a source exist
        await utils.openSource()
        let ret = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(ret !== '').to.be.true
        await page.evaluate(() => $('#modalStatsSources').modal('hide'))
        await libs.waitForDialog('#modalStatsSources', 'close');

        // clear buffer
        page.evaluate(() => app.ui.stats.tab.toolbar.newStat())

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('This will clear all the sources and related reports. Are you sure')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // open sources
        await utils.openSource()

        // verify it is empty
        ret = await page.evaluate(() => $('#tblStatsSources > tbody >tr').text())

        expect(ret === '').to.be.true
    })
})


describe("Statistics: Stats: Storage management: Save", async () => {
    it("Test # 2226: Click on icon, expect dialog to show", async () => {
        await utils.initStats()

        await utils.openSave()
    })

    it("Test # 2227: Type no name, no description, save", async () => {
        await utils.initStats()

        await utils.openSave()

        let btnClick = await page.$("#btnStatsSaveOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2228: Type a name, no description, save", async () => {
        await utils.initStats()

        await utils.openSave()

        await page.evaluate(() => $('#txtStatsName').val('testsave'))

        let btnClick = await page.$("#btnStatsSaveOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsSourcesSave', 'close');
    })

    it("Test # 2229: Type a name, with description, save", async () => {
        await utils.initStats()

        await utils.openSave()

        await page.evaluate(() => $('#txtStatsName').val('testsave2'))
        await page.evaluate(() => $('#txtStatsDescription').text('testsave description'))

        let btnClick = await page.$("#btnStatsSaveOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsSourcesSave', 'close');
    })

    it("Test # 2230: Type an existing name, save, expect overwrite dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        await utils.openSave()

        await page.evaluate(() => $('#txtStatsName').val('test-1'))

        let btnClick = await page.$("#btnStatsSaveOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The name is already used.Do you want')
    })

    it("Test # 2231: Load existing, perform a change, verify dirty flag, click save, type a name, save, overwrite, dirty flag should disappear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // make it dirty
        await page.evaluate(() => app.ui.stats.tab.toolbar.switchTheme())

        let text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1*]').to.be.true

        await utils.openSave()

        await page.evaluate(() => $('#txtStatsName').val('test-1'))

        let btnClick = await page.$("#btnStatsSaveOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The name is already used.Do you want')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1]').to.be.true
    })
})

describe("Statistics: Stats: Storage management: Load", async () => {
    it("Test # 2235: Open dialog, verify list is populated, select item, verify description is displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        await utils.openOpen()

        const list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 1).to.be.true

        await page.evaluate(() => $('#selStatsSourcesLoadNames option[value="test-1"]').attr('selected', 'selected'))
        await page.evaluate(() => app.ui.stats.storage.load.nameSelected())

        const description = await page.evaluate(() => $('#divStatsSourcesLoadDescription').text())
        expect(description !== '').to.be.true
    })

    it("Test # 2236: Load report, make it dirty, open dialog, select and load, verify save dirty dialog appears", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // make it dirty
        await page.evaluate(() => app.ui.stats.tab.toolbar.switchTheme())

        let text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1*]').to.be.true

        await utils.openOpen()

        const list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 1).to.be.true

        let btnClick = await page.$("#btnStatsLoadOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The report file has been modified.Do you want to save it first?')
    })
})

describe("Statistics: Stats: Storage management: Delete", async () => {
    it("Test # 2240: Open the load dialog, select an item, delete, expect msgbox, cancel, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        await utils.openOpen()

        let list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 1).to.be.true

        await page.evaluate(() => $('#selStatsSourcesLoadNames option[value="test-1"]').attr('selected', 'selected'))

        // click on delete
        let btnClick = await page.$("#btnStatsLoadDelete");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxNo");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 1).to.be.true

    })

    it("Test # 2241: Open the load dialog, select an item, delete, expect msgbox, confirm, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        await utils.openOpen()

        let list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 1).to.be.true

        await page.evaluate(() => $('#selStatsSourcesLoadNames option[value="test-1"]').attr('selected', 'selected'))

        // click on delete
        let btnClick = await page.$("#btnStatsLoadDelete");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        list = await page.evaluate(() => $('#selStatsSourcesLoadNames').children())
        expect(list.length === 0).to.be.true
    })
})

describe("Statistics: Stats: Message in main screen", async () => {
    it("Test # 2242: Stat the stats, msg \"No view is defined\" should be visible", async () => {
        await utils.initStats()

        const display = await page.evaluate(() => $('#spanStatsEmptySparkchart').css('display'))
        expect(display).to.have.string('inline')
    })

    it("Test # 2243: Stat the stats, load report 1, msg \"No view is defined\" should NOT be visible", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        const display = await page.evaluate(() => $('#spanStatsEmptySparkchart').css('display'))
        expect(display).to.have.string('none')
    })
})
