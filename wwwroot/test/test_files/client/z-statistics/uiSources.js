/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");
const {exec} = require('child_process');

describe("Statistics: Stats: Sources: Add", async () => {
    it("Test # 2250: YGBLSTAT: Should display dialog", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
    })

    it("Test # 2251: FHEAD: Should display dialog", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()
    })

    it("Test # 2252: PEEKBYNAME: Should display dialog", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()
    })
})

describe("Statistics: Stats: Sources: Edit", async () => {
    it("Test # 2254: YGBLSTAT: Select entry, then edit, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');
    })

    it("Test # 2255: YGBLSTAT: Double-click selection, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.dblClickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector');
    })

    it("Test # 2256: FHEAD: Load test 9, select entry, then edit, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(9))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(9))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
    })

    it("Test # 2257: FHEAD: Load test 9, double-click selection, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(9))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(9))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.dblClickOnElement(elem)

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
    })

    it("Test # 2258: PEEKBYNAME: Load test 10, select entry, then edit, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(10))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(10))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
    })

    it("Test # 2259: PEEKBYNAME: Load test 9, double-click selection, should display dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(10))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(10))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.dblClickOnElement(elem)

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
    })
})

describe("Statistics: Stats: Sources: Delete", async () => {
    it("Test # 2260: Select with no selection, should display msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // click on delete
        let btnClick = await page.$("#btnStatsSourcesDelete");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2261: Select entry, then delete, should delete it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let list = await page.evaluate(() => $('#tblStatsSources > tbody').children())
        expect(list.length === 1).to.be.true

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        // click on delete
        let btnClick = await page.$("#btnStatsSourcesDelete");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('This source has views in the Spark Chart')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        list = await page.evaluate(() => $('#tblStatsSources > tbody').children())
        expect(list.length === 0).to.be.true
    })
})

describe("Statistics: Stats: Sources: Clear all", async () => {
    it("Test # 2265: Create 2 entries, then clear all, list should be empty", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let btnClick = await page.$("#btnStatsSourcesClearAll");
        await btnClick.click();

        const list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === 0).to.be.true
    })
})

describe("Statistics: Stats: Sources: Ok button", async () => {
    it("Test # 2266: Delete entry, click Cancel, reopen it, should not have deleted it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let btnClick = await page.$("#btnStatsSourcesClearAll");
        await btnClick.click();

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === 0).to.be.true

        let elem = await page.$('#btnStatsSourcesCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        await utils.openSource()

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === 1).to.be.true
    })

    it("Test # 2267: Delete entry, click ok, reopen it, should have deleted it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // click clear all
        let btnClick = await page.$("#btnStatsSourcesClearAll");
        await btnClick.click();

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === 0).to.be.true

        let elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        await utils.openSource()

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === 0).to.be.true
    })


    it("Test # 2270: create item, disable it, close dialog, reopen, confirm", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select first row
        let btnClick = await page.$("#sourcesEntryEnabled-0");
        await btnClick.click();

        let elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        await utils.openSource()

        const checked = await page.evaluate(() => $('#sourcesEntryEnabled-0').is(':checked'))
        expect(checked).to.be.false
    })
})

describe("Statistics: Stats: PIDs", async () => {
    it("Test # 2271: verify that PIds list is empty", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await libs.delay(500)

        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))

        const pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').text())
        expect(pids).to.have.string('No processes found...')

        await libs.delay(2000)
    })

    it("Test # 2272: verify that PIds list is empty, run test program, refresh, verify it gets populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))

        let pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').val())
        expect(pids.length === 0).to.be.true

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').text())
        expect(pids).to.not.have.string('No processes found...')

        // wait for program to finish running
        await libs.delay(3000)
    })

    it("Test # 2272: verify that PIds list is empty, run test program, refresh, verify it gets populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))

        let pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').val())
        expect(pids.length === 0).to.be.true

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').text())
        expect(pids).to.not.have.string('No processes found...')

        // wait for program to finish running
        await libs.delay(3000)

        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        pids = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids').text())
        expect(pids).to.have.string('No processes found...')
    })

    it("Test # 2274: select samples, PIDS, no pid selected, press OK, expect msgbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set pids processes
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesPids').attr('checked', 'checked'))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('You must select at least one process')
    })

    it("Test # 2275: select samples, run test program, PIDS select PIDs, press ok, expect NO msgbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set top processes
        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesPids').attr('checked', 'checked'))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // refresh the list
        let btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        // select the pid
        await page.evaluate(() => $('#selStatsYgblstatsSelectorPids option:first').attr('selected', 'selected'))

        // and close the dialog
        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        // wait for dialog to close (so, no msgbox)
        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');
    })

    it("Test # 2276: Create source with aggr, edit with PIDs, click ok, should save", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        // set top processes
        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesPids').attr('checked', 'checked'))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // refresh the list
        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        // select the pid
        await page.evaluate(() => $('#selStatsYgblstatsSelectorPids option:first').attr('selected', 'selected'))

        // and close the dialog
        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        // wait for dialog to close (so, no msgbox)
        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');
    })

    it("Test # 2277: Load report 1, edit first source, select PIDs, click ok, expect inputbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        // set top processes
        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesPids').attr('checked', 'checked'))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // refresh the list
        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        // select the pid
        await page.evaluate(() => $('#selStatsYgblstatsSelectorPids option:first').attr('selected', 'selected'))

        // and close the dialog
        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalInputbox');

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The processes type has changed.This will require a deletion of the existing views.Do you want to proceed?')
    })

    it("Test # 2278: start program, create source with PIDs, restart test program, edit again, verify PIDs gets deselected", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        // set top processes
        await page.evaluate(() => $('#divStatsYgblProcessesAll').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesTop').css('display', 'none'))
        await page.evaluate(() => $('#divStatsYgblProcessesPids').css('display', 'block'))
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesPids').attr('checked', 'checked'))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // refresh the list
        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        await libs.delay(500)

        // select the pid
        await page.evaluate(() => $('#selStatsYgblstatsSelectorPids option:first').attr('selected', 'selected'))

        await libs.delay(3000)

        // run the background random generator program again
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // refresh the list
        btnClick = await page.$("#btnStatsYgblRefreshPids");
        await btnClick.click();

        // get the selected list
        const selItems = await page.evaluate(() => $('#selStatsYgblstatsSelectorPids option:first').val())
        expect(selItems === undefined).to.be.true
    })
})

describe("Statistics: Stats: ygbl dialog: Add mode", async () => {
    it("Test # 2280: click ok, should display msg asking for samples", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('You must select at least one sample')
    })

    it("Test # 2282: select samples, select region, set sample rate to 0, click ok, should display msgbox asking for sample rate", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        // reset sample rate
        await page.evaluate(() => $('#inpStatsYgblstatsSelectorRateSecs').val(0))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('You must enter a sample rate greater than 0')
    })

    it("Test # 2283: select samples, select region, click ok, should close and add the entry", async () => {
        await utils.initStats()

        await utils.openSource()

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        const oldTotal = list.length

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length > oldTotal).to.be.true
    })

    it("Test # 2284: select samples, select region, select processes top, click ok, should display msg asking for # of processes", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        // set top processes
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('You must enter a valid number of processes')
    })

    it("Test # 2286: select samples, select region, select processes top, #, click ok, should close and add the entry sample name", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        // set top processes
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))

        await page.evaluate(() => $('#numStatsYgblstatsSelectorProcessesTop').val(3))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('You must select the sample to be used')
    })

    it("Test # 2283: select samples, select region, click ok, should close and add the entry", async () => {
        await utils.initStats()

        await utils.openSource()

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        const oldTotal = list.length

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))
        await page.evaluate(() => app.ui.stats.sources.ygblstats.samplesChange())

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        // set top processes
        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))

        await page.evaluate(() => $('#optStatsYgblstatsSelectorTopSample option[value="SET"]').attr('selected', 'selected'))

        await page.evaluate(() => $('#numStatsYgblstatsSelectorProcessesTop').val(3))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length > oldTotal).to.be.true
    })

    it("Test # 2287: select samples, select region, click cancel, should close and NOT add the entry", async () => {
        await utils.initStats()

        await utils.openSource()

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        const oldTotal = list.length

        await utils.openSourceAdd()

        // set samples
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        let elem = await page.$('#btnStatsYgblstatsSelectorCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length === oldTotal).to.be.true
    })
})

describe("Statistics: Stats: ygbl dialog: Edit mode", async () => {
    it("Test # 2290: Should populate list with all data correctly", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        expect(await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples').val().length) === 11).to.be.true

        expect(await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions').val().length) === 1).to.be.true

        expect(await page.evaluate(() => $('#statsYgblstatsSelectorProcessesAgg').is(':checked'))).to.be.true
    })

    it("Test # 2291: edit samples, click ok, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="MLK"]').attr('selected', 'selected'))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('Update the source?')
    })

    /*
    it("Test # 2292: edit regions, click ok, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(4)').text())
            console.log(colData)
        expect(colData.split(', ').length === 2).to.be.true
    })

     */

    it("Test # 2293: edit processes, click ok, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))

        await page.evaluate(() => $('#numStatsYgblstatsSelectorProcessesTop').val(3))

        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(4)').text())
        expect(colData).to.have.string('Top 3 of SET')
    })

    it("Test # 2294: edit sample rate, click ok, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#inpStatsYgblstatsSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.ygblstats.sampleRateUpdated())

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('Update the source?')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');


        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(6)').text())
        expect(colData).to.have.string('3')
    })

    it("Test # 2295: edit sample rate, click ok, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#inpStatsYgblstatsSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.ygblstats.sampleRateUpdated())

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('Update the source?')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(6)').text())
        expect(colData).to.have.string('3')
    })

    it("Test # 2296: edit regions, click ok, inputbox should appear, click yes, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        const oldLength = await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions').val().length)
        expect(oldLength === 1).to.be.true

        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('The regions are changed')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(3)').text())
        expect(colData.split(', ').length > oldLength)
    })

    it("Test # 2297: load report 1, edit samples [add MLK], click ok, inputbox should appear, should have no warnings, click yes, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        const oldLength = await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples').val().length)
        expect(oldLength === 11).to.be.true

        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="MLK"]').attr('selected', 'selected'))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text == 'Update the source?').to.be.true

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(3)').text())
        expect(colData.split(', ').length > oldLength).to.be.true
    })

    it("Test # 2298: load report 1, edit samples [remove SET], click ok, inputbox should appear, should have no warnings, click yes, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        const oldLength = await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples').val().length)
        expect(oldLength === 11).to.be.true

        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').prop('selected', false))

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('SET will be removed')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');
    })
})
