/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");
const {exec} = require('child_process');

/*
describe("Statistics: Stats: Rendering: Dynamic: Layout", async () => {
it("Test # 2700: Load report 6, verify 2nd view ProcessId (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[0]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[1]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[2]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[3]')

        await libs.delay(10000)
    }
})

it("Test # 2701: Load report 6, verify 2nd view ProcessId (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[0]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[1]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[2]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[3]')

        await libs.delay(10000)
    }
})

it("Test # 2702: Load report 6, verify 4th view ProcessId (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[0]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[1]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[2]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[3]')

        await libs.delay(10000)
    }
})

it("Test # 2703: Load report 6, verify 4th view ProcessId (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[0]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[1]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[2]')

        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.not.have.string('P[3]')

        await libs.delay(10000)
    }
})

it("Test # 2704: Load report 6, verify 1st view Data (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        const sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        const sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        const sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2705: Load report 6, verify 1st view Data (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2706: Load report 6, verify 2nd view Data (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true


        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2707: Load report 6, verify 2nd view Data (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        let sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(parseInt(sampleStat4) !== 0).to.be.true

        let sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(parseInt(sampleStat5) !== 0).to.be.true

        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(parseInt(sampleStat4) === 0).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(parseInt(sampleStat5) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2708: Load report 6, verify 3rd view Data (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(4000)

        // check the processes
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2709: Load report 6, verify 3rd view Data (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2710: Load report 6, verify 4th view Data (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes for DEFAULT
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true


        // check the processes for YDBAIM
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true

        // check the processes
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat3) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2711: Load report 6, verify 4th view Data (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes for DEFAULT
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true


        // check the processes for DEFAULT
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) === 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) === 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2712: Load report 6, verify 5th view Data (H)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

        // wait for samples to be accumulated
        await libs.delay(5000)

        // check the processes for DEFAULT
        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        let sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(parseInt(sampleStat4) === 0).to.be.true

        let sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(parseInt(sampleStat5) !== 0).to.be.true

        let sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(7)').text())
        expect(parseInt(sampleStat6) === 0).to.be.true

        let sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(8)').text())
        expect(parseInt(sampleStat7) === 0).to.be.true

        let sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(9)').text())
        expect(parseInt(sampleStat8) !== 0).to.be.true

        let sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(10)').text())
        expect(parseInt(sampleStat9) === 0).to.be.true

        let sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(11)').text())
        expect(parseInt(sampleStat10) !== 0).to.be.true

        let sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(12)').text())
        expect(parseInt(sampleStat11) !== 0).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(5)').text())
        expect(parseInt(sampleStat4) === 0).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(6)').text())
        expect(parseInt(sampleStat5) !== 0).to.be.true

        sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(7)').text())
        expect(parseInt(sampleStat6) === 0).to.be.true

        sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(8)').text())
        expect(parseInt(sampleStat7) === 0).to.be.true

        sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(9)').text())
        expect(parseInt(sampleStat8) !== 0).to.be.true

        sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(10)').text())
        expect(parseInt(sampleStat9) === 0).to.be.true

        sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(11)').text())
        expect(parseInt(sampleStat10) !== 0).to.be.true

        sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(12)').text())
        expect(parseInt(sampleStat11) === 0).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(parseInt(sampleStat1) !== 0).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(3)').text())
        expect(parseInt(sampleStat2) !== 0).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(4)').text())
        expect(parseInt(sampleStat3) !== 0).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(5)').text())
        expect(parseInt(sampleStat4) === 0).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(6)').text())
        expect(parseInt(sampleStat5) !== 0).to.be.true

        sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(7)').text())
        expect(parseInt(sampleStat6) === 0).to.be.true

        sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(8)').text())
        expect(parseInt(sampleStat7) === 0).to.be.true

        sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(9)').text())
        expect(parseInt(sampleStat8) !== 0).to.be.true

        sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(10)').text())
        expect(parseInt(sampleStat9) === 0).to.be.true

        sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(11)').text())
        expect(parseInt(sampleStat10) !== 0).to.be.true

        sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(12)').text())
        expect(parseInt(sampleStat11) !== 0).to.be.true

        await libs.delay(10000)
    }
})

it("Test # 2713: Load report 6, verify 5th view Data (V)", async () => {
    if (global.serverMode === 'RO') {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

    // run the background random generator program
    const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,10)"');

    // wait for samples to be accumulated
    await libs.delay(5000)

    // check the processes for DEFAULT
    let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) === 0).to.be.true

    let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) === 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) === 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) === 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) === 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) === 0).to.be.true

    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(2)').text())
    expect(parseInt(sampleStat2) === 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) === 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) === 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true


    sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(2)').text())
    expect(parseInt(sampleStat1) !== 0).to.be.true

    sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(3)').text())
    expect(parseInt(sampleStat2) !== 0).to.be.true
    }
})
})
 */
