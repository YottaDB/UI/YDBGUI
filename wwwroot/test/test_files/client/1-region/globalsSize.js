/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("CLIENT: Region View: Globals tab", async () => {
    it("Test # 1950: Open the dialog, select scan and close. Open again and verify that dialog display default option and param", async () => {
        // remove octo, as the detection keeps everything open
        try {
            execSync('mv $ydb_dist/plugin/octo $ydb_dist/plugin/octoOld')
        } catch (e) {
        }

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200);
        // Verify that there are no temporary directories left when getting a list of globals; they should be deleted
        tempdirs = execSync('find $ydb_tmp/* -type d').toString();
        expect(tempdirs).to.be.empty;

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#optGlobalSizeTypeScan");
        await btnClick.click();

        //btnClick = await page.$("#btnGlobalSizeCancel");
        //await btnClick.click();
        await page.evaluate(() => $('#modalGlobalSize').modal('hide'));

        // wait for regionView to close
        await libs.waitForDialog('#modalGlobalSize', 'close');

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        const flag = await page.evaluate(() => $('#optGlobalSizeTypeArSample').prop('checked'));

        expect(flag).to.be.true
    });

    it("Test # 1951: Open the dialog, select Importance sampling, verify that param is set to 1000", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#optGlobalSizeTypeImpSample");
        await btnClick.click();

        await libs.delay(200)

        const flag = await page.evaluate(() => $('#inpGlobalSizeParam').val());
        expect(flag === '1000').to.be.true
    })

    it("Test # 1952: Open the dialog, select Scan, verify that param is set to 0", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#optGlobalSizeTypeScan");
        await btnClick.click();

        await libs.delay(200)

        const flag = await page.evaluate(() => $('#inpGlobalSizeParam').val());
        expect(flag === '0').to.be.true
    })

    it("Test # 1953: Open the dialog, execute using default values, verify that table gets populated", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^Categoriesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2) > td:nth-child(3)').text());
        expect(text === '').to.be.false

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2) > td:nth-child(4)').text());
        expect(text === '').to.be.false
    })

    it("Test # 1954: Open the dialog, execute using default values, verify that sort headers are correctly colored", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(2000)

        let color = await page.evaluate(() => $('#regionViewGlobalsHeaderName').css('color'));
        expect(color).to.have.string('rgb(255, 127, 39)') // ydb_status_red

        color = await page.evaluate(() => $('#regionViewGlobalsHeaderStatus').css('color'));
        expect(color).to.have.string('rgb(59, 26, 104)') // ydb_status_green

        color = await page.evaluate(() => $('#regionViewGlobalsHeaderSizeBlocks').css('color'));
        expect(color).to.have.string('rgb(59, 26, 104)') // ydb_status_green

        color = await page.evaluate(() => $('#regionViewGlobalsHeaderSizeBytes').css('color'));
        expect(color).to.have.string('rgb(59, 26, 104)') // ydb_status_green
    })

    it("Test # 1955: Open the dialog, execute using default values, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^Categoriesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^Employeesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^ORDok312.00 KiB')
    })

    it("Test # 1956: Open the dialog, execute using default values, sort by name, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        btnClick = await page.$("#regionViewGlobalsHeaderName");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^nwCustomersok312.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^nwOrdersok312.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^testglobalok28.00 KiB')
    })

    it("Test # 1957: Open the dialog, execute using default values, sort by name twice, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderName");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#regionViewGlobalsHeaderName");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^Categoriesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^Employeesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^ORDok312.00 KiB')
    })

    it("Test # 1958: Open the dialog, execute using default values, sort by status, verify that data is correctly sorted", async () => {
        // create a gld file for default
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/DEFAULT.gld" yottadb -run GDE @/YDBGUI/wwwroot/test/test-gld/DEFAULT.gde');

        // create a zombie in DEFAULT
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/DEFAULT.gld" yottadb -run %XCMD \'s ^%ydbocto=1\'');

        // delete the .gld file
        execSync('rm /YDBGUI/wwwroot/test/DEFAULT.gld');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderStatus");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^ORDok312.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(4)').text());
        expect(text).to.have.string('^OrderDetailsok416.00 KiB')
    })

    it("Test # 1959: Open the dialog, execute using default values, sort by status twice, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderStatus");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#regionViewGlobalsHeaderStatus");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^Employeesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(4)').text());
        expect(text).to.have.string('^ORDok')
    })

    it("Test # 1960: Open the dialog, execute using default values, sort by size blocks, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBlocks");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^Categoriesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^Employeesok28.00 KiB')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^%ydboctozombie28.00 KiB')
    })

    it("Test # 1961: Open the dialog, execute using default values, sort by size blocks twice, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBlocks");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBlocks");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^OrderDetailsok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^PSNDFok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^ORDok312.00 KiB')
    })

    it("Test # 1962: Open the dialog, execute using default values, sort by size bytes, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBytes");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^Categoriesok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^Employeesok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^%ydboctozombie')
    })

    it("Test # 1963: Open the dialog, execute using default values, sort by size bytes twice, verify that data is correctly sorted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBytes");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#regionViewGlobalsHeaderSizeBytes");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^OrderDetailsok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('PSNDFok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^ORDok312.00 KiB')
    })

    it("Test # 1964: Open the dialog, execute using default values, click the refresh button, verify that table is reset", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#btnRegionViewRefresh");
        await btnClick.click();

        await libs.delay(500)

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1) > td:nth-child(3)').text());
        expect(text === '').to.be.true
    })

    it("Test # 1965: Open the dialog, execute using default values, click the refresh button, verify that table is reset, fetch size again, verify that default sort order and coloring is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        btnClick = await page.$("#btnRegionViewRefresh");
        await btnClick.click();

        await libs.delay(500)

        let text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1) > td:nth-child(3)').text());
        expect(text === '').to.be.true

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(500)

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(1)').text());
        expect(text).to.have.string('^%ydboctozombie')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(2)').text());
        expect(text).to.have.string('^Categoriesok')

        text = await page.evaluate(() => $('#tblRegionViewGlobals > tbody >tr:nth-child(3)').text());
        expect(text).to.have.string('^Employeesok28.00')
    })
})

describe("CLIENT: Region View: Globals: Tree map", async () => {
    it("Test # 1980: Open DEFAULT Region View, select Regions tab, Tree map... button should be disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        const disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').attr('disabled'));
        expect(disabled).to.have.string('disabled')
    })

    it("Test # 1981: Open DEFAULT Region View, select Regions tab, compute space, Tree map... button should be enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        const disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1982: Open DEFAULT Region View, select Regions tab, compute space, Tree map... button should be enabled, close Region view, open it again, select Regions tab, Tree map button should be disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        let disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false

        // close region view
        btnClick = await page.$("#btnRegionViewClose");
        await btnClick.click();

        // wait for regionView to be closed by the async call
        await libs.waitForDialog('#modalRegionView', 'close');

        btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1983: Open DEFAULT Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        const disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRegionViewGlobalsTreeMap");
        await btnClick.click();

        // wait for tree map to be set by the async call
        await libs.waitForDialog('#modalGlobalsTreeMap');
    })

    it("Test # 1984: Open DEFAULT Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed, input should have max = 12", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        const disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRegionViewGlobalsTreeMap");
        await btnClick.click();

        // wait for tree map to be set by the async call
        await libs.waitForDialog('#modalGlobalsTreeMap');

        const max = await page.evaluate(() => $('#inpGlobalsTreeMap').attr('max'));
        expect(max).to.have.string('13')
    })

    it("Test # 1985: Open YDBOCTO Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed, input should have max = 2", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView3");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRegionView') !== 'none';
        expect(isVisible).to.be.true;

        btnClick = await page.$("#navRegionViewGlobals");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRegionViewGlobalSize");
        await btnClick.click();

        // wait for globalSize to be set by the async call
        await libs.waitForDialog('#modalGlobalSize');

        btnClick = await page.$("#btnGlobalSizeOk");
        await btnClick.click();

        await libs.delay(1000)

        const disabled = await page.evaluate(() => $('#btnRegionViewGlobalsTreeMap').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRegionViewGlobalsTreeMap");
        await btnClick.click();

        // wait for tree map to be set by the async call
        await libs.waitForDialog('#modalGlobalsTreeMap');

        const max = await page.evaluate(() => $('#inpGlobalsTreeMap').attr('max'));
        expect(max).to.have.string('3')
    })
})
