%ydbguiRoutines ; Routines handlers; 05-07-2021
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
; ****************************************************************
; find(body)
;
; BODY:
; mask (string) 	mask (may have * and ?)
;
; RETURNS
; JDOM res array with routines/sources list
; ****************************************************************
find(body)
	new res,rou,props,srcRoutines,objRoutines,ix,cnt,%ZR,mask
	;
	set mask=body("mask")
	do zrouUpdate(.body)
	;
	; find the srcRoutines
	do SILENT^%RSEL(mask,"SRC")
	if %ZR merge srcRoutines=%ZR
	;
	; find the objRoutines
	kill %ZR
	do SILENT^%RSEL(mask,"OBJ")
	if %ZR=0,$data(srcRoutines)=0 set res("data")=0 goto findQuit
	;
	merge objRoutines=%ZR
	;
	; flag entries without source
	set ix=""
	for  set ix=$order(objRoutines(ix))	quit:ix=""  do
	. if $zlength($text(@("+0^"_ix))),$zlength($text(@("+1^"_ix)))=0 set objRoutines(ix,"noSourceCode")=1
	;
merge
	merge res("pre-data")=objRoutines
	merge res("pre-data")=srcRoutines
	;
	set ix=""
	for  set ix=$order(res("pre-data",ix)) quit:ix=""  do
	. set res("data",$increment(cnt),"routine")=ix
	. set res("data",cnt,"location")=res("pre-data",ix)
	. set res("data",cnt,"source")=$select($get(res("pre-data",ix,"noSourceCode")):"false",1:"true")
	;
	kill res("pre-data")
	;
findQuit
	set res("result")="OK"
	;
	quit *res
	;
	;
; ****************************************************************
; get(body)
;
; BODY:
; routine (string) 	the routine name
;
; RETURNS
; JDOM res array with routines/sources list
; ****************************************************************
get(body)
	new res,file,path,line,rowCnt,props,tmp,i,buf,%ZR
	;
	set routine=$ztranslate(body("routine"),"_","%")
	do zrouUpdate(.body)
	;
	for i=1:1 set tmp=$text(@("+"_i_"^"_routine)) quit:$zlength(tmp)=0  set res("data","file",i)=tmp
	if $data(res("data","file"))=0 do  goto getQuit
	. set res("result")="ERROR"
	. set res("error","description")="Routine is empty or not found"
	;
	; get the file properties
	do SILENT^%RSEL($ztranslate(routine,"_","%"),"SRC")
	if %ZR>0 do
	. set *props=$$getFileProps^%ydbguiUtils(%ZR(routine)_$ztranslate(routine,"%","_")_".m")
	. merge res("data","props")=props
	. set res("data","props","filename")=$ztranslate(routine,"%","_")_".m"
	. set res("data","props","path")=$zparse(%ZR(routine),"DIRECTORY")
	;
	; get the object file properties
	kill %ZR
	do SILENT^%RSEL(routine,"OBJ")
	if %ZR>0 do
	. set path=$get(%ZR(routine))
	. if $zextract(path,$zlength(path)-2,$zlength(path))'=".so" set path=path_$ztranslate(routine,"%","_")_".o"
	. ;
	. set *props=$$getFileProps^%ydbguiUtils(path)
	. merge res("data","object","props")=props
	. set res("data","object","props","filename")=$zparse(path,"NAME")_$zparse(path,"TYPE")
	. set res("data","object","props","path")=$zparse(path,"DIRECTORY")
	;
	set res("result")="OK"
	;
getQuit
	quit *res
	;
	;
zrouUpdate(body)
	if $get(body("zroutines"))'="",body("zroutines")'=$zroutines set $zroutines=body("zroutines")
	;
	quit
