%ydbguiStatsGenerator ; Stats Generator code
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	;
	quit
	;
	; NOTE: this file is not distributed and ment only for local testing
	;
	;
	
go
	new procs,rateLow,rateHigh,str,PIDs,ret
	;
	write !!,"Random Stats Generator 1.0.1",!
	;
promptProcesses	
	read "How many processes you want to run (press ^ to quit) ? ",procs
	if procs="^" w !! halt
	if +procs=0 write !,"It must be a number",! goto promptProcesses
	;
promptLowRate
	read !,"Enter the lower rate in ms.: ",rateLow
	if +rateLow=0 write !,"It must be a number",! goto promptLowRate
	;
promptHighRate
	read !,"Enter the higher rate in ms.: ",rateHigh
	if +rateHigh=0 write !,"It must be a number",! goto promptHighRate
	if rateHigh<rateLow write !,"It must be higher than the low rate..." goto promptHighRate
	;
promptRun	
	read !!,"Press R to run, ^ to quit: ",str
	if str="R"!(str="r") goto run
	if str="^" w !! halt
	goto promptRun
	;	
run
	write !!,"Starting..."
	;	
	for ix=1:1:procs do
	. job statsGen(rateLow,rateHigh)
	. set PIDs(ix)=$zjob
	. write !,"Process: "_$zjob_" started..."
	;
	write !!,"Started",!
	;
terminateLoop	
	read !,"Press Q to quit: ",str
	if str="Q"!(str="q") do  goto go
	. for ix=1:1:procs do
	. . set ret=$zsigproc(PIDs(ix),15)
	. . write !,"Process "_PIDs(ix)_" terminated with return code: "_ret
	goto terminateLoop
	;
	quit				
	;
;*************************************************	
; statsGen	
;*************************************************	
statsGen(rateLow,rateHigh,ttl)
	; ttl (optional): Time To Live in seconds
	new rate,params,gname,rec,ix,dummy
	;
	view "STATSHARE"
	;
	if $get(ttl) set $ztimeout=ttl_":halt"
	;
	set rate=($random(rateHigh-rateLow)+rateLow)/1000
	;
	write !,"Rate: "_rate
rateLoop
	hang rate
	;
	if $get(gname)'="" lock -@gname
	;
	set gname="^"_$char($random(26)+65)
	;
	lock +@gname:5
	;
	write !,"Hitting: "_gname
 	for rec=1:1:$random(1000)+1 do
	. set @gname@(rec)=rec
	;
	set ix="" for  set ix=$order(@gname@(ix)) quit:ix=""  do
	. set opCode=$random(3)
	. ;
	. if opCode set dummy=$get(@gname@(ix))
	. else  kill @gname@(rec)
	;
	lock -@gname:5
	;
	hang .5
	;
	lock +@gname
	goto rateLoop
	;
